/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import static net.chafar.cuchimana.R.id;
import static net.chafar.cuchimana.R.id.syncdir;
import static net.chafar.cuchimana.R.layout;
import static net.chafar.cuchimana.R.string;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.File;

public class Folder extends ActivityBase implements VoiceRecording.VoiceRecordingListener, FragmentOnAttachListener {

  private VoiceRecording    _voiceRecording;

  private FragmentBase   _current;
  public enum ftype {
    file, dir, none
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(layout.activity_folder);

    int currpage = FolderAdapter.FILEFRAG_NUM;
    if (savedInstanceState != null) {
        currpage = savedInstanceState.getInt("pagenum");
    }

    Toolbar myToolbar = findViewById(id.toolbar);
    setSupportActionBar(myToolbar);

    ViewPager2 pager = findViewById(id.pager);
    FolderAdapter adapter = new FolderAdapter(this);
    TabLayout tabs = findViewById(id.tabs);
    tabs.addOnTabSelectedListener( new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        // menuda mierda: esto se llama antes que _adapter.createFragment() para el primero,
        // pero después para el segundo
        switch(tab.getId()) {
          case FolderAdapter.FILEFRAG_NUM:
            _current = fragment(ftype.file);
            break;
          case FolderAdapter.DIRFRAG_NUM:
            _current = fragment(ftype.dir);
            break;
        }
        FragmentBase frag = current();
        if (frag != null)   // unas veces ya se ha creado y otras no !!!
          frag.gotActive();
      }
      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
        if (current() != null)
          current().gotUnactive();
      }
      @Override
      public void onTabReselected(TabLayout.Tab tab) {
        onTabSelected(tab);
      }
    });
    pager.setAdapter(adapter);
    // conectar TabLayout con pager
    new TabLayoutMediator(tabs, pager, (tab, pos) -> {
        switch(pos) {
          case FolderAdapter.FILEFRAG_NUM:
            tab.setText(string.filesfrag_name); tab.setTag("files");
            tab.setId(FolderAdapter.FILEFRAG_NUM);
            // needed to avoid tab.view crashing at onRestoreInstanceState()
            tab.view.setId(id.filefragtabview);
            break;
          case FolderAdapter.DIRFRAG_NUM:
            tab.setText(string.dirsfrag_name);
            tab.setTag("dirs");
            tab.setId(FolderAdapter.DIRFRAG_NUM);
            // needed to avoid tab.view crashing at onRestoreInstanceState()
            tab.view.setId(id.dirfragtabview);
            break;
          default:
            tab.setText("TAB " + (pos + 1)); tab.setTag(tab.getText()); tab.setId(-1); break;
        }
      }).attach();

    // fragments stuff
    pager.setCurrentItem(currpage);
    if (_fnfrag != null && currentHasItems())
      _fnfrag.setListener((ItemsFragment)current());

    _dataModel.mgr().currDirName().observe(this, name -> {
      if (getSupportActionBar() != null)
        getSupportActionBar().setTitle(name);
    });

    if ( ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED )
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PermStorage);
    // MANAGE_EXTERNAL_STORAGE es nueva en API 30 (Android 11), y tiene protection level: signature|appop|preinstalled
    // ... parece que sin eso, en android 11, no podemos usar File.createTempFile(), x eso, ademas de WRITE_EXTERNAL_STORAGE,
    //     si api >= 30 necesitamos tb MANAGE_EXTERNAL_STORAGE, que no se pide de la forma habitual, sino así como sigue
    //     Hay que averiguar mas sobre los niveles de protección de MANAGE_EXTERNAL_STORAGE
    //     ( para api >= 30 aki: https://developer.android.com/training/data-storage/manage-all-files )
    // HAY QUE declarar ambas en manifest
    if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
      if (!Environment.isExternalStorageManager(new File(Constants.RootDir))) {
        Intent settingsIntent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
        if ( settingsIntent.resolveActivity(getPackageManager()) != null )
          startActivity(settingsIntent);
        else
          Toast.makeText(this, "no cuela", Toast.LENGTH_LONG).show();
      }
    }
    if ( ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
      ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET}, PermNet);
    if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED)
      ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, PermNet);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (grantResults[0] == PackageManager.PERMISSION_GRANTED )
      switch (requestCode) {
        case PermStorage:
          // on first execution upon installing, the very first lodad() will be done without external storage permissions
          _dataModel.load();
          break;
        case PermRecord:
          filesFragment().doCommand(id.menu_new_voice);
          break;
        case PermLocation:
          filesFragment().doCommand(id.menu_new_location);
          break;
      }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Esto se llama ya avanzada la creación de la vista de la activity y si llamas a Activity.invalidateOptionsMenu()
    Log.d(this.getClass().getSimpleName(), ".onCreateOptionsMenu");
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.folder_menu, menu);
    menu.findItem(id.menu_pick).setEnabled(filesFragment() != null && null != filesFragment().testPickFileIntent());
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    // esto se llama tras onCreateOptionsMenu y cada vez que despliegas menu.
    // puede acondicionar el menú según condiciones actuales; para cambiar los items
    // que aparecen sin desplegar habría que llamar a invalidateOptionsMenu()
    // p.e. en adapter.onPageSelected()
    Log.d(this.getClass().getSimpleName(), ".onPrepareOptionsMenu");
    MenuItem item = menu.findItem(id.menu_mark_submenu);
    if (item != null)
      item.setEnabled(_dataModel.marked() > 0);
    item = menu.findItem(id.menu_new_location);
    if (item != null)
      item.setEnabled(LocHelper.available(this));
    item = menu.findItem(syncdir);
    if (item != null)
      item.setEnabled(Utils.haveEncryptedPref(Constants.PrivKeyPass));
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if ( itemId == id.menu_new_vid || itemId == id.menu_new_pic || itemId == id.menu_new_text || itemId == id.menu_pick ) {
      return filesFragment().doCommand(item.getItemId());
    } else if ( itemId == syncdir || itemId == id.menu_new_dir ) {
      return dirsFragment().doCommand(item.getItemId());
    } else if ( itemId == id.menu_new_voice ) {
      if ( ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PermRecord);
        return true;
      }
      return filesFragment().doCommand(item.getItemId());

    } else if ( itemId == id.menu_new_location ) {

      if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PermLocation);
        return true;    // cuando se obtenga onRequestPermissionsResult() la dirigirá a doCommand()
      }
      return filesFragment().doCommand(item.getItemId());

    } else if ( itemId == id.menu_move ) {

      File newpath;
      for (File entry : _dataModel.marks()) {
        newpath = new File(FileManager.absPath(_dataModel.mgr().relPath()), entry.getName());
        if ( entry.renameTo(newpath) ) {
          //////// el path de marcas, como las listas en gral que maneja filemgr, es absoluto,
          // x lo que no se puede hacer así tan fácil
          Intent intent = getSyncIntent(SyncJob.CodOp.renamed, 0, FileManager.relPathOf(newpath.getPath()));
          intent.putExtra("oldpath", FileManager.relPathOf(entry.getPath()));
          startService(intent);
        } else {
          Log.e(getClass().getSimpleName(), "Failed moving marked to current dir");
          break;
        }
      }
      _dataModel.unmark();
      _dataModel.load();

      return true;

    } else if ( itemId == id.menu_unmark ) {

      // un-marks everything marked
      _dataModel.unmark();
      filesFragment().getAdapter().refreshView();
      dirsFragment().getAdapter().refreshView();
      return true;

    } else if ( itemId == id.menu_selall ) {

      if ( currentHasItems() ) {
        _dataModel.selectAll();
        ItemsFragment frgm = (ItemsFragment) current();
        frgm.getAdapter().refreshView();
        frgm.startActionMode();
      }
      return true;
    } else if ( itemId == id.menu_settings ) {
      Intent intent = new Intent(this, CuSettings.class);
      startActivity(intent);
      return true;
    } else if ( itemId == id.sync_dlg ) {
      SyncFragment dlg = new SyncFragment();
      dlg.show(getSupportFragmentManager(), dlg.getClass().getSimpleName());
      return true;
    } else {
      Log.e(getClass().getSimpleName(), "Unknown options item selected");
    }
    return false;
  }

  public void onAttachFragment(@NonNull FragmentManager mgr, @NonNull Fragment fragment) {
    super.onAttachFragment(mgr, fragment);

    // CUIDADO: esto se ejecuta _antes_ que onCreate() del fragment

    /* Quedarse con puntero a la activity lo hace el fragment en onAttach
       Esto es ejemplo de anotar en la activity puntero al fragment
       Sin embargo, para cuando se cerrase el fragment, no tenemos onDetachFragment,
       x lo que para poner _voiceRecording a null tendría que colaborar el fragment
       añadiendo un método a la interface de listener tal que fragmentDetached() o algo así
       y en su onDetach() llamar a ese método para que aki se pusiera _voiceRecording a null
    */
    if (fragment instanceof VoiceRecording)
      _voiceRecording = (VoiceRecording) fragment;

  }

  @Override
  public void onRecordingDone(Job job, String name) {
    if (_voiceRecording != null) {
      filesFragment().recordingDone(job, name);
      _voiceRecording.dismiss();
    }

  }

  private FragmentBase current() {
    return _current;
  }

  protected FragmentBase fragment(ftype type) {
    for (Fragment frgm : getSupportFragmentManager().getFragments()) {
      if (frgm.getClass() == FilesFragment.class && type == ftype.file
          || frgm.getClass() == DirsFragment.class && type == ftype.dir)
        return (FragmentBase) frgm;
    }
    return null;
  }

  protected FilesFragment filesFragment() {
    return (FilesFragment) fragment(ftype.file);
  }

  protected DirsFragment dirsFragment() {
    return (DirsFragment) fragment(ftype.dir);
  }

  protected int selected() {
    if ( _current instanceof FilesFragment )
      return FolderAdapter.FILEFRAG_NUM;
    else
      return FolderAdapter.DIRFRAG_NUM;
  }
  private boolean currentHasItems() {
    return current().getClass().getGenericSuperclass() == ItemsFragment.class;
  }

  @Override
  public View getViewForSnackbar() {
    View view = current().getView();
    if (view != null)
      view = view.findViewById(R.id.fab);
    return view;
  }

  /*
   * lifecycle
   *
   *    onCreate() -> onStart() [ -> onRestoreInstanceState ] -> onResume() -> running -> onPause() -> onSaveInstanceState() -> onStop() -> onDestroy()
   *                                                                ^ - - - - - - - - - - - -|
   *                     ^ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
   *        ^ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
   * giro de pantalla:
   *
   *     onPause -> onSaveInstanceState -> onStop -> onDestroy -> onCreate(!null) -> onStart -> onRestoreInstanceState -> onResume
   *
   * startactivity:
   *
   *    onPause -> onSaveInstanceState -> onStop -> [ ... ] -> onStart -> onResume   !!!! FALTA onRestoreInstanceState !!!!
   *                                                                               -> onActivityResult
   * cambio a otra app o a teléfono inactivo (sleep)
   *
   *    onPause -> onSaveInstanceState -> onStop -> [ ... ] -> onStart -> onResume   !!!! FALTA onRestoreInstanceState !!!!
   *
   * salir y volver a la apli (que se cierre o no la entrada de lista de aplis no afecta a nada)
   *
   *    onPause -> onStop -> onDestroy -> [ ... ] -> onCreate(null) -> onStart -> onResume
   *
   * el usuario pasa a operar con el desplegable de arriba
   *
   *    onPause -> [ ... ] -> onResume
   */

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt("pagenum", selected());
    // Siempre entre onPause() y onStop(), aunque no se vaya a llamar a onDestroy()
    Log.d(this.getClass().toString() + ".onSaveInstanceState", "...");
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle inState) {
    // Entre onStart() y onResume(), pero paise que solo si ha habido onCreate antes de onStart
    Log.d(this.getClass().toString() + ".onRestoreInstanceState", "...");
    super.onRestoreInstanceState(inState);
  }

  @Override
  protected void onPause() {
    // primer callback, antes de onStop
    Log.d(this.getClass().toString() + ".onPause", "...");
    super.onPause();    // despacha onPause() a fragments
  }

  @Override
  protected void onResume() {
    // volvemos al top de la stack (y tb siempre que antes ocurre onStart)
    Log.d(this.getClass().toString() + ".onResume", "...");
    _dataModel.load();    // may be some file added by receiving sharing intent
    super.onResume();     // dispatch onResume() to fragments
    // since prefs get managed by its own activity, this will be executed always after changed
    Utils.NetPref syncpref = Utils.getSyncPref(this);
    if ( Build.VERSION.SDK_INT < Build.VERSION_CODES.P && syncpref == Utils.NetPref.notroaming )
      // Version code P, Android 9 released august 2018
      show(getString(string.unsupported), Utils.ErrCode.Unsupported);
    if ( Utils.NetPref.never != syncpref)
      registerForSync();
  }

  @Override
  protected void onStop() {
    // otra stack activa
    Log.d(this.getClass().toString() + ".onStop", "...");
    super.onStop();     // despacha onStop() a los fragments
  }

  @Override
  protected void onStart() {
    // la stack vuelve a estar activa (y tb en primera ejecución)
    Log.d(this.getClass().toString() + ".onStart", "...");
    super.onStart();    // despacha onStart() a los fragments
    String action = getIntent().getStringExtra("action");
    if (action != null && action.equals(SyncFragment.class.getSimpleName())) {
      SyncFragment dlg = new SyncFragment();
      dlg.show(getSupportFragmentManager(), dlg.getClass().getSimpleName());
    }
  }

  @Override
  protected void onDestroy() {
    Log.d(this.getClass().toString() + ".onDestroy", "...");
    Job.save();
    super.onDestroy();    // despacha onDestroy() a los fragments
  }

  /**********************
   * adapter
   *********************/

  private class FolderAdapter extends FragmentStateAdapter {

    static public final int NUM_PAGES = 2;
    static public final int FILEFRAG_NUM = 0;
    static public final int DIRFRAG_NUM = 1;

    FolderAdapter(FragmentActivity acti) {
      super(acti);
    }

    @NonNull
    @Override
    public Fragment createFragment(int pos) {
      switch(pos) {
        case FILEFRAG_NUM:
          _current = new FilesFragment();
          break;
        case DIRFRAG_NUM:
        default:
          _current = new DirsFragment();
      }
      // given than in some cases this is called _after_ tablayout listener onTabSelected()
      // and in others _before_, we have to call this in both places
      _current.gotActive();
      return _current;
    }

    @Override
    public int getItemCount() {
      return NUM_PAGES;
    }
  }

}
