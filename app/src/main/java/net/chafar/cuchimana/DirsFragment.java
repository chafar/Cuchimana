/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.List;

/* Cuidado: no usar constructora para pasar nada, pq dice la doc
 * que el diálogo puede ser instanciado por cuestiones de lifecycle
 * por el runtime con la constructora vacía que tiene por defecto
 * y, por tanto, en esos casos nos cascaría.
 */
public class DirsFragment extends ItemsFragment implements FilenameFragment.FilenameListener, AcceptFragment.AcceptListener {

  @Override
  public void onCreate(Bundle savedState) {
    super.onCreate(savedState);
    // Si savedState != null es re-creación ¿hay algo que debamos hacer distinto?
    _actionModeCallback.setFragment(this);
    _actionModeCallback.setMenuId(R.menu.dir_context_menu);
    // restore action mode folder_menu if some selection in place
    if ( _dataModel.usingDirs() && _dataModel.selected() > 0 )
      startActionMode();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    CoordinatorLayout view = (CoordinatorLayout) inflater.inflate(R.layout.folder_fragment, container, false);
    view.setId(R.id.dirfragview);
    RecyclerView rv = view.findViewById(R.id.fragment_view);
    rv.setId(R.id.dirfraglistview);
    final DirsViewAdapter adapter = new DirsViewAdapter(this);
    rv.setAdapter(adapter);
    // use a grid layout manager
    GridLayoutManager mgr = new GridLayoutManager(getContext(), ImageManager.instance().dpWidth() / Constants.SampleSize, RecyclerView.VERTICAL, false);
//    LinearLayoutManager mgr = new LinearLayoutManager(getContext());
    rv.setLayoutManager(mgr);

    if ( _acti instanceof Folder ) {
      FloatingActionButton fab = view.findViewById(R.id.fab);
      fab.setOnClickListener(view1 -> doCommand(R.id.menu_home));
    }
    _dataModel.mgr().dirs().observe(getViewLifecycleOwner(), (Observer<List<File>>) dirs -> {
      if ( _dataModel.usingDirs() )
        _dataModel.clearSelections();
      adapter.resetView();
    });

    return view;
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    // este bundle nos lo presentaran si  hay re-creación, en onCreate(), onCreateView() y onActivityCreated()
  }

  public DirsViewAdapter getAdapter() {
    if ( null != getView() )
      return (DirsViewAdapter) ((RecyclerView) getView().findViewById(R.id.dirfraglistview)).getAdapter();
    return null;
  }

  @Override
  public void gotActive() {
    if ( _dataModel != null )
      _dataModel.useDirs();
  }

  @Override
  protected void adjustActionMenu() {
  }

  @Override
  protected boolean doCommand(int itemId) {
    if ( super.doCommand(itemId) )
      return true;
    Job job = Job.make(itemId);
    if ( itemId == R.id.menu_new_dir ) {
      launchFilenameFragment(R.string.action_newdir, null, null, job);
      return true;
    } else if ( itemId == R.id.menu_home ) {
      _dataModel.home();
      Toast.makeText(getContext(), "changed to: " + Constants.RootDir, Toast.LENGTH_SHORT).show();
      return true;
    } else if ( itemId == R.id.syncdir ) {
      syncDir(job.id, _dataModel.mgr().relPath());
      return true;
    }
    Job.pop();    // unused
    return false;
  }

  @Override
  protected void itemClicked(int pos) {
    // el conclick del adapter debería llamar aki, y aki gestionar el chdir
  }

  public boolean chdir(String name) {
    if ( _dataModel.chdir(name) ) {
      Toast.makeText(getContext(), "changed to: " + name, Toast.LENGTH_SHORT).show();
      ItemsViewAdapter adapter = getAdapter();
      if ( adapter != null )
        adapter.unselect();
    } else {
      Toast.makeText(getContext(), "Uuuuppss: couldn't change to: " + name, Toast.LENGTH_SHORT).show();
      return false;
    }
    return true;
  }

  // manejador del onClick de FilenameFragment
  @Override
  public Boolean onFilenameOkClick(DialogFragment frm, String name) {
    if ( super.onFilenameOkClick(frm, name) )
      return true;

    FilenameFragment dlg = (FilenameFragment) frm;
    Job job = Job.peek();
    if ( job == null ) {
      Log.e(getClass().getSimpleName(), "no job after filename ok click");
      return false;
    }
    if ( job.type == R.id.menu_new_dir ) {
      if ( !Utils.testFileName(name) || name.codePointCount(0, name.length()) > 25 ) {
        Toast.makeText(getContext(), requireContext().getString(R.string.badname) + ": " + name, Toast.LENGTH_SHORT).show();
        return true;
      }
      if ( !_dataModel.mkdir(name) )
        Toast.makeText(getContext(), requireContext().getString(R.string.dirnotcreated) + ": " + name, Toast.LENGTH_SHORT).show();
      else
        autoSyncItem(job, new File(FileManager.instance().relPath(), name).getPath(), SyncJob.CodOp.content);
    } else if ( job.type == R.id.action_rename ) {
      int selpos = _dataModel.firstSelection();
      if ( selpos < _dataModel.selections().size() ) {
        _dataModel.renameDir(selpos, dlg.name());
        autoSyncItem(job, _dataModel.selectedItem(selpos).getPath(), SyncJob.CodOp.renamed);
      }
    } else
      return false;
    commandDone();
    return true;
  }

  @Override
  public void onFilenameCancelled() {
  }

}