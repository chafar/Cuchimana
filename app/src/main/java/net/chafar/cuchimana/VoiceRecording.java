/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.io.File;
import java.io.IOException;


/**
 */
public class VoiceRecording extends DialogFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

  private static final String   ARG_FILEPATH = "filepath";
  private static final String   ARG_JOBID = "jobid";
  private static final int      DISABLED_ALPHA = 0x40;
  private static final int      ENABLED_ALPHA = 0xff;
  // configurable suggested duration (minutes)
  static public final int       DEFAULT_DURATION = 2;
  // absolute max duration admitted (minutes)
  private static final int      MAX_DURATION = 60;

  private VoiceRecordingListener _listener = null;

  private ToggleButton    _pausebut = null;
  private ToggleButton    _stopbut = null;
  private Button          _donebut = null;
  private TextView        _duraview = null;
  private int             _duration;
  private Job             _job;
  private CountDownTimer _refreshtimer = null;
  private RecordViewModel _vmodel;

  /**
   * Higher level listener interface (Activity or the like)
   */
  public interface VoiceRecordingListener {
    void onRecordingDone(Job job, String quilosa);
  }

  /**
   * Use this factory method to create a new instance of this fragment using the provided parameters.
   * La VENTAJA de esto es que se le pueden pasar parámetros, ya que según parece la constructora no se puede hacer con parámetros,
   * o no se puede re-escribir o lo que sea. ASI lo tendríamos que hacer
   * tb en FilenameFragment
   */
  static public VoiceRecording newInstance(String filepath, Job job) {
    VoiceRecording fragment = new VoiceRecording();
    Bundle args = new Bundle();
    args.putString(ARG_FILEPATH, filepath);
    args.putInt(ARG_JOBID, job.id);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    _vmodel = new ViewModelProvider(requireActivity()).get(RecordViewModel.class);
    if ( getArguments() != null ) {
      String val = getArguments().getString(ARG_FILEPATH);
      _vmodel.setFile(new File(val == null ? "esto-no-ocurre-nunca!!!!" : val));
      _job = Job.job(getArguments().getInt(ARG_JOBID));
    }
    _duration = Utils.getPref(requireActivity(), R.string.recdurpref, DEFAULT_DURATION);
  }

 /** A diferencia de FilenamDialog, donde el diálogo subyacente es un AlertDialog
  * y re-escribe onCreateDialog(), aki lo que se re-escribe es onCreateView(),
  * avalado por https://developer.android.com/reference/android/app/DialogFragment
  */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.voice_recording, container, false);
    Button but = view.findViewById(R.id.startstopbut);
    if (but != null) {
      _stopbut = (ToggleButton) but;
      _stopbut.setOnCheckedChangeListener(this);
    }
    but = view.findViewById(R.id.pausebut);
    if (but != null) {
      _pausebut = (ToggleButton) but;
      _pausebut.setOnCheckedChangeListener(this);
    }
    _donebut = view.findViewById(R.id.donebut);
    if (_donebut != null)
      _donebut.setOnClickListener(this);
    but = view.findViewById(R.id.moredura);
    if (but != null)
      but.setOnClickListener(this);
    but = view.findViewById(R.id.lessdura);
    if (but != null)
      but.setOnClickListener(this);
    EditText et = view.findViewById(R.id.recordfilename);
    if (et != null) {
      String name = _vmodel.file().getName();
      if (name.indexOf('-') >= 0)
        name = name.substring(0, _vmodel.file().getName().indexOf('-')) + '.' + Utils.fileExtension(_vmodel.file());
      File tmp = new File(_vmodel.file().getParent(), name);
      et.setText(Utils.fileNameWithoutExt(tmp));
    }
    _duraview = view.findViewById(R.id.duration);
    showDuration();
    showState();

    if (_vmodel.state() != RecordViewModel.RecordState.idle) {
      buildTimer();
      _refreshtimer.start();
    }

    return view;
  }

  @SuppressLint("DefaultLocale")
  private void showDuration() {
    if (_duraview != null)
      _duraview.setText(String.format("%d'", _duration));
  }

  private void showRemaining() {
    if (_duraview != null)
      _duraview.setText(_vmodel.remaining() < 100 ? _vmodel.remaining()+"''" : String.valueOf((_vmodel.remaining()+30)/60)+'\'');
  }

  private void showState() {
    if (getContext() != null) {
      if ( _vmodel.state() != RecordViewModel.RecordState.idle )
        _stopbut.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_record_stop));
      _stopbut.getBackground().setAlpha(_vmodel.state() == RecordViewModel.RecordState.recorded ? DISABLED_ALPHA : ENABLED_ALPHA);
      _donebut.getBackground().setAlpha(_vmodel.state() == RecordViewModel.RecordState.recorded ? ENABLED_ALPHA : DISABLED_ALPHA);
      if ( _pausebut != null ) {
        if ( _vmodel.state() == RecordViewModel.RecordState.paused )
          _pausebut.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_record_resume));
        else
          _pausebut.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_record_pause));
        _pausebut.getBackground().setAlpha(_vmodel.state() == RecordViewModel.RecordState.recording || _vmodel.state() == RecordViewModel.RecordState.paused
            ? ENABLED_ALPHA : DISABLED_ALPHA);
      }
    }
  }

  private void buildTimer() {
    _refreshtimer = new CountDownTimer(200, 10000) {
      @Override
      public void onFinish() {
        showRemaining();
        showState();
        cancel();
        if ( _vmodel.state() != RecordViewModel.RecordState.recorded )
          start();
      }

      @Override
      public void onTick(long millisUntilFinished) {
      }
    };
  }

  @Override
  public void onClick(View vw) {
    if (vw instanceof Button) {
      int id = vw.getId();
      if (id == R.id.donebut){
        View view = getView();
        EditText et = view != null ? (EditText) view.findViewById(R.id.recordfilename) : null;
        if ( et != null && _vmodel.state() == RecordViewModel.RecordState.recorded )
          _listener.onRecordingDone(_job, et.getText().toString());
        else {
          if ( _vmodel.file().exists() )
            _vmodel.file().delete();
          dismiss();
        }
        _vmodel.reset();
      } else if (id == R.id.moredura) {
        _duration += (_duration < 5) ? 1 : 5;
        if ( _duration > MAX_DURATION )
          _duration = MAX_DURATION;
        showDuration();
      } else if (id == R.id.lessdura) {
        _duration -= (_duration <= 5) ? 1 : 5;
        if ( _duration < 1 )
          _duration = 1;
        showDuration();
      }
    }
  }

  @Override
  public void onCheckedChanged(CompoundButton button, boolean checked) {
    ToggleButton but = (ToggleButton) button;
    int id = but.getId();
    if ( id == R.id.startstopbut ) {
      if ( checked ) {
        if ( _vmodel.state() == RecordViewModel.RecordState.idle ) {
          if ( !_vmodel.prepare() ) {
            Toast.makeText(this.getContext(), "Media recorder failed", Toast.LENGTH_LONG).show();
            return;
          }
          _vmodel.start(_duration);
          showState();
          buildTimer();
          _refreshtimer.start();
        }
      } else {
        if ( _vmodel.state() == RecordViewModel.RecordState.recording ) {
          _vmodel.stop();
          showState();
        }
      }
    } else if ( id == R.id.pausebut ) {
      if ( checked )
        _vmodel.pause();
      else
        _vmodel.resume();
      showState();
    }
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    if ( context instanceof VoiceRecordingListener ) {
      _listener = (VoiceRecordingListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement VoiceRecordingListener");
    }
  }

  @Override
  public void onCancel(@NonNull DialogInterface dialog) {
    super.onCancel(dialog);
    if (_vmodel.state() != RecordViewModel.RecordState.idle)
      _vmodel.reset();
    if (_vmodel.file().exists())
      _vmodel.file().delete();
  }

  @Override
  public void onDetach() {
    super.onDetach();
    _listener = null;
    if ( _refreshtimer != null)
      _refreshtimer.cancel();
  }

  @Override
  public void onStart() {
    super.onStart();
    Log.d(getClass().getSimpleName(), "onPause");
  }

  @Override
  public void onPause() {
    super.onPause();
    Log.d(getClass().getSimpleName(), "onPause");
  }

  @Override
  public void onResume() {
    super.onResume();
    Log.d(getClass().getSimpleName(), "onresume");
  }

  @Override
  public void onStop() {
    super.onStop();
    Log.d(getClass().getSimpleName(), "onStop");
  }

  @SuppressWarnings("SameParameterValue")
  static public class RecordViewModel extends AndroidViewModel implements MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener {

    public enum RecordState {
      idle,
      recording,
      paused,
      recorded
    }

    private File            _file = null;
    private RecordState     _state = RecordState.idle;
    private MediaRecorder   _recorder = null;
    private CountDownTimer  _timer = null;
    private int             _remaining = 0; // remaining duration

    @Override
    protected void onCleared() {
      super.onCleared();
      if (_recorder != null) {
        _recorder.reset();
        _recorder.release();
      }
    }

    protected RecordViewModel(Application app) {
      super(app);

    }

    protected void setFile(File file) { _file = file; }

    protected File file() { return _file; }

    private Boolean prepare() {
      if (_recorder == null) {
        _recorder = new MediaRecorder();
        _recorder.setOnInfoListener(this);
      } else
        _recorder.reset();
      _recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
      _recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
      _recorder.setOutputFile(_file.getPath());
      _recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
      try {
        _recorder.prepare();
      } catch ( IOException ex ) {
        Utils.loge(ex, getClass().getSimpleName());
        reset();
        return false;
      }
      return true;
    }

    protected void setState(RecordState st) { _state = st; }

    protected RecordState state() { return _state; }

    protected int remaining() { return _remaining; }

    protected Boolean start(int duration) {
      if (_state == RecordState.idle) {
        prepare();
        _state = RecordState.recording;
        _timer = new CountDownTimer(1000, 10000) {
          @Override
          public void onFinish() {
            if ( state() == RecordViewModel.RecordState.recording && _remaining > 0 )
              --_remaining;
            cancel();
            if ( _remaining == 0 )
              stop();
            else
              start();
          }

          @Override
          public void onTick(long millisUntilFinished) {
          }
        };
        _remaining = duration * 60;
        _timer.start();
        _recorder.start();
        return true;
      }
      return false;
    }

    protected Boolean stop() {
      if (_state == RecordState.recording) {
        _state = RecordState.recorded;
        // cuidado: esto puede lanzar una excepción si no se ha llegao a grabar nada,
        // pq el fichero es inutilizable en ese estado
        try { _recorder.stop(); }
        catch(Exception ex) {
          reset();
          return false;
        }
        return true;
      }
      return false;
    }

    protected void pause() {
      if (_state == RecordState.recording) {
        _recorder.pause();
        _state = RecordState.paused;
      }
    }

    protected void resume() {
      if (_state == RecordState.paused) {
        _recorder.pause();
        _state = RecordState.recording;
      }
    }

    protected void reset() {
      if (_recorder != null)
        _recorder.reset();
      _state = RecordState.idle;
      if ( _timer != null)
        _timer.cancel();
    }

    /*
     * MediaRecorder callbacks
     */

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
      switch(what) {
        case MediaRecorder.MEDIA_ERROR_SERVER_DIED:
          // hay que desechar el mr y construir uno nuevo
        case MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN:
          Log.d(getClass().getSimpleName(), "error callback");
      }
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
      switch(what) {
        // this is for historical interest, since we've discovered that recording
        // pauses still count for setMaxDuration() as consumed time, so we are
        // counting for ourselves, avoiding pauses being counted
        case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
          // there's no guarantee that recording has been already stopped: see setMaxDuration() doc
          setState(RecordViewModel.RecordState.recorded);
          Log.d(getClass().getSimpleName(), "max file duration reached");
          break;
        case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
        case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_APPROACHING:
          // approaching sucede cuando lleva el 90% del fichero.
          // si no se utiliza setNextFile(), cuando llega al 100% para
          // y llega el reached
          // there's no guarantee that recording has been already stopped when 'reached': see setMaxFileSize() doc
          Log.d(getClass().getSimpleName(), "max file size approached/reached");
          break;
      }
    }
  }
}
