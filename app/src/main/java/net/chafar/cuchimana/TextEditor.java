/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import java.io.FileInputStream;
import java.io.FileOutputStream;

// based on NotePad android sdk sample
public class TextEditor extends AppCompatActivity implements TextWatcher, AcceptFragment.AcceptListener {

  private static final int MAX_FILE_LENGTH = 1024 * 1024;

  private static final String EDITOR_CONTENT = "textContent";
  private static final String ORIGINAL_TEXT = "origText";

  // Global mutable variables
  private Uri             _uri;
  private EditText        _edtext;
  private String          _origText;
  private AcceptFragment  _accfrag = null;
  private boolean         _readonly = false;

  /* Heredado del sample NotePad del sdk, pintaba unas rayas, pero es una chorrada,
   * Lo conservo x si interesa para algo. En el layout se pone esta clase específicamente */
  static public class LinedEditText extends AppCompatEditText {
    @SuppressWarnings("FieldCanBeLocal")
    private final Paint _paint;

    // This constructor is used by LayoutInflater
    public LinedEditText(Context context, AttributeSet attrs) {
      super(context, attrs);

      _paint = new Paint();
      _paint.setStyle(Paint.Style.STROKE);
      _paint.setColor(0x800000FF);
    }

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final Intent intent = getIntent();
    final String action = intent.getAction();
    if (action != null && action.equals(Intent.ACTION_EDIT) )
      _uri = intent.getData();
    else if (action != null && action.equals(Intent.ACTION_VIEW)) {
      _uri = intent.getData();
      _readonly = true;
    } else {
      Log.e(this.getClass().getSimpleName(), "Unknown action, exiting: " + action);
      finish();
      return;
    }

    setContentView(R.layout.activity_tex_editor);

    Toolbar myToolbar = findViewById(R.id.toolbar);
    setSupportActionBar(myToolbar);
    _edtext = findViewById(R.id.textpad);
    _edtext.addTextChangedListener(this);
    if (_readonly)
      _edtext.setKeyListener(null);
    _origText = null;

    if ( savedInstanceState != null ) {
      _edtext.setText(savedInstanceState.getString(EDITOR_CONTENT));
      _origText = savedInstanceState.getString(ORIGINAL_TEXT);
    } else {
      String schema = _uri.getScheme();
      if (schema != null && schema.equals("content")) {
        String mime = getContentResolver().getType(_uri);
        if (mime != null && mime.equals(Constants.TextMime)) {
          Cursor csr = getContentResolver().query(_uri, null, null, null, null);
          if (csr != null) {
            if ( csr.moveToFirst()) {
              int colidx = csr.getColumnIndex("_size");
              if (0 <= colidx) {
                int size = csr.getInt(colidx);
                if ( MAX_FILE_LENGTH > size ) {
                  ParcelFileDescriptor fd = null;
                  try {
                    fd = getContentResolver().openFileDescriptor(_uri, "r");
                  } catch ( Exception ex ) {
                    ex.printStackTrace();
                  }
                  if ( fd != null ) {
                    FileInputStream fis = new FileInputStream(fd.getFileDescriptor());
                    byte[] bytes = Utils.loadFile(fis, size);
                    try {
                      fis.close();
                    } catch ( Exception ex ) {
                      ex.printStackTrace();
                    }
                    _edtext.setText(new String(bytes));
                    _origText = null;   // setText() has fired beforeTextChanged()
                  }
                }
              }
            }
            csr.close();
          }
        }
      } else
        Log.e(this.getClass().getSimpleName(), "Bad Uri scheme: " + schema);
    }
  }

  public boolean modified() {
    return _origText != null;
  }

  @Override
  public void onAcceptCancelClick() {
  }

  @Override
  public void onAcceptOkClick(DialogFragment dialog) {
    AcceptFragment dlg = (AcceptFragment) dialog;
    int i = dlg.jobId();
    if ( i == 0 ) {
      setResult(RESULT_CANCELED);
      finish();

      _edtext.setText(_origText);
      _origText = null;
      invalidateOptionsMenu();
    } else if ( i == R.id.menu_revert ) {
      _edtext.setText(_origText);
      _origText = null;
      invalidateOptionsMenu();
    }
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    if (!modified())
      invalidateOptionsMenu();
    _origText = _edtext.getText().toString();
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) { }

  @Override
  public void afterTextChanged(Editable s) { }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate folder_menu from XML resource
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.tex_editor_menu, menu);

    // Append items for any other activities that can do stuff this content as well.
    // This queries activities on the system that implement the ALTERNATIVE_ACTION
    // for our data, adding an item for each one that is found.
    Intent intent = new Intent();
    intent.setDataAndType(_uri, Constants.TextMime);
    intent.addCategory(Intent.CATEGORY_ALTERNATIVE);
    menu.addIntentOptions(Menu.CATEGORY_ALTERNATIVE, 0, 0,
        new ComponentName(this, TextEditor.class), null, intent, 0, null);

    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    // because setEnabled doesn't changes icon's appearence, we simply hide item when not needed
    menu.findItem(R.id.menu_revert).setVisible(modified());
    menu.findItem(R.id.menu_save).setVisible(modified());
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle all of the possible folder_menu actions.
    int itemId = item.getItemId();
    if ( itemId == R.id.menu_save ) {
      String text = _edtext.getText().toString();
      if ( !save(text) )
        Toast.makeText(this, R.string.couldnt_complete_action, Toast.LENGTH_LONG).show();
      else
        invalidateOptionsMenu();
    } else if ( itemId == R.id.menu_revert ) {
      confirm(R.id.menu_revert);
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    if (modified())
      confirm(0);
    else
      super.onBackPressed();
  }

  /* called when Activity have lose focus and is killed afterwards.
   * not called when the user simply navigates away from the Activity */
  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    outState.putString(EDITOR_CONTENT, _edtext.getText().toString());
    if (_origText != null)
      outState.putString(ORIGINAL_TEXT, _origText);
    super.onSaveInstanceState(outState);
  }

  private boolean save(String text) {

    ParcelFileDescriptor fd = null;
    try { fd = getContentResolver().openFileDescriptor(_uri, "w"); } catch ( Exception ex ) { ex.printStackTrace(); }
    if ( fd != null ) {
      FileOutputStream fos = new FileOutputStream(fd.getFileDescriptor());
      try { fos.write(text.getBytes()); } catch (Exception ex) { ex.printStackTrace(); }
      try { fos.close(); } catch(Exception ex) { ex.printStackTrace(); }
      _origText = null;
      return true;
    }
    return false;
  }

  private void confirm(int action) {
    if (modified()) {
      if ( _accfrag == null ) {
        _accfrag = new AcceptFragment();
        _accfrag.setListener(this);
        _accfrag.setJobId(action);
        _accfrag.setTitle(R.string.confirm_editor_exit);
      }
      _accfrag.show(getSupportFragmentManager(), "AcceptFragment");
    }
  }

}
