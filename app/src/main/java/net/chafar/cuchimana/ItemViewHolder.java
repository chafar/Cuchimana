/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public abstract class ItemViewHolder extends RecyclerView.ViewHolder {
  public final CardView   _card;
  public ImageView _imgView = null;
  private ColorStateList  _backgr;

  protected ItemViewHolder(View view) {
    super(view);
    _card = (CardView) view;
    _backgr = null;
  }

  public boolean hasSelection() {
    return _backgr != null;
  }

  public void toggleSelected() {
    if (_card.isSelected()) {
      _card.setSelected(false);
      _card.setCardBackgroundColor(_backgr.getDefaultColor());
      _backgr = null;
    } else {
      if (_backgr == null)
        _backgr = _card.getCardBackgroundColor();
      _card.setSelected(true);
      _card.setCardBackgroundColor(_card.getResources().getColor(R.color.selected, null));
    }
  }

  public void select(Boolean yn) {
    if (yn != hasSelection())
      toggleSelected();
  }

  public void mark(Boolean yn) {
    CheckBox mark = _card.findViewById(R.id.checkBox);
    if (mark != null) {
      mark.setChecked(yn);
    }
  }

}
