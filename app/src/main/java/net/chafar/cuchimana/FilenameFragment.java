/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilenameFragment extends DialogFragment {

  // Use this instance of the interface to deliver action events
  private FilenameListener _listener = null;
  private String            _name = null;
  private @StringRes int    _tit = R.string.action_newdir;
  private Boolean           _accepted = false;
  private Boolean           _cancelled = false;
  private String            _savedText = null;
  private Job               _job = null;
  private boolean           _forfile = false;
  private final ArrayList<String> _sugs = new ArrayList<>();

  // Esto permite ajustarse la iface como interese
  public interface FilenameListener {
    Boolean onFilenameOkClick(DialogFragment dialog, String name);
    void onFilenameCancelled();
  }

  public void setListener(FilenameListener listener) { _listener = listener; }

  public String name() { return _name; }

  public void setJob(Job job) {
    _job = job;
  }

  /// pass isfile as false if name is for directory, true if for file
  public void setName(String name, boolean isfile) {
    _name = name;
    _forfile = isfile;
  }

  public void setTitle(@StringRes int titId) {
    if (getDialog() != null)
      getDialog().setTitle(titId);
    else
      _tit = titId;
  }

  private void doOnShowDialog(AlertDialog dlg) {
    AutoCompleteTextView tv = dlg.findViewById(R.id.filename);
    if (_savedText != null) {
      tv.setText(_savedText);
    } else if (_name != null)
      tv.setText(_name);
    if (_name != null && _name.isEmpty())
      dlg.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
    tv.setOnFocusChangeListener((v, hasFocus) -> {
      if ( hasFocus ) {
        Dialog dl = FilenameFragment.this.getDialog();
        if (null != dl)
          dl.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
      }
    });
    tv.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) { }
      @Override
      public void afterTextChanged(Editable s) {
        dlg.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(evalName(s.toString()));
        fillSuggestions( tv.getText().toString() );
        @SuppressWarnings("unchecked")
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) tv.getAdapter();
        adapter.clear();
        adapter.addAll(_sugs);
        adapter.notifyDataSetChanged();
        AutoCompleteTextView tv = dlg.findViewById(R.id.filename);
      }
    });
    dlg.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(_name != null && evalName(_name));
    fillSuggestions(tv.getText().toString());
    ArrayAdapter<String> adapter = new ArrayAdapter<>(dlg.getContext(), android.R.layout.simple_list_item_1, _sugs);
    tv.setAdapter(adapter);
    // number of chars to show suggestions
    tv.setThreshold(1);
    if (_sugs.size() > 0)
      tv.showDropDown();
  }

  boolean evalName(String name) {
    boolean enable = name != null && !name.isEmpty();
    if (enable && _job != null) {
      String path = _job.get(Job.StoreCode.file, null);
      if (null != path) {
        if (!Utils.testFileName(name))
          enable = false;
        else {
          String ext = Utils.fileExtension(path);
          File file = new File(new File(path).getParent(), name + '.' + ext);
          if ( file.exists() && !file.getPath().equals(path) )
            enable = false;
        }
      }
    }
    return enable;
  }

  void fillSuggestions(String name) {
    _sugs.clear();
    // http://tutorials.jenkov.com/java-regex/index.html
    if (_forfile) {
      String fnamroot, match, tmp;
      Pattern ptn;
      Matcher mcher;
      FileManager mgr = FileManager.instance();
      List<File> files = mgr.files().getValue();
      // part of the name to be included in the regex pattern(s)
      String subname = name.replaceAll("[-|\\.]", "-");
      if (subname.contains("-"))
        subname = subname.substring(0, subname.indexOf('-'));
      // each entry in pattern list should find matches different to those found by other patterns
      ArrayList<String> ptnlist = new ArrayList<>(Collections.singletonList("(^%s.*)[-\\.]"));
      if ( files != null ) {
        for ( String iptn : ptnlist ) {
          ptn = Pattern.compile( String.format( iptn, subname) );
          for ( int ii = 0; ii < files.size(); ++ii ) {
            tmp = files.get(ii).getName();
            fnamroot = tmp.substring(0, tmp.lastIndexOf('.'));
            mcher = ptn.matcher(fnamroot);
            while ( mcher.find() && tmp != null) {
              match = tmp.substring(mcher.start(), mcher.end());
              Log.d(getClass().getSimpleName(), "________ group(0): " + mcher.group(1));
              if (!match.equals(name) && !match.equals(subname) && !_sugs.contains(match))
                _sugs.add(match);
              if (mcher.groupCount() > 0) {
                tmp = mcher.group(1);
                if (tmp != null && !tmp.equals(name) && !tmp.equals(subname) && !_sugs.contains(tmp))
                  _sugs.add(mcher.group(1));
              }
            }
          }
        }
      }
      if (_sugs.size() > 0) {
        _sugs.sort(Comparator.comparingInt(String::length));
      }
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(STYLE_NORMAL, R.style.AppTheme);
  }

  @SuppressLint("InflateParams")
  @NonNull
  @Override
  public Dialog onCreateDialog(@Nullable Bundle savedState) {

    // this is called every time you call show() on Fragment

    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    LayoutInflater inflater = requireActivity().getLayoutInflater();
    builder.setView(inflater.inflate(R.layout.filename, null, false));
    // si se pulsa ok, se actualiza _name con el valor del control
    builder.setPositiveButton(R.string.ok, this::onDialogOkClick);
    builder.setNegativeButton(R.string.cancel, (dialog, which) -> _cancelled = true);

    _accepted = false;
    AlertDialog dlg = builder.create();
    if (savedState != null) {
      _tit = savedState.getInt("title");
      int jobid = savedState.getInt("jobid", -1);
      if (0 > jobid)
        _job = Job.job(jobid);
      _name = savedState.getString("name");
      // don't know why we can't find TextView here to set its text, so we keep saved TextView text
      _savedText = savedState.getString("text");
      _forfile = savedState.getBoolean("forfile", false);
    }
    dlg.setTitle(_tit);
    dlg.setOnShowListener(dialogInterface -> {
      if (dialogInterface.getClass() == AlertDialog.class)
        doOnShowDialog((AlertDialog)dialogInterface);
    });
    // get more space for suggestions over the screen keyboard
    Window window = dlg.getWindow();
    WindowManager.LayoutParams wlp = window.getAttributes();
    wlp.gravity = Gravity.TOP;
    window.setAttributes(wlp);
    return dlg;
  }

  private void onDialogOkClick(DialogInterface dialogInterface, int i) {
    if ( null != getDialog() ) {
      TextView tv = getDialog().findViewById(R.id.filename);
      if (null != tv) {
        _name = tv.getText().toString();
//        _name = _name.replace('-', '_');
        _accepted = true;
      }
    }
  }

  @Override
  public void onCancel(@NonNull DialogInterface dialog) {
    _cancelled = true;
    super.onCancel(dialog);
  }

  @Override
  public void onStop() {
    super.onStop();
    // si no se ha pulsado botón ok, _name tiene el valor inicial
    if (_listener != null) {
      if (_accepted)
        _listener.onFilenameOkClick(this, _name);
      else if (_cancelled)
        _listener.onFilenameCancelled();
    }
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    if (_job != null)
      outState.putInt("jobid", _job.id);
    outState.putInt("title", _tit);
    outState.putCharSequence("name", _name);
    outState.putBoolean("forfile", _forfile);
    Dialog dlg = getDialog();
    if (dlg != null) {
      TextView tv = dlg.findViewById(R.id.filename);
      if ( tv != null )
        outState.putCharSequence("text", tv.getText().toString());
    }
  }
}
// see https://developer.android.com/guide/topics/ui/dialogs#CustomLayout
