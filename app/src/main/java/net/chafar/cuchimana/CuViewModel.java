/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKey;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/* El sentido de esto: los datos
*
* Una de sus características es que son ajenas al ciclo de vida de
* activity/fragment, p.e., retienen datos en los cambios de orientación.
*
* Tb existe ViewModel, que no tiene acceso al application context.
*/

@SuppressWarnings("unused")
public class CuViewModel extends AndroidViewModel {

  final private SharedPreferences _state;
  /// see Constants.EncryptedFile declaration for info
  final private FileManager       _provider;
  final private Vector<Boolean>   _sels;
  final private List<File>        _marks;
  private boolean                 _usingDirs;     // instruct to use dirs for selections
  ArrayList<String>               _backsels;

  // ATTENTION: if this is not public, it won't even install in some devices
  public CuViewModel(@NonNull Application app) {
    super(app);
    _state = app.getSharedPreferences(Constants.StateFile, Context.MODE_PRIVATE);
    FileManager.create(app, _state.getString(Constants.CurrDirKey, null));
    _provider = FileManager.instance();
    _sels = new Vector<>();
    _usingDirs = false;
    _marks = new ArrayList<>();
    EncryptedSharedPreferences encprefstmp;
    try {
      MasterKey masterKey = new MasterKey.Builder(app).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build();
      encprefstmp = (EncryptedSharedPreferences) EncryptedSharedPreferences.create(app, Constants.EncryptedPrefs, masterKey, EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV, EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
    } catch ( GeneralSecurityException | IOException ignored ) { encprefstmp = null; }
    Utils.setEncryptedPrefs(encprefstmp);
    // this is done here as the best way of doing only once in an app living
    createNotificationChannel(app);
  }

  FileManager mgr() { return _provider; }

  private void createNotificationChannel(@NonNull Application app) {
    // Create the NotificationChannel, but only on API 26+ because
    // NotificationChannel class is new and not in support library
    if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationManager notificationManager = app.getSystemService(NotificationManager.class);
      if (null == notificationManager.getNotificationChannel(app.getClass().getSimpleName())) {
        CharSequence name = app.getString(R.string.app_name);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(app.getClass().getSimpleName(), name, importance);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        notificationManager.createNotificationChannel(channel);
      }
    }
  }

  @Override
  public void onCleared() {
    // esto se llama al destruirse el ViewModel, que es remanente a traves
    // de todo el lifecycle. despues de esto, la constructora volverá a ser
    // llamada. Ocurre cuando das tecla atrás, aunque luego te encuentres
    // la apli todavía abierta en la lista de aplicaciones
    SharedPreferences.Editor stated =  _state.edit();
    stated.putString(Constants.CurrDirKey, _provider.relPath());
    stated.apply();
  }

  public void home() {
    _provider.home();
    load();
  }

  public boolean chdir(String dirname) {
    if ( _provider.chdir(dirname) ) {
      load();
      return true;
    }
    return false;
  }

  public boolean mkdir(String dirname) {
    if ( _provider.mkdir(dirname) ) {
      load();
      return true;
    }
    return false;
  }

  /// imports file into current dir
  @SuppressWarnings("WeakerAccess")
  public boolean importFile(String path, String name, @Nullable String ext) {
    int dotpos = path.lastIndexOf('.');
    if (0 > dotpos) {
      if (null == ext) {
        Log.d(getClass().getSimpleName(), ".importFile failed: no file extension");
        return false;
      }
    } else
      ext = path.substring(path.lastIndexOf('.'));
    File dst = new File(mgr().relPath(), name + '.' + ext);
    if (!FileManager.copyFile(path, dst.getPath()))
      return false;
    load();
    return true;
  }

  /// imports content uris into current dir
  public boolean importLocalUri(Uri uri, String name, Context ctx) {
    boolean ok = true;
    String mime = ctx.getContentResolver().getType(uri);
    String scheme = uri.getScheme();
    if (scheme == null)
      return false;
    if (scheme.equals("content")) {
      File dest = new File( FileManager.absPath(getPathForUri(uri, name, ctx)) );
      Log.d("onFilenameOkClick: ", "Picking '" + uri.toString() +"' to " + dest.getPath());
      ParcelFileDescriptor fd = null;
      try {
        fd = ctx.getContentResolver().openFileDescriptor(uri, "r");
      } catch ( Exception ex ) {
        ex.printStackTrace();
      }
      if ( null != fd ) {
        if ( !FileManager.copyFile(fd, dest) )
          ok = false;
      }
    } else if (scheme.equals("file")) {
      if ( uri.getPath() == null || !importFile(uri.getPath(), name, Utils.getExtensionFromMimeType(mime)) )
        return false;
    } else {
      Log.d("onFilenameOkClick: ", "No esperaba action_pick con uri de tipo '" + uri.getScheme() +'\'');
      return false;
    }
    load();
    return ok;
  }

  public String getPathForUri(Uri uri, String name, Context ctx) {
    String mime = ctx.getContentResolver().getType(uri);
    String ext = Utils.getExtensionFromMimeType(mime);
    return new File(mgr().relPath(), name + '.' + ext).getPath();
  }

  public void load() {
    backupSels();
    _provider.load();
    restoreSels();
  }

  public void useDirs() {
    if (!_usingDirs) {
      _usingDirs = true;
      clearSelections();
    }
  }

  public void useFiles() {
    if (_usingDirs) {
      _usingDirs = false;
      clearSelections();
    }
  }

  int pos(String name) {
    ArrayList<File> items = _usingDirs ? _provider.dirList() : _provider.fileList();
    for (int ipos=0; ipos<items.size(); ++ipos)
      if (items.get(ipos).getName().equals(name))
        return ipos;
    return items.size();
  }

  public boolean usingFiles() { return !_usingDirs; }

  public boolean usingDirs() { return _usingDirs; }

  public File file(int pos) { return _provider.file(pos); }

  public boolean removeItems() {
    boolean res = true;
    Log.d(getClass().getSimpleName(), ".removeItems ");
    Vector<Boolean> sels = selections();
    for ( int ii = 0; ii<sels.size(); ++ii )
      if ( sels.get(ii)) {
        if (_usingDirs && !_provider.removeDir(ii)
            || usingFiles() && !_provider.removeFile(ii)) {
          res = false;
          break;
        }
        if (usingFiles())
          removeSamples(new File(FileManager.absPath(_provider.relFilePath(ii))));
        else
          removeSamples(new File(FileManager.absPath(_provider.relDirPath(ii))));
      }
    load();
    return res;
  }

  public boolean renameFile(File file, String name) {
    if (_provider.renameFile(file, name)) {
      load();
      return true;
    }
    return false;
  }

  public boolean renameFile(int pos, String name) {
    return renameFile(selectedItem(pos), name);
  }

  public void removeSamples(File file) { _provider.removeSamples(file); }

  public void renameDir(int pos, String name) {
    File dir = selectedItem(pos);
    _provider.renameDir(dir, name);
    load();
  }

  /* Selections API
   */

  public Vector<Boolean> selections() {
    return _sels;
  }

  /// returns true if pos valid
  public boolean selectItem(int pos) {
    if (pos < 0 || pos >= _sels.size())
      return false;
    _sels.set(pos, true);
    return true;
  }

  /// returns true if pos valid
  public boolean unSelectItem(int pos) {
    if (pos < 0 || pos >= _sels.size())
      return false;
    _sels.set(pos, false);
    return true;
  }

  /// deselects all
  public void unselect() {
    int ii;
    for ( ii = 0; ii < _sels.size(); ++ii )
      if ( _sels.get(ii) )
        _sels.set(ii, false);
  }

  /** resizes _sels and sets all selections to false
   * sets new size with respect to _files/_dirs depending on _usingDirs
   */
  public void clearSelections() {
    _sels.clear();
    List<File> content = _usingDirs ? _provider.dirList() : _provider.fileList();
    for(int ii = 0; ii<content.size(); ++ii )
      _sels.add(false);
    Log.d(getClass().getSimpleName(), "clearSelections: " + _sels.size() + (_usingDirs ? " dirs" : " files"));
  }

  public void selectAll() {
    for(int ii=0; ii<_sels.size(); ++ii )
      _sels.set(ii,true);
  }

  public Boolean selected(int pos) {
    return (pos >= 0 && pos < _sels.size()) && _sels.get(pos);
  }

  public int firstSelection() {
    int ii;
    for (ii=0; ii<_sels.size(); ++ii)
      if (_sels.get(ii))
        return ii;
    return ii;
  }

  void backupSels() {
    _backsels = new ArrayList<>();
    for (int ii=0; ii<_sels.size(); ++ii)
      if (_sels.get(ii))
        _backsels.add(name(ii));
  }

  void restoreSels() {
    clearSelections();
    int pos, count = _usingDirs ? _provider.dirsCount() : _provider.filesCount();
    for (String name : _backsels) {
      pos = pos(name);
      if (pos < count)
        selectItem(pos);
    }
  }

  public String name(int pos) {
    if (usingFiles() && pos < _provider.fileList().size())
      return _provider.fileList().get(pos).getName();
    else if (usingDirs() && pos < _provider.dirList().size())
      return _provider.dirList().get(pos).getName();
    return "";
  }

  public File selectedItem(int pos) {
    if (usingDirs())
      return _provider.dir(pos);
    return _provider.file(pos);
  }

  public Vector<File> selectedItems() {
    Vector<File> items = new Vector<>();
    List<File> list = usingDirs() ? _provider.dirList() : _provider.fileList();
    for (int ii=0; ii<list.size(); ++ii) {
      if (selected(ii))
        items.add(list.get(ii));
    }
    return items;
  }

  public int selected() {
    int count = 0;
    for (int ii=0; ii<_sels.size(); ++ii)
      if (_sels.get(ii))
        ++count;
    return count;
  }

  /*
   * marks management
   */

  public void unmark() { _marks.clear(); }

  public void markSelected() {
    for ( File entry : selectedItems() )
      if (!_marks.contains(entry))
        _marks.add(entry);
  }

  public void unmarkSelected() {
    for ( File entry : selectedItems() )
      _marks.remove(entry);
  }

  public int marked() { return _marks.size(); }

  public boolean marked(File entry) {
    return _marks.contains(entry);
  }

  public boolean marked(int pos) {
    File item = selectedItem(pos);
    return item != null && _marks.contains(item);
  }

  /// returns copy of internal marks list
  public List<File> marks() {
    return new ArrayList<>(_marks);
  }
}

