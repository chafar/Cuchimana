/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayDeque;

/** Synchronization jobs management
 *
 * This is not thread-safe: you should only use it within main/UI thread
 */
class SyncJob {
  public enum CodOp {
    content,
    renamed,
    removed,
    test,
    queue
  }
  protected CodOp code;
  public String path;
  public String oldpath;
  public SyncService.SSHParams params;
  public SyncService.SyncResult res;
  protected String privkeypass;
  static private final JobList _queue = new JobList();

  static public SyncJob make(CodOp code, String path) {
    SyncJob job = new SyncJob(code, path);
    _queue.add(job);
    return job;
  }

  static boolean add(SyncJob job) {
    return _queue.add(job); }

  static SyncJob poll() { return _queue.poll(); }

  static SyncJob peek() { return _queue.peek(); }

  static boolean empty() { return _queue.isEmpty(); }

  static int size() { return _queue.size(); }

  static boolean isOngoing(String path) {
    for (SyncJob job : _queue)
      if ( job.res.errcode == Utils.ErrCode.Ongoing
            && path.equals(job.path) )
        return true;
    return false;
  }

  static private String savePath() {
    return new File(FileManager.instance().userDir(), "SyncJobQueue.txt").getPath();
  }

  static boolean save() {
    String path = savePath();
    try {
      FileWriter writer = new FileWriter(path);
      for ( SyncJob job : _queue )
        if ( job.code != CodOp.queue && job.code != CodOp.test )
          writer.write(job.toString());
      writer.close();
    }
    catch ( Exception ex ) {
      return false;
    }
    return true;
  }

  static boolean load(Context ctx) {
    SyncJob job;

    File file = new File(savePath());
    byte[] bytes = Utils.loadFile(file);
    if (bytes.length == 0)
      return false;

    boolean existing;
    String content = new String(bytes);
    job = new SyncJob(CodOp.test, null);
    int from=0;
    char ch;
    String name = null;
    String value;
    for (int pos=0; pos<content.length(); pos++ ) {
      ch = content.charAt(pos);
      if ( ch == '=' )
        if ( pos > from ) {
          name = content.substring(from, pos);
          from = pos + 1;
        } else
          return false;
      if ( ch == ';' || ch == '\n' ) {
        if ( name != null && pos > from ) {
          value = content.substring(from, pos);
          from = pos + 1;
          switch (name) {
            case "code": job.code = CodOp.valueOf(value); break;
            case "path": job.path = value; break;
            case "oldpath": job.oldpath = value; break;
            case "res": job.res = new SyncService.SyncResult(Utils.ErrCode.valueOf(value)); break;
          }
          if (ch == '\n') {
            // register job
            if (job.code != CodOp.test && job.code != CodOp.queue) {
              job.params = SyncService.getSyncPrefs(ctx);
              job.privkeypass = Utils.getEncryptedPref(Constants.PrivKeyPass, null);
              existing = false;
              for ( SyncJob existingjob : _queue )
                if ( job.matches(existingjob) ) {
                  existing = true;
                  break;
                }
              if ( !existing ) {
                // ongoing result shouldn't be found here, except due to some crash:
                // solve the issue here to avoid this job act as stopper for queue processing
                if (job.res.errcode == Utils.ErrCode.Ongoing)
                  job.res.errcode = Utils.ErrCode.Delayed;
                _queue.add(job);
              }
            }
            job = new SyncJob(CodOp.test, null);
          }
        }
        name = null;
      }
    }

    return true;
  }

  private SyncJob(CodOp code, String path) {
    this.code = code;
    this.path = path;
    this.res = new SyncService.SyncResult(Utils.ErrCode.Ok);
  }

  public boolean matches(final SyncJob job) {
    return code == job.code && path.equals(job.path)
        && (oldpath == null && job.oldpath == null
            || oldpath != null && job.oldpath != null && oldpath.equals(job.oldpath))
        && params.user.equals(job.params.user) && params.host.equals(job.params.host) && params.port == job.params.port;
  }

  @NonNull
  @Override
  public String toString() {
    String str = "code=" + code.toString();
    if (path != null)
      str += ';' + "path=" + path;
    if (oldpath != null)
      str += ';' + "oldpath=" + oldpath;
    if (res != null)
      str += ';' + "res=" + res.code().toString();
    str += '\n';
    return str;
  }

  private static class JobList extends ArrayDeque<SyncJob> {

  }
}
