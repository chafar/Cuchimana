/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Vector;

public abstract class ItemsFragment extends FragmentBase implements FilenameFragment.FilenameListener, AcceptFragment.AcceptListener {

  protected File            _intentFile;
  private Uri               _uri;
  // private because intended only to use just one instance
  private FilenameFragment  _fnfrag = null;
  private AcceptFragment    _okfrag = null;
  protected final ItemsFragment.AmCallback _actionModeCallback = new AmCallback();

  abstract protected void itemClicked(int pos);

  abstract protected void adjustActionMenu();

  public ItemsViewAdapter getAdapter() {
    if (getView() != null) {
      RecyclerView view = getView().findViewById(R.id.filefraglistview);
      if ( view != null )
        return (ItemsViewAdapter) view.getAdapter();
    }
    return null;
  }

  public void someItemSelected() {
    if (_actionMode == null)
      startActionMode();
    else {
      _actionMode.getMenu().clear();
      _actionMode.invalidate();
    }
    adjustActionMenu();
  }

  public void someItemUnSelected() {
    if ( _actionMode != null) {
      if ( 0 == _dataModel.selected() )
        _actionMode.finish();
      else {
        _actionMode.getMenu().clear();
        _actionMode.invalidate();
        adjustActionMenu();
      }
    }
  }

  protected void launchFilenameFragment(int titres, @Nullable File file, @Nullable Uri uri, @NonNull Job job) {
    _uri = uri;
    if ( _fnfrag == null ) {
      _fnfrag = new FilenameFragment();
      _fnfrag.setListener(this);
    }
    _fnfrag.setTitle(titres);
    if (file != null) {
      String name = file.getName();
      if (0 < name.indexOf('-'))
        _fnfrag.setName(name.substring(0, name.lastIndexOf('-')), _dataModel.usingFiles());
      else if (0 <= name.indexOf('.'))
        _fnfrag.setName(name.substring(0, name.lastIndexOf('.')), _dataModel.usingFiles());
      else
        _fnfrag.setName(name, _dataModel.usingFiles());
    }
    _fnfrag.setJob(job);
    // this call involves that activity will re-create the dialog on configuration changes
    _fnfrag.show(requireActivity().getSupportFragmentManager(), "FileNameFragment");
  }

  public void startActionMode() {
    if (_actionMode == null) {
      _actionMode = requireActivity().startActionMode(_actionModeCallback);
    }
  }

  protected boolean doCommand(int itemId) {
    if ( _dataModel.selected() < 1 && (itemId == R.id.action_remove || itemId == R.id.action_rename) ) {
      Log.e(getClass().getSimpleName(), "trying to do command with no selection");
      return true;
    }
    Job job = Job.make(itemId);
    if ( itemId == R.id.action_remove ) {
      if ( _okfrag == null ) {
        _okfrag = new AcceptFragment();
        _okfrag.setListener(this);
      }
      _okfrag.setActionCode(itemId);
      _okfrag.setJobId(job.id);
      _okfrag.setTitle(R.string.confirm_remove);
      if (_dataModel.selected() > 1)
        _okfrag.setText(getString(R.string.someitems));
      else {
         Vector<File> files = _dataModel.selectedItems();
         _okfrag.setText(files.get(0).getName());
      }
      _okfrag.show(requireActivity().getSupportFragmentManager(), "AcceptFragment");
      return true;
    } else if ( itemId == R.id.action_rename ) {
      if ( _dataModel.selected() > 1 ) {
        show(getString(R.string.toomanysels), Utils.ErrCode.Unknown);
        return true;
      }
      int selpos = _dataModel.firstSelection();
      job.put(Job.StoreCode.file, _dataModel.file(selpos).getPath());
      launchFilenameFragment(R.string.action_rename, _dataModel.selectedItem(selpos), null, job);
      return true;
    } else if ( itemId == R.id.action_mark ) {
      _dataModel.markSelected();
      getAdapter().unselect();
      getAdapter().refreshView();
      return true;
    }
    Job.pop();   // unused
    return false;
  }

  boolean commandDone() {
    Job job = Job.pop();
    return job != null;
  }

  /* This processes both our intent picking a file through an external
   * file manager and the external intent sharing a file
   */
  void receiveFile(Intent intent) {
    ArrayList<Uri> uris = new ArrayList<>();
    Uri uri = intent.getData();
    if (uri != null)
      uris.add(uri);
    ClipData data = intent.getClipData();
    if (data != null)
      for (int ii=0; ii< data.getItemCount(); ++ii) {
        uri = data.getItemAt(ii).getUri();
        if ( uri != null )
          uris.add(uri);
      }
    String name;
    Job job = Job.peek();
    if (uris.size() > 0) {
      // int jobid = intent.getIntExtra("jobid", 0);
      if ( uris.size() == 1 ) {
        // ask for filename only if just one uri
        uri = uris.get(0);
        Log.d(getClass().getSimpleName(),"encoded path: " + uri.toString());
        // empty name works: we only need file extension for FileNameFragment to be able to compare resulting path with existing files
        job.put(Job.StoreCode.file, FileManager.absPath(_dataModel.getPathForUri(uri, "", requireContext())));
        launchFilenameFragment(R.string.action_pick, new File(job.get(Job.StoreCode.file, "")), uri, job);
      } else {
        ///////////// CUIDADO: aki CREO que no se pueden validar los nombres de fichero que vienen en las uris,
        // x lo que habría que pedir nombres para todos: UUUUUuuuuupppppsssss !!!!
        // ===>>> realmente, btm, no hacemos nada con esto
        for (Uri thisuri : uris) {
          try { name = URLDecoder.decode(thisuri.getPath(), "UTF-8"); }
          catch (Exception UnsupportedEncodingException) {
            show(getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
            continue;
          }
          name = new File(name).getName();
          if (getContext() != null) {
            if ( !_dataModel.importLocalUri(thisuri, name, getContext()) )
              show(getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
          }
        }
        if ( requireActivity() instanceof Receive )
          ((Receive) requireActivity()).workDone();
      }
    }
  }

  @Override
  public Boolean onFilenameOkClick(DialogFragment dialog, String name) {
    // we to have preserve the job for subclasses if we don't consume it.
    Job job = Job.peek();
    if (job == null) {
      Log.e(getClass().getSimpleName(), "no job after filename ok click");
      return false;
    }
    if ( job.type == R.id.menu_pick ) {
      if (_uri == null || getContext() == null || !_dataModel.importLocalUri(_uri, name, getContext()))
        show(getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
      else
        autoSyncItem(job, _dataModel.getPathForUri(_uri, name, getContext()), SyncJob.CodOp.content);
      commandDone();
      if ( requireActivity() instanceof Receive )
        ((Receive) requireActivity()).workDone();
      return true;
    }
    return false;
  }

  void autoSyncItem(Job job, String path, SyncJob.CodOp code) {
    String pref = Utils.getPref(requireActivity(), R.string.autosyncpref, "off");
    if ( pref != null && !pref.equals("off") ) {
      Intent intent = getSyncIntent(job.id, path, code);
      requireActivity().startService(intent);
    }
  }

  protected void autoRenameSyncItem(int jobid, String path, String oldpath) {
    String pref = Utils.getPref(requireActivity(), R.string.autosyncpref, "off");
    if ( pref != null && !pref.equals("off") ) {
      Intent intent = getRenameSyncIntent(jobid, path, oldpath);
      requireActivity().startService(intent);
    }
  }

  protected void syncFile(int jobid, String path) {
    File file = new File(FileManager.absPath(path));
    if ( file.isDirectory() ) {
      Log.e(getClass().getSimpleName(), "trying to dirSync not dir: " + path);
      return;
    }
    Intent intent = getSyncIntent(jobid, path, SyncJob.CodOp.content);
    requireActivity().startService(intent);
  }

  void syncDir(int jobid, String path) {
    File dir = new File(FileManager.absPath(path));
    if ( ! dir.isDirectory() ) {
      Log.e(getClass().getSimpleName(), "trying to dirSync not dir: " + path);
      return;
    }
    Intent intent = getSyncIntent(jobid, path, SyncJob.CodOp.content);
    requireActivity().startService(intent);
  }

  protected Intent getRenameSyncIntent(int jobid, String path, String oldpath) {
    Intent intent = getSyncIntent(jobid, path, SyncJob.CodOp.renamed);
    intent.putExtra("oldpath", oldpath);
    return intent;
  }

  @Override
  public void onAcceptCancelClick() {
    commandDone();
  }

  @Override
  public void onAcceptOkClick(DialogFragment dialog) {
    int code = ((AcceptFragment) dialog).actionCode();
    if ( code == R.id.action_remove ) {
      Job job = Job.peek();
      if ( job == null ) {
        Log.e(getClass().getSimpleName(), "no job after accept ok click");
        return;
      }
      /* record selected items for later synchyng before removing, that will reload
         and, thus, clear selections; but for now this sync is disabled, see below
      List<String> paths = new ArrayList<String>();
      Vector<Boolean> sels = _dataModel.selections();
      for ( int ii = 0; ii<sels.size(); ++ii ) {
        if (sels.get(ii))
          if (_dataModel.usingFiles())
            paths.add(_dataModel.mgr().relFilePath(ii));
          else
            paths.add(_dataModel.mgr().relDirPath(ii));
      }
      */
      if ( !_dataModel.removeItems() )
        show(getString(R.string.couldnt_remove), Utils.ErrCode.Unknown);
      else {
      /* btm this is disabled to protect remote content; we could implement
         boolean setting stating if local deletions must be synchronized or not,
         in wich case we must implement rmoving in SFTP/Session
        for ( String path : paths )
          autoSyncItem(job.id, path, SyncJob.CodOp.removed);
       */
        someItemUnSelected();
      }
      commandDone();
    }
  }

  /** Action mode callback
   */
  class AmCallback implements ActionMode.Callback {

    ItemsFragment _frm = null;
    int           _menuId = 0;

    protected void setFragment(ItemsFragment frm) { _frm = frm; }

    protected void setMenuId(int id) { _menuId = id; }

    // Called when the action mode is created; startActionMode() was called
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      MenuInflater inflater = mode.getMenuInflater();
      inflater.inflate(_menuId, menu);
      ActionBar ab = ((AppCompatActivity)requireActivity()).getSupportActionBar();
      if ( ab != null ) ab.hide();
      return true;
    }

    // Called each time the action mode is shown. Always called after onCreateActionMode, but
    // may be called multiple times if the mode is invalidated.
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

      if (0 == menu.size()) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(_menuId, menu);
      }
      return true; // Return false if nothing is done
    }

    // Called when the user selects a contextual menu item
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      return _frm.doCommand(item.getItemId());
    }

    // Called when the user exits action mode
    @Override
    public void onDestroyActionMode(ActionMode mode) {
      _actionMode = null;
      _dataModel.clearSelections();
      if (getAdapter() != null)
        getAdapter().unselect();
      ActionBar bar = ((AppCompatActivity) requireActivity()).getSupportActionBar();
      if (null != bar)
        bar.show();
    }
  }

}
