/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;

class FilesViewAdapter extends ItemsViewAdapter {

  FilesViewAdapter(FilesFragment frm) {
    super(frm);
  }

  public void onAttachedToRecyclerView(@NonNull RecyclerView rv) {
    _rv = rv;
  }

  @NonNull
  @Override
  public FileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    Log.d(getClass().getSimpleName(), "onCreateViewHolder");
    View view = LayoutInflater.from(_fragment.requireActivity()).inflate(R.layout.file_item, parent, false);
    return new FileViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int pos) {
    final FileViewHolder holder = (FileViewHolder) viewHolder;
    File file = _fragment.dataModel().mgr().fileList().get(pos);
    String name = file.getName();
    Log.d(getClass().getSimpleName(), "onBindViewHolder" + pos + ", " + name);
    holder._textView.setText(name.toCharArray(), 0, name.length());
    String mime = Utils.mimeType(file);
    if ( mime != null ) {
      switch ( mime ) {
        case Constants.CamPicMime:
        case Constants.PngMime:
          holder._imgView.setImageResource(R.drawable.img_placeholder);
          break;
        case Constants.CamVidMime:
          holder._imgView.setImageResource(R.drawable.video_placeholder);
          break;
        case Constants.PdfMime:
          holder._imgView.setImageResource(R.drawable.ic_pdf);
          break;
        case Constants.VoiceRecordMime:
        case Constants.AudioAacMime:
        case Constants.AudioOggMime:
          holder._imgView.setImageResource(R.drawable.ic_baseline_audiotrack_24px);
          break;
        case Constants.TextMime:
          holder._imgView.setImageResource(R.drawable.ic_note_text);
          break;
        case Constants.GeoMime:
          holder._imgView.setImageResource(R.drawable.ic_add_location_blue_24dp);
          break;
        default:
          holder._imgView.setImageResource(R.drawable.ic_file);
          break;
      }
      holder.mark(_fragment._dataModel.marked(file));
      holder._card.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Log.d(getClass().getSimpleName(), "_card.onClick");
          if ( 0 < _fragment.dataModel().selected() ) {
            toggleSelect(holder);
          } else
            _fragment.itemClicked(holder.getBindingAdapterPosition());
        }
      });
      holder._card.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
          Log.d(getClass().getSimpleName(), "_imgView.onLongClick");
          toggleSelect(holder);
          return true;
        }
      });
      Log.d("... oBindViewHolder: ",holder.getBindingAdapterPosition() + ", " + holder.getLayoutPosition());
      Resources resmgr = _fragment.requireContext().getResources();
      Utils.exe().execute(ItemsViewAdapter.ImageLoadTask.push(this, file, pos, resmgr));
    }
  }

  @Override
  public int getItemCount() {
    List<File> files = _fragment.dataModel().mgr().files().getValue();
    if ( files != null )
      return files.size();
    return 0;
  }

  @Override
  void imageLoaded(int pos, Bitmap bm) {
    View view;
    int iv;
    FilesViewAdapter.FileViewHolder holder;
    for (iv=0; iv<_rv.getChildCount(); ++iv) {
      view = _rv.getChildAt(iv);
      if (_rv.getChildAdapterPosition(view) == pos) {
        holder = (FilesViewAdapter.FileViewHolder) _rv.getChildViewHolder(view);
        holder._imgView.setImageBitmap(bm);
        break;
      }
    }
    if (iv == _rv.getChildCount()) {
      Log.d(getClass().getSimpleName() + ".imageLoaded: ", "Adding pending bitmap for pos " + pos);
      _pendingbmps.add(new PendingBmp(pos, bm));
    }
  }

  /* FileViewHolder
   */
  static class FileViewHolder extends ItemViewHolder {
    private final TextView _textView;

    private FileViewHolder(View view) {
      super(view);
      _textView = view.findViewById(R.id.file_name);
      _imgView = view.findViewById((R.id.file_img));
    }
  }

  /* CellGestureDetector
   *
   * GestureDetector se usa para recopilar los distintos eventos que componen
   * una gesture y, una vez que la detecta, llamar al método apropiado del
   * listener (que implemente GestureDetector.OnGestureListener) que se le pasa
   * en la constructora. GestureDetector.SimpleOnGestureListener implementa
   * manejadores usuales vacíos que retornan false para que no sean tenidos en cuenta:
   * hay que re-escribir _siempre_ onDown (pq es el primer evento de toda gesture y
   * si se ignra, el sistema ya no te pasa los siguientes que correspondan a la misma
   * gesture) y luego ya los manejadores de las gestures que interesen.
   *
   * dejo el código aki, pq aunque no se está usando, funciona:
   *
   *  en onCreateViewHolder():
   *
   *     holder._gesman = new GestureDetectorCompat(parent.getContext(), new CellGestureDetector(holder));
   *
   *  en onBindViewHolder():
   *
   *    el manejador 'inline' que el holder asigna a _imgView  con
   *    holder._imgView.setOnClickListener() pasaría a ser CellGestureDetector.onSingleTapUp()
   *    y el que asigna con holder._imgView.setOnLongClickListener() pasaría a ser
   *    CellGestureDetector.onLongPress()
   *
   *    ambas asignaciones se sustituyen por:
   *
   *    holder._imgView.setOnTouchListener(new View.OnTouchListener() {
   *      @Override
   *      public boolean onTouch(View view, MotionEvent event) {
   *        if (!holder._gesman.onTouchEvent(event))
   *        return holder._imgView.onTouchEvent(event);
   *        return true;
   *      }
   *    });
   *
  class CellGestureDetector extends GestureDetector.SimpleOnGestureListener {
    private static final String DEBUG_TAG = "Gestures";
    private FileViewHolder _holder;

    CellGestureDetector(FileViewHolder holder) {
      _holder = holder;
    }

    @Override
    public boolean onDown(MotionEvent ev) {
      Log.d(DEBUG_TAG, "onDown");
      return true;
    }

    @Override
    public void onLongPress(MotionEvent ev) {
      Log.d(DEBUG_TAG, "onLongPress");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent ev) {
      Log.d(DEBUG_TAG, "onSingleTapUp");
      return super.onContextClick(ev);
    }
  }
   */
}
