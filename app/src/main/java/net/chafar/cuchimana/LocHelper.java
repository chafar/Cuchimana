/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

@SuppressWarnings({"SpellCheckingInspection", "unused"})
public class LocHelper implements LocationListener {

  public interface LocHelperListener {
    void onLocFileSaved(File file, Job job);
    Context ctx();
  }

  static public final float minAccuracy = (float)50.0;

  private static LocationManager _mgr = null;
  private static File       _locFile = null;
  static public LocHelper   _helper = null;
  private LocHelperListener _listener;
  private final Job         _job;

  LocHelper(LocHelperListener listener, Job job) {
    _listener = listener;
    _mgr = (LocationManager) listener.ctx().getSystemService(Context.LOCATION_SERVICE);
    _helper = this;
    _job = job;
  }

  /** start acquiring location from device
   */
  public void search(File file) {
    _locFile = file;
    // we look for just one update as soon as possible
    Criteria crit = new Criteria();
    crit.setAltitudeRequired(false);
    crit.setSpeedRequired(false);
    crit.setBearingRequired(false);
    crit.setHorizontalAccuracy(Criteria.ACCURACY_MEDIUM);
    crit.setPowerRequirement(Criteria.POWER_HIGH);
    _mgr.requestSingleUpdate(crit, this, null);
  }

  public void cancel() {
    if (_mgr != null)
      _mgr.removeUpdates(this);
    _listener = null;
  }

  protected void finalize() {
    cancel();
  }

  static public Boolean enabled() {
    if (_mgr == null)
      return false;
    return _mgr.isProviderEnabled(LocationManager.GPS_PROVIDER) || _mgr.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
  }

  static public Boolean available(Context ctx) {
    return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION);
  }

  static public Boolean gpsAvailable(Context ctx) {
    return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
  }

  static public Boolean netAvailable(Context ctx) {
    return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_NETWORK);
  }

  /** renames _locfile
   */
  static public boolean rename(String newname) {
    File newfile = new File(_locFile.getParent(), newname  + '.' + Constants.GeoExtension);
    if (!FileManager.rename(_locFile, newfile.getName()))
      return false;
    _locFile = newfile;
    return true;
  }

  /** returns most recent location from either network or gps
   */
  static public Location last() {
    if (_mgr == null || _helper == null)
      return null;
    Location loc1 = _mgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    Location loc2 = _mgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    if (null != loc1 && null != loc2 && loc1.getTime() > loc1.getTime())
      return loc1;
    return loc2;
  }

  static public Boolean save(Location loc, File file) {
    if (file != null)
      _locFile = file;
    if (_locFile == null || ! _locFile.exists())
      return false;
    FileOutputStream fos;
    String json = "{\"type\": \"Point\", \"coordinates\": [" + loc.getLatitude() + ", " + loc.getLongitude() + "]}";
    try { fos = new FileOutputStream(_locFile); } catch ( FileNotFoundException ex ) { return false; }
    try { fos.write(json.getBytes()); } catch (Exception ex) { ex.printStackTrace(); return false; }
    try { fos.close(); } catch(Exception ex) { ex.printStackTrace(); return false; }
    return true;
  }

  static public Uri load(File file) {
    FileInputStream fin;
    String str;
    byte[] bytes = Utils.loadFile(file, 1024);
    if (bytes.length == 0)
      return null;
    try {
      JSONArray coos = (JSONArray) new JSONObject(new String(bytes)).get("coordinates");
      str = "geo:" + coos.getDouble (0) + "," + coos.getDouble(1);
    } catch ( JSONException ex ) { ex.printStackTrace(); return null; }
    return Uri.parse(str);
  }

  static public Uri uri(Location loc) {
    return Uri.parse("geo:" + loc.getLatitude() + "," + loc.getLongitude());
  }

  /* LocationListener implementation
  */

  @Override
  public void onLocationChanged(android.location.Location location) {
    // esto se ejecuta en la misma thread que la activity: sin poblemos
    Log.d(getClass().getSimpleName(), "New location received, accuracy: " + location.getAccuracy());
    if (location.getAccuracy() > minAccuracy) {
      LocHelper._helper.search(_locFile);
      return;
    }
    save(location, null);
    if ( _listener != null)
      _listener.onLocFileSaved(_locFile, _job);
    // x si alguna vez usamos la llamada de múltiples updates
    cancel();
  }
  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {
    Log.d(getClass().getSimpleName(), "status changed received: " + provider);
  }
}
