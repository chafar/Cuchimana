/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.chafar.cuchimana;

import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public abstract class FragmentBase extends Fragment {

  protected CuViewModel     _dataModel;
  protected ActionMode      _actionMode = null;
  protected AppCompatActivity _acti;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Uses the same instance in every fragment that does this
    _dataModel = new ViewModelProvider(requireActivity()).get(CuViewModel.class);
    _acti = (AppCompatActivity) requireActivity();
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    // once upon a time this did something
    super.onSaveInstanceState(outState);
  }

  public CuViewModel dataModel() {
    return _dataModel;
  }

  abstract public void gotActive();

  public void gotUnactive() {
    if (_actionMode != null)
      _actionMode.finish(); // Action picked, so close the CAB
  }

  void show(String msg, Utils.ErrCode status) {
    ((ActivityBase) requireActivity()).show(msg, status);
  }

  public Intent getSyncIntent(int jobid, String path, SyncJob.CodOp code) {
    return ((ActivityBase) requireActivity()).getSyncIntent(code, jobid, path);
  }

}
