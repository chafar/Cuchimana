/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import static androidx.preference.PreferenceManager.getDefaultSharedPreferences;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import java.io.File;

public class SyncFragment extends DialogFragment {

  @Override
  public void show(@NonNull FragmentManager manager, @Nullable String tag) {
    super.show(manager, tag);
  }

  // Here we could have access to dialog, once created, to tune it
  @NonNull
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    return super.onCreateDialog(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    LinearLayout view = (LinearLayout) inflater.inflate(R.layout.sync_fragment, container, false);

    CuViewModel _dataModel = new ViewModelProvider(requireActivity()).get(CuViewModel.class);

    Button but = view.findViewById(R.id.closebut);
    if (but != null) {
      but.setOnClickListener(v -> dismiss());
    }

    but = view.findViewById(R.id.testbut);
    if (but != null) {
      but.setOnClickListener(v -> {
        if (!Utils.syncAllowed(requireContext())) {
          Toast.makeText(getContext(), R.string.nonet, Toast.LENGTH_LONG).show();
          return;
        }
        Job job = Job.make(R.id.testbut);
        Intent intent = ((ActivityBase) requireActivity()).getSyncIntent(SyncJob.CodOp.test, job.id, null);
        requireActivity().startService(intent);
        dismiss();
      });
    }

    TextView tv = view.findViewById(R.id.tvPubKey);
    if (tv != null) {
      String path = _dataModel.mgr().userDir();
      File pubfile = new File(path, Constants.SyncKeysFileName + ".pub");
      byte[] pubkey = Utils.loadFile(pubfile, Integer.MAX_VALUE);
      tv.setText(new String(pubkey));
      tv.setSelectAllOnFocus(true);
    }
    tv = view.findViewById(R.id.tvHostKey);
    if (tv != null) {
      tv.setText(getDefaultSharedPreferences(requireActivity()).getString("synchostkey", null));
      tv.setSelectAllOnFocus(true);
    }
    return view;
  }

}