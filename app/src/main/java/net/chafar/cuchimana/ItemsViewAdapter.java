/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.os.HandlerCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public abstract class ItemsViewAdapter extends RecyclerView.Adapter<ItemViewHolder> {
  protected RecyclerView _rv;
  protected final ItemsFragment _fragment;
  protected final LinkedList<PendingBmp> _pendingbmps;

  static protected class PendingBmp {
    protected final int pos;
    protected final Bitmap bmp;
    PendingBmp(int p, Bitmap b) { pos = p; bmp = b; }
  }

  protected ItemsViewAdapter(ItemsFragment frm) {
    _fragment = frm;
    _rv = null;
    _pendingbmps = new LinkedList<>();
  }

  @Override
  public void onViewAttachedToWindow(@NonNull ItemViewHolder holder) {
    Log.d(getClass().getSimpleName() + ".onViewAttachedToWindow", "pos " + holder.getBindingAdapterPosition());

    // sync holder selection state with current selection state
    holder.select(selected(holder.getBindingAdapterPosition()));

    // assign image if view attached after bitmap loaded
    if ( _pendingbmps.size() > 0) {
      PendingBmp pending;
      ListIterator<PendingBmp> it = _pendingbmps.listIterator();
      while ( it.hasNext() ) {
        pending = it.next();
        if (pending.pos == holder.getLayoutPosition()) {
          Log.d(getClass().getSimpleName() + ".onViewAttachedToWindow", "pending bitmap attached");
          if (holder._imgView != null)
            holder._imgView.setImageBitmap(pending.bmp);
          it.remove();
          break;
        }
      }
    }
  }

  @SuppressLint("NotifyDataSetChanged")
  protected void resetView() { notifyDataSetChanged(); }

  public void refreshView() {
    View view;
    int iv;
    ItemViewHolder holder;
    for (iv=0; iv<_rv.getChildCount(); ++iv) {
      view = _rv.getChildAt(iv);
      holder = (ItemViewHolder) _rv.getChildViewHolder(view);
      holder.mark(_fragment._dataModel.marked(iv));
      holder.select(_fragment._dataModel.selected(iv));
    }
  }

  protected void toggleSelect(ItemViewHolder holder) {
    int pos = holder.getBindingAdapterPosition();
    if (selected(pos))
      unselect(holder);
    else
      select(holder);
  }

  private void select(ItemViewHolder holder) {
    int pos = holder.getBindingAdapterPosition();
    if (pos<0)
      return;
    _fragment.dataModel().selectItem(pos);
    if ( !holder.hasSelection() )
      holder.toggleSelected();
    _fragment.someItemSelected();
  }

  private void unselect(ItemViewHolder holder) {
    if ( holder.hasSelection() )
      holder.toggleSelected();
    int pos = holder.getBindingAdapterPosition();
    if ( pos<0 || !selected(pos) )
      return;
    _fragment.dataModel().unSelectItem(pos);
    _fragment.someItemUnSelected();
  }

  /// deselects all
  protected void unselect() {
    ItemViewHolder holder;
    int ii;
    // this processes only views actually shown in screeen due to the nature of RecyclerView
    // that detaches childs when they go out of screen
    for ( ii = 0; ii < _rv.getChildCount(); ++ii ) {
      holder = (ItemViewHolder) _rv.getChildViewHolder(_rv.getChildAt(ii));
      _rv.getChildAt(0);
      unselect(holder);
    }
    _fragment.dataModel().unselect();
  }

  private Boolean selected(int pos) {
    return _fragment.dataModel().selected(pos);
  }

  abstract void imageLoaded(int pos, Bitmap bm);

  private void imageLoadDone() {
    ImageLoadTask task;
    Iterator<ImageLoadTask> it = ImageLoadTask._tasks.iterator();
    while (it.hasNext()) {
      task = it.next();
      if ( task._bm != null ) {
        imageLoaded(task._pos, task._bm);
        it.remove();
      }
    }
  }

  static protected class ImageLoadTask extends FutureTask<Bitmap> {
    static private final ArrayList<ImageLoadTask> _tasks = new ArrayList<>();
    final int      _pos;
    final ItemsViewAdapter  _adapter;
    final Handler _posthandler;
    public Bitmap _bm = null;

    private ImageLoadTask(ItemsViewAdapter adapter, File file, int pos, ImageLoadCallable callable) {
      super( callable );
      // https://developer.android.com/guide/background/threading#java
      _posthandler = HandlerCompat.createAsync(Looper.getMainLooper());
      _adapter = adapter;
      _pos = pos;
    }

    @Override
    protected void done() {
      if (!isCancelled())
        try { _bm = get(); } catch ( Exception ignored ) {}
      _posthandler.post(_adapter::imageLoadDone);
    }

    static public ImageLoadTask push(ItemsViewAdapter adapter, File file, int pos, Resources resmgr) {
      ImageLoadTask task = new ImageLoadTask(adapter, file, pos, new ImageLoadCallable(file, resmgr));
      _tasks.add( task );
      return task;
    }

    static public ImageLoadTask push(ItemsViewAdapter adapter, File file, int pos) {
      ImageLoadTask task = new ImageLoadTask(adapter, file, pos, new ImageLoadCallable(file));
      _tasks.add( task );
      return task;
    }
  }

  static private class ImageLoadCallable implements Callable<Bitmap> {
    final File      _file;
    final Resources _resmgr;

    protected ImageLoadCallable(File file, @Nullable Resources resmgr) {
      _file = file;
      _resmgr = resmgr;
    }

    protected ImageLoadCallable(File file) {
      _file = file;
      _resmgr = null;
    }

    @Override
    public Bitmap call() {
      if (_resmgr != null)
        return ImageManager.instance().getSampleBitmap( _file, Constants.SampleSize, Constants.SampleSize, ImageManager.instance().density(), _resmgr);
      else
        return ImageManager.instance().getDirBitmap(_file);
    }
  }

}

