/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

public abstract class ActivityBase extends AppCompatActivity implements FragmentOnAttachListener {

  protected final int PermStorage = 0;
  protected final int PermRecord = 1;
  protected final int PermLocation = 2;
  protected final int PermNet = 3;
  static public final String DEFNOTCHANID = "default";

  protected CuViewModel _dataModel;
  protected FilenameFragment  _fnfrag = null;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Get a new ViewModel from the ViewModelProvider.
    _dataModel = new ViewModelProvider(this).get(CuViewModel.class);

    // Image manager
    DisplayMetrics dm = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(dm);
    new ImageManager(dm, _dataModel);

    getSupportFragmentManager().addFragmentOnAttachListener(this);

    if (this instanceof Folder || this instanceof Receive)
      SyncJob.load(this);

    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      CharSequence name = getString(R.string.name);
      String description = "...";
      int importance = NotificationManager.IMPORTANCE_DEFAULT;
      NotificationChannel channel = new NotificationChannel(DEFNOTCHANID, name, importance);
      channel.setDescription(description);
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      NotificationManager notificationManager = getSystemService(NotificationManager.class);
      notificationManager.createNotificationChannel(channel);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    SyncReceiver.instance().setContext(this);
  }

  @Override
  protected void onPause() {
    super.onPause();
    SyncReceiver.instance().setContext(null);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (this instanceof Folder || this instanceof Receive)
      SyncJob.save();
  }

  public void onAttachFragment(@NonNull FragmentManager mgr, @NonNull Fragment fragment) {
    // CUIDADO: esto se ejecuta _antes_ que onCreate()
    if (fragment instanceof FilenameFragment)
      _fnfrag = (FilenameFragment) fragment;
  }

  protected View getViewForSnackbar() { return null; }

  public void show(String msg, Utils.ErrCode status) {
    View view = getViewForSnackbar();
    if (view != null) {
      Snackbar snack = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
      snack.setTextColor( Utils.ErrCode.error(status) ? Color.WHITE : Color.BLACK );
      snack.setBackgroundTint(Utils.ErrCode.error(status) ? Color.RED : Color.GREEN);
      snack.show();
    } else
      Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
  }

  public Intent getSyncIntent(SyncJob.CodOp code, int jobid, String path) {
    return makeSyncIntent(this, code, jobid, path);
  }

  static private Intent makeSyncIntent(Context ctx, SyncJob.CodOp code, int jobid, String path) {
    Intent intent = new Intent(ctx, SyncService.class);
    intent.putExtra("code", code.ordinal());
    intent.putExtra("path", path);
    intent.putExtra("jobid", jobid);
    intent.putExtra("privkeypass", Utils.getEncryptedPref(Constants.PrivKeyPass, null));
    intent.putExtra("receiver", SyncReceiver.messenger());
    return intent;
  }

  protected void registerForSync() {
    // ... note that ... all capabilities requested must be satisfied
    // so may be we need to register more than one NetworkRequest depending on syncpref
    final ConnectivityManager connmgr = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE));
    NetworkRequest.Builder builder = new NetworkRequest.Builder();
    builder.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
    if ( Utils.NetPref.wifionly == Utils.getSyncPref(this) )
      builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
    else if ( Utils.NetPref.notroaming == Utils.getSyncPref(this)
      && Build.VERSION.SDK_INT >= Build.VERSION_CODES.P )
        builder.addCapability( NetworkCapabilities.NET_CAPABILITY_NOT_ROAMING );
    NetworkRequest req = builder.build();
    connmgr.registerNetworkCallback(req, new NetCallback());
  }

  private final class NetCallback extends ConnectivityManager.NetworkCallback {
    @Override
    public void onAvailable(@NonNull Network network) {
      super.onAvailable(network);
      if (SyncJob.size() > 0) {
        Intent intent = getSyncIntent(SyncJob.CodOp.queue, 0, null);
        startService(intent);
      }
    }
    @Override
    public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities caps) {

    }
    @Override
    public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties props) {

    }
  }

  // we need it to not be static to be able to retrieve some context
  @SuppressLint("HandlerLeak")
  static public final class SyncReceiver extends Handler {

    enum What {
      syncjob,
      net
    }

    private Context _ctx;

    @SuppressLint("StaticFieldLeak")    // _ctx is only used if not null and is renewed from activity's onResume() and reset from onPause()
    static private final SyncReceiver _instance = new SyncReceiver(Looper.getMainLooper());
    static private final Messenger _messenger = new Messenger( _instance );

    static Messenger messenger() { return _messenger; }
    static SyncReceiver instance() { return _instance; }

    protected SyncReceiver(Looper looper) {
      super(looper);
    }

    protected void setContext(Context ctx) { _ctx = ctx; }

    @Override
    public void handleMessage(Message msg) {

      SyncJob job = (SyncJob) msg.obj;
      if (_ctx != null && _ctx instanceof ActivityBase)
        ((ActivityBase) _ctx).show(job.res.msg, job.res.errcode);
      if (!Utils.ErrCode.error(job.res.code()))
        SyncJob.poll();
      if (job.res.msg != null)
        Log.d(getClass().getSimpleName(), "msg received: " + job.res.msg);
      if (job.res.code() == Utils.ErrCode.Conn)
        job.res.errcode = Utils.ErrCode.Delayed;
      if (SyncJob.size() > 0) {
        // if first in queue != current one and delayed, retry with it.
        SyncJob next = SyncJob.peek();
        if ( next != null && next != job && next.res.code() == Utils.ErrCode.Delayed && _ctx != null && Utils.syncAllowed(_ctx) ) {
          Intent intent = makeSyncIntent(_ctx, SyncJob.CodOp.queue, 0, null);
          _ctx.startService(intent);
        }
      } else if (_ctx != null) {
        // stop service if no more sync jobs to process
        Intent intent = makeSyncIntent(_ctx, SyncJob.CodOp.queue, 0, null);
        _ctx.stopService(intent);
      }
    }
  }
}
