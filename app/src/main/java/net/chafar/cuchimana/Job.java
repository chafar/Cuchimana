/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileWriter;
import java.util.EnumMap;
import java.util.Map;
import java.util.Stack;

/** intstance's 'id' member will always be > 0
 * Note that, though this works as job stack, for now that feature is not used,
 * since only one job is active at a given time
 */
@SuppressWarnings({"SameParameterValue", "unused"})
class Job {
  static private int _lastid = 0;
  static private final JobStack _stack = new JobStack();
  /// job id
  final int id;
  /// command id
  final int type;
  /// usually for intent type
  public int code;
  /// stores
  public enum StoreCode {
    file,
    user0,
    user1
  }
  // remeber! no duplicate keys
  private EnumMap<Job.StoreCode, String> _strStore;

  private Job(int type) {
    id = _nextId();
    this.type = type;
    this.code = -1;
    _strStore = null;
  }

  void put(Job.StoreCode key, String str) {
    if (_strStore == null)
      _strStore = new EnumMap<>(Job.StoreCode.class);
    _strStore.put(key, str);
  }
  String get(Job.StoreCode key, String def) {
    if (_strStore != null && _strStore.containsKey(key))
      return _strStore.get(key);
    return def;
  }

  @NonNull
  @Override
  public String toString() {
    StringBuilder str = new StringBuilder("id=" + id + ';');
    str.append("type=").append(type).append(';');
    str.append("code=").append(code).append(';');
    if ( _strStore != null ) {
      str.append("strings:");
      for ( Map.Entry<StoreCode, String> keyval : _strStore.entrySet() )
        str.append(keyval.getKey().toString()).append('=').append(keyval.getValue());
    }
    str.append('\n');
    return str.toString();
  }

  static private int _nextId() {
    if (_lastid == Integer.MAX_VALUE)
      _lastid = 0;
    return ++_lastid;
  }

  static public Job make(int type) {
    Job job = new Job(type);
    _stack.push(job);
    return job;
  }

  static public Job peek() { return _stack.peek(); }

  static public Job pop() { return _stack.pop(); }

  static public Job job(int id) { return _stack.job(id); }

  static private String savePath() {
    return new File(FileManager.instance().userDir(), "JobStack.txt").getPath();
  }

  static boolean save() {
    String path = savePath();
    if ( _stack.size() > 0 ) {
      try {
        FileWriter writer = new FileWriter(path);
        for ( Job job : _stack )
          writer.write(job.toString());
        writer.close();
      } catch ( Exception ex ) {
        return false;
      }
    }
    return true;
  }

  static boolean load() {
    Job job = null;

    if (_stack.size() > 0)
      // must be called at early stage, when no job has been added to the stack
      return false;
    File file = new File(savePath());
    if ( !file.exists() )
      return true;
    byte[] bytes = Utils.loadFile(file);
    if (bytes.length == 0)
      return true;

    boolean instore = false;
    String content = new String(bytes);
    int from=0;
    char ch;
    String name = null;
    String value;
    for (int pos=0; pos<content.length(); pos++ ) {
      ch = content.charAt(pos);
      if ( ch == '=' || ch == ':' )
        if ( pos > from ) {
          name = content.substring(from, pos);
          from = pos + 1;
          if ( ch == ':' && name.equals("strings"))
            instore = true;
        } else
          return false;
      if ( ch == ';' || ch == '\n' ) {
        if ( name != null && pos > from ) {
          value = content.substring(from, pos);
          from = pos + 1;
          if (instore && job != null)
            job.put(StoreCode.valueOf(name), value);
          switch (name) {
            case "id": break;
            case "type": job = new Job(Integer.parseInt(value)); break;
            case "code": if (job != null) job.code = Integer.parseInt(value); break;
          }
          if (ch == '\n') {
            // register job
            if (job != null) {
              _stack.add(job);
              job = null;
              instore = false;
            }
          }
        }
        name = null;
      }
    }
    file.delete();
    return true;
  }

  private static class JobStack extends Stack<Job> {

    @Nullable
    Job job(int id) {
      for ( Job job : this)
        if (job.id == id)
          return job;
      return null;
    }

    @Override
    public Job push(Job job) {
      return super.push(job);
    }

    @Override
    @Nullable
    public Job peek() {
      if (isEmpty())
        return null;
      return super.peek();
    }

    @Override
    @Nullable
    public Job pop() {
      if (isEmpty())
        return null;
      return super.pop();
    }
  }   // JobStack
}
