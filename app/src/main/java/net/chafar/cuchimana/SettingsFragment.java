/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Base64;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

import java.security.SecureRandom;
import java.util.Calendar;

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsFragment extends PreferenceFragmentCompat {
  private String _prevhost;
  private String _prevuser;
  private CuViewModel _dataModel = null;

  public void setDataModel(CuViewModel dataModel) {
    _dataModel = dataModel;
  }

  private void resetSyncKeys() {
    if (_dataModel != null) {
      Log.d(getClass().getSimpleName(), "invalidating current sync keys");
      SecureRandom ran = new SecureRandom();
      ran.setSeed(String.valueOf(Calendar.getInstance().getTimeInMillis()).getBytes());
      byte[] bytes = new byte[32];
      ran.nextBytes(bytes);
      String pass = Base64.encodeToString(bytes, Base64.NO_PADDING|Base64.NO_WRAP);
      if (SSH.newKeyPair("client@cuchimana", _dataModel.mgr().userDir(), Constants.SyncKeysFileName, pass.getBytes()))
        Utils.setEncryptedPref(Constants.PrivKeyPass, pass);
    }
  }

  @Override
  public void onCreatePreferences(Bundle bundle, String s) {
    setPreferencesFromResource(R.xml.preferences, null);
    EditTextPreference et = findPreference(getString(R.string.synchostpref));
    if (et != null) {
      _prevhost = et.getText();
      et.setOnBindEditTextListener(editText -> editText.addTextChangedListener(new MyKeyFilter()));
    }
    et = findPreference(getString( R.string.syncuserpref));
    if (et != null) {
      _prevuser = et.getText();
      et.setOnBindEditTextListener(editText -> editText.addTextChangedListener(new MyKeyFilter()));
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    boolean changed = false;
    EditTextPreference et = findPreference(getString(R.string.synchostpref));
    if (et != null)
      if ( _prevhost == null || !_prevhost.equals(et.getText()))
        changed = true;
    et = findPreference(getString(R.string.syncuserpref));
    if (et != null)
      if (_prevuser == null || !_prevuser.equals(et.getText()))
        changed = true;

    if (changed) {

      resetSyncKeys();

      // Note that notification channel with id ActivityBase.DEFNOTCHANID must have been
      // created and that Folder.onStart() must inspect its intent for an extra string
      // with value SyncFragment.class.getSimpleName() to show that fragment when called
      // from the notification user tap

      Intent intent = new Intent(requireActivity(), Folder.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.putExtra("action", SyncFragment.class.getSimpleName());
      PendingIntent pendingIntent = PendingIntent.getActivity(requireActivity(), 0, intent, PendingIntent.FLAG_IMMUTABLE);
      NotificationCompat.Builder builder = new NotificationCompat.Builder(requireActivity(), ActivityBase.DEFNOTCHANID)
          .setSmallIcon(R.drawable.ic_baseline_vpn_key_24)
          .setContentTitle(getString(R.string.keypairsubject))
          .setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.keypairlongtext)))
          .setPriority(NotificationCompat.PRIORITY_DEFAULT)
          .setContentIntent(pendingIntent)
          .setAutoCancel(true);
      NotificationManagerCompat mgr = NotificationManagerCompat.from(requireActivity());
      mgr.notify(R.id.newkeypairnot, builder.build());
    }

  }

  static class MyKeyFilter implements android.text.TextWatcher {
    boolean changemine;
    int start, after;
    CharSequence prev;

    MyKeyFilter() { }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      this.start = start;
      this.after = after;
      prev = s;
    }
    @Override
    public void afterTextChanged(Editable s) {
      if ( changemine ) {
        changemine = false;
        return;
      }
      Log.d("host", "user change: " + s.toString());
      char ch;
      int type;
      for ( int ii = start; ii < start + after; ++ii ) {
        ch = s.charAt(ii);
        type = Character.getType(ch);   // https://developer.android.com/reference/java/lang/Character?hl=en#getType(char)
        if ( type == Character.SURROGATE || type == Character.CONTROL || type == Character.FORMAT || type == Character.LINE_SEPARATOR || type == Character.PARAGRAPH_SEPARATOR
            || type == Character.PRIVATE_USE || type == Character.UNASSIGNED) {
          changemine = true;
          s.replace(0, s.length(), prev);
        } else if ( !Utils.isalpha(ch ) && !Utils.isdigit(ch) && ch != '.' && ch != '-' && ch != '_' ) {
          changemine = true;
          s.replace(ii, ii+1, "_");
        }
      }
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { }
  }
}
