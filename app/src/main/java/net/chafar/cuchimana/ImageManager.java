/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import static java.lang.Character.LINE_SEPARATOR;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.graphics.pdf.PdfRenderer;
import android.media.MediaMetadataRetriever;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;

import androidx.core.content.res.ResourcesCompat;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;

// útil: https://developer.android.com/training/multiscreen/screendensities#TaskUseDP
@SuppressWarnings({"WeakerAccess", "unused"})
public class ImageManager {

  static private ImageManager _instance = null;

  private final CuViewModel _dataModel;

  static public ImageManager instance() {
    return _instance;
  }

  /// how many raw pixels match given pixels quantity
  static public int rawPixels(int dppix, float density) {
    return (int) (dppix * density);
  }

  private final DisplayMetrics _metrics;

  public ImageManager(DisplayMetrics metrics, CuViewModel datamodel) {
    _metrics = metrics;
    _instance = this;
    _dataModel = datamodel;
  }

  public float density() {
    return _metrics.density;
  }

  public int dpWidth() {
    return (int) (_metrics.widthPixels / _metrics.density);
  }

  public int dpHeight() {
    return (int) (_metrics.heightPixels / _metrics.density);
  }

  public float inchWidth() {
    return (int) (_metrics.widthPixels / _metrics.xdpi);
  }

  public float inchHeight() {
    return (int) (_metrics.heightPixels / _metrics.ydpi);
  }

  /// returns absolute sample path that file name in current dir
  public String getSamplePath(String name, int reqwdp, int reqhdp) {
    final int size = Math.max(reqwdp, reqhdp);
    String samname =  name.substring(0, name.lastIndexOf('.')) + '-' + size + ".png";
    return new File(_dataModel.mgr().samplesCachePath(), samname).getPath();
  }

  public Bitmap getSampleBitmap(File src, int reqwdp, int reqhdp, float density, Resources resmgr) {

    final int reqw = rawPixels(reqwdp, density);
    final int reqh = rawPixels(reqhdp, density);

    Bitmap samplebm = null;
    VectorDrawable icon = null;

    String mime = Utils.mimeType(src);

    final File sampleFile = new File(getSamplePath(src.getName(), reqwdp, reqhdp));

    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inSampleSize = 1;
    if (sampleFile.exists()) {
      samplebm = BitmapFactory.decodeFile(sampleFile.toString(), options);
    } else {
      if (mime != null) {
        switch ( mime ) {
          case "image/jpeg":
          case "image/png":
            samplebm = scaleBitmap(src, reqw, reqh);
            icon = (VectorDrawable) ResourcesCompat.getDrawable(resmgr, R.drawable.img_placeholder, null);
            if (icon != null)
              icon.setColorFilter(new PorterDuffColorFilter(resmgr.getColor(R.color.material_light_blue_300, null), PorterDuff.Mode.SRC_ATOP));
            break;
          case Constants.CamVidMime:
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
              retriever.setDataSource(src.getAbsolutePath());
              samplebm = retriever.getFrameAtTime(0, MediaMetadataRetriever.OPTION_CLOSEST);
              // alternativas como retriever.getPrimaryImage (MediaMetadataRetriever.BitmapParams params)
              // entraron en api level 28, mientras que getFrameAtTime() lo hizo en api level 10
            } catch (Exception ex) {
              Log.e(this.getClass().toString(), ex.toString());
            } finally {
              _dataModel.mgr().saveImageSample(samplebm, sampleFile.getName());
              try {
                retriever.release();
              } catch (RuntimeException ex) {
                Log.e(this.getClass().toString(), ex.toString());
              }
            }
            if (samplebm != null)
              samplebm = scaleBitmap(sampleFile, reqw, reqh);
            icon = (VectorDrawable) ResourcesCompat.getDrawable(resmgr, R.drawable.video_placeholder, null);
            if (icon != null)
              icon.setColorFilter(new PorterDuffColorFilter(resmgr.getColor(R.color.material_light_blue_300, null), PorterDuff.Mode.SRC_ATOP));
            break;
          case Constants.PdfMime:
            FileInputStream is = null;
            FileDescriptor fd = null;
            try { is = new FileInputStream(src); } catch (Exception ignored ) { }
            if (is != null) {
              try { fd = is.getFD(); } catch (Exception ignored ) { }
              if (fd != null) {
                PdfRenderer engine = null;
                try { engine = new PdfRenderer(ParcelFileDescriptor.open(src, ParcelFileDescriptor.MODE_READ_ONLY)); } catch (Exception ignored ) { }
                if (engine != null && engine.getPageCount() > 0) {
                  PdfRenderer.Page page = engine.openPage(0);
                  float aspect = (float)page.getHeight() / (float)page.getWidth();
                  int cliph = aspect >= 1 ? reqh : (int) (reqh * aspect);
                  int clipw = (int) (cliph / aspect);
                  Rect rect = new Rect();
                  rect.top = (aspect >= 1) ? 0 : (reqh - cliph) / 2;
                  rect.bottom = rect.bottom + cliph;
                  rect.left = (aspect >= 1) ? (reqw - clipw) / 2 : 0;
                  rect.right = rect.left + clipw;
                  samplebm = Bitmap.createBitmap(reqw, reqh, Bitmap.Config.ARGB_8888);
                  page.render(samplebm, rect, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                }
              }
            }
            icon = (VectorDrawable) ResourcesCompat.getDrawable(resmgr, R.drawable.ic_pdf, null);
            break;
          case Constants.VoiceRecordMime:
          case Constants.AudioAacMime:
          case Constants.AudioOggMime:
          case Constants.AudioMpeg:
            RecordSample ri = new RecordSample(_metrics, reqw, reqh);
            // note: we aren't decoding the file, we just use it as some pseudo-random data source
            ri.draw(Utils.loadFile(src, 4096));
            samplebm = ri.bitmap();
            icon = (VectorDrawable) ResourcesCompat.getDrawable(resmgr, R.drawable.ic_baseline_audiotrack_24px, null);
            if (icon != null)
              icon.setColorFilter(new PorterDuffColorFilter(resmgr.getColor(R.color.material_light_blue_300, null), PorterDuff.Mode.SRC_ATOP));
            break;
          case Constants.GeoMime:
            icon = (VectorDrawable) ResourcesCompat.getDrawable(resmgr, R.drawable.ic_add_location_blue_24dp, null);
//            samplebm = getTextSample(src, reqw, reqh, density, resmgr, R.color.geo_background);
            samplebm = getBitmapFromVectorDrawable(resmgr, R.drawable.ic_map);
            break;
          case Constants.TextMime:
            samplebm = getTextSample(src, reqw, reqh, density, resmgr, R.color.material_blue_800);
            break;
          default:
        }
        if (samplebm != null) {
          if (icon != null) {
            icon.setBounds(0,0, rawPixels(24, density),rawPixels(24, density));
            Canvas canvas = new Canvas(samplebm);
            icon.draw(canvas);
          }
          _dataModel.mgr().saveImageSampleAsync(samplebm, sampleFile.getName());
        }
      }
    }
    return samplebm;
  }

  private Bitmap getTextSample(File src, int reqw, int reqh, float density, Resources resmgr, int bkgColorRes) {
    Bitmap samplebm = Bitmap.createBitmap(reqw, reqh, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(samplebm);
    canvas.drawColor(resmgr.getColor(bkgColorRes, null));
    Paint painting = new Paint();
    //painting.setStrokeWidth(5f);
    painting.setStyle(Paint.Style.FILL);
    painting.setAntiAlias(true);
    painting.setColor(0xffffffff);
    painting.setTypeface(Typeface.MONOSPACE);
    painting.setTextSize(rawPixels(18, density));
    byte[] bytes = Utils.loadFile(src, 1024);
    String text = new String(bytes);
    int from = 0, pos = 0;
    int space = rawPixels(4, density);
    int rx = 2;
    int ry = (int) painting.getTextSize();
    int run;
    boolean eot = false;
    int codepoint;
    while (pos < text.length() && ry < samplebm.getHeight()) {
      codepoint = text.codePointAt(pos);
      if ( Character.isWhitespace(text.codePointAt(from)) )
        ++from;
      if ((pos+1) == text.length())
        eot = true;    // let last char be rendered
      if (Character.isWhitespace(codepoint) && pos > from && !Character.isWhitespace(text.codePointAt(from)) || eot) {
        if (eot)
          ++ pos;   // rendering uses pos-1 as last char, but be aware with the rest
        run = (int) painting.getRunAdvance(text, from, pos, from, pos, false, pos);
        if ( samplebm.getWidth() < (rx + run) ) {
          ry += (int) painting.getTextSize();
          rx = 2;
        }
        if (ry <= samplebm.getHeight())
          canvas.drawText(text, from, pos, rx, ry, painting);
        rx += run + space;
        from = pos + 1;
      }
      // LINE_SEPARATOR: 0xd
      if (codepoint == 0xa || eot || Character.getType(codepoint) == LINE_SEPARATOR)
        break;
      ++pos;
    }
    return samplebm;
  }

  public Bitmap getDirBitmap(File dir) {
    Bitmap bm = null;
    File last = _latestFileInDir(dir);
    if ( last != null ) {
      final File sampleDir = new File(_dataModel.mgr().samplesCachePath(FileManager.relPathOf(last.getPath()))).getParentFile();
      final File sampleFile = new File(sampleDir, last.getName().substring(0, last.getName().lastIndexOf('.')) + '-' + Constants.SampleSize + ".png");
      bm = load(sampleFile);
    }
    return bm;
  }

  static public Bitmap load(File file) {
    Bitmap bm = null;
    if ( file.exists() ) {
      final BitmapFactory.Options options = new BitmapFactory.Options();
      options.inSampleSize = 1;
      bm = BitmapFactory.decodeFile(file.toString(), options);
    }
    return bm;
  }

  private File _latestFileInDir(File dir) {
    File last = null;
    if (dir.isDirectory() && !dir.getName().equals("..")) {
      File[] files = dir.listFiles();
      if (files != null) {
        for ( File file: files) {
          if ( file.isDirectory() )
            file = _latestFileInDir(file);
          if ( file !=  null && (last == null || file.lastModified() > last.lastModified()) )
            last = file;
        }
      }
    }
    return last;
  }

  /** scale given bitmap
   * Always maintains aspect ratio, but not always full bitmap content: if
   * reqw == reqh image will be cropped by its largest dimension to obtain
   * its central square. If reqw != reqh, then the largest dimension will
   * be fitted in provided bounds.
   */
  public Bitmap scaleBitmap(File file, final int reqw, final int reqh) {

    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inSampleSize = 1;
    options.inJustDecodeBounds = true; // First, decode just source dimensions
    BitmapFactory.decodeFile(file.getAbsolutePath(), options);

    // Calculate inSampleSize: el factor de mayor reducción que mantenga resolución >= tamaño requerido
    if (options.outWidth > reqw || options.outHeight > reqh) {

      final int halfHeight = options.outHeight / 2;
      final int halfWidth = options.outWidth / 2;
      while ((halfHeight / options.inSampleSize) >= reqh
          || (halfWidth / options.inSampleSize) >= reqh) {
        options.inSampleSize *= 2;
      }
    }
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    Bitmap bm = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
    // Log.d("IMGR", String.format("inSampleSize: %d; out: %d,%d", options.inSampleSize, bm.getWidth(), bm.getHeight()));

    if (bm == null)
      return null;

    // resize to the exact dimensions we need
    float factor;   // to be multiplied by source dimensions to reduce them
    if (reqw == reqh)
      // we will crop the largest dimension in source, so let's calc
      // factor based in the shortest one
      factor = (float) reqw / Math.min(bm.getWidth(), bm.getHeight());
    else
      // fit the largest dimension into provided bounds choosing the
      // lower factor
      factor = Math.min( (float) reqw / bm.getWidth(), (float) reqh / bm.getHeight());
    int width = Math.round( factor * bm.getWidth());
    int height = Math.round( factor * bm.getHeight());

    // filtering: https://developer.android.com/reference/android/graphics/Paint#FILTER_BITMAP_FLAG
    Bitmap sbm = Bitmap.createScaledBitmap(bm, width, height, true);

    bm = Bitmap.createBitmap(sbm, (sbm.getWidth() - reqw) / 2, (sbm.getHeight() - reqh) / 2, reqw, reqh);
    return bm;
  }

  static public Bitmap getBitmapFromVectorDrawable(Resources resmgr, int drawableId) {
    Bitmap bitmap;
    Drawable drawable = ResourcesCompat.getDrawable(resmgr, drawableId, null);
    if (drawable != null) {
      bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap);
      drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
      drawable.draw(canvas);
    } else
      bitmap = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
    return bitmap;
  }

  static public int rotate(File file, int deg) {

    if (!file.exists())
      return -1;
    Bitmap img = load(file);
    if (img == null)
      return -1;
    Matrix matrix = new Matrix();
    matrix.postRotate(deg);
    Bitmap rotated = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
    String mime = Utils.mimeType(file.getName());
    Bitmap.CompressFormat fmt = Bitmap.CompressFormat.JPEG;
    if (mime.equals(Constants.PngMime))
      fmt = Bitmap.CompressFormat.PNG;
    try {
      FileOutputStream os = new FileOutputStream(file.getAbsolutePath());
      rotated.compress(fmt, 100, os);
    } catch (Exception ex) {
      Utils.loge(ex, ImageManager.instance().getClass().getSimpleName() + ".rotate");
      return -1;
    }

    return 0;
  }

}

