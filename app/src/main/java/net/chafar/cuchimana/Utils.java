/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.security.crypto.EncryptedSharedPreferences;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URLConnection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

@SuppressWarnings({"WeakerAccess","unused"})
public class Utils {

  public enum NetPref {
    all,
    wifionly,
    notroaming,
    never
  }

  static private EncryptedSharedPreferences _encprefs;   // https://developer.android.com/topic/security/data-android-versions

  // common background task, used for short-lived tasks (usually non-net tasks)
  static protected final ExecutorService _exe = Executors.newCachedThreadPool();

  static public ExecutorService exe() { return _exe; }

  /// returns first occurrence of any of chars in str or -1
  static public int firstCharOf(@NotNull String str, String chars) {
    char[] stra = str.toCharArray();
    for (int ii=0; ii<stra.length; ++ii) {
      if (chars.indexOf(stra[ii]) >= 0)
        return ii;
    }
    return -1;
  }

  static public boolean testFileName(@NonNull String name) {
    return 0 >= Utils.firstCharOf(name, "/\\?*%:|\"<>");
  }

  static public String mimeType(File file) {
    String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension(file));
    if (mime == null)
      switch (fileExtension(file)) {
        case Constants.GeoExtension: mime = Constants.GeoMime; break;
        case Constants.OpusExtension: mime = Constants.AudioOggMime; break;
        case Constants.AacExtension: mime = Constants.AudioAacMime; break;
      }
    return mime;
  }

  static public String mimeType(String name) {
    String mime = URLConnection.guessContentTypeFromName(name);
    if (mime == null && fileExtension(name).equals(Constants.GeoExtension))
      mime = Constants.GeoMime;
    return mime;
  }

  @NotNull
  static public String mimeMain(@NotNull String mime) {
    int pos = mime.indexOf('/');
    if (0>pos)
      return "";
    return mime.substring(0, pos);
  }

  @NotNull
  static public String fileExtension(@NotNull File file) {
    return fileExtension(file.toString());
  }

  @NonNull
  static public String getExtensionFromMimeType(String mime) {
    String ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(mime);
    if (ext == null)
      return "unknown_mime_extension_for_" + mime.replace('/','_');
    return ext;
  }

  static public String directory(String path) {
    File parent = new File(path).getParentFile();
    if (parent != null)
      return parent.getPath();
    return "";
  }

  @NotNull
  static public String fileExtension(@NotNull String path) {
    int pos = path.lastIndexOf('.');
    return pos < 0 ? "" : path.substring(pos+1);
  }

  @NotNull
  static public String fileNameWithoutExt(@NotNull File file) {
    String nam = file.getName();
    int pos = nam.lastIndexOf('.');
    return pos < 1 ? "" : nam.substring(0, pos);
  }

  static public byte[] loadFile(File file) {
    return loadFile(file, 0);
  }

  // max == 0, entire file
  static public byte[] loadFile(File file, int max) {
    int size = (int) file.length();
    if (0 < max && max < size)
      size = max;
    try {return loadFile(new FileInputStream(file), size); } catch(Exception ignored) { }
    return new byte[0];
  }

  static public byte[] loadFile(FileInputStream fis, int size) {
    byte[] bytes = new byte[size];
    try {
      BufferedInputStream buf = new BufferedInputStream(fis);
      buf.read(bytes, 0, bytes.length);
      buf.close();
    } catch ( IOException e) {
      e.printStackTrace();
    }
    return bytes;
  }

  static public boolean isdigit(Character ch) {
    return ch >= '0' && ch <= '9';
  }

  static public boolean isalpha(Character ch) {
    return ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <='z';
  }

  static public void logd(Exception ex, String tag) {
    Log.d(tag, ex.getMessage() != null ? ex.getMessage() : "???");
  }

  static public void loge(Exception ex, String tag) {
    Log.e(tag, ex.getMessage() != null ? ex.getMessage() : "???");
  }

  /// cuidado: esto no necesariamente implica tener acceso a internet: es estar conectado a una red
  static public boolean isNetworkAvailable(final Context context) {
    final ConnectivityManager connmgr = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
    return connmgr.getActiveNetworkInfo() != null && connmgr.getActiveNetworkInfo().isConnected();
  }

  /// DANGER: this uses net: don't use it in main thread or you'll get NetworkOnMainThreadException
  static public boolean isReachable(String host) {
    InetAddress address;
    try {
      address = InetAddress.getByName(host);
      if (address.isReachable(1000))
        return true;
    } catch ( IOException ex ) {
      Log.d(Utils.class.getSimpleName() + ", testing internet conn", ex.getMessage() != null ? ex.getMessage() : "failed");
    }
    return false;
  }

  static public NetPref getSyncPref(Context ctx) {
    String pref = getPref(ctx, R.string.autosyncpref, "off");
    switch ( pref ) {
      case "on":
        return NetPref.all;
      case "wifi":
        return NetPref.wifionly;
      case "notroaming":
        return NetPref.notroaming;
    }
    // "off"
    return NetPref.never;
  }

  static public boolean syncAllowed(Context ctx) {
    String host = getPref(ctx,R.string.synchostpref, "");
    NetPref syncpref = getSyncPref(ctx);
    if ( host.length() == 0 || syncpref == NetPref.never )
      return false;
    final ConnectivityManager connmgr = ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE));
    final Network net = connmgr.getActiveNetwork();
    if (net == null)
      return false;
    NetworkCapabilities caps = connmgr.getNetworkCapabilities(net);
    if ( caps == null || syncpref == NetPref.notroaming && !caps.hasCapability(NetworkCapabilities.NET_CAPABILITY_NOT_ROAMING))
      return false;
    return syncpref == NetPref.all
        || syncpref == NetPref.wifionly && caps.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
  }

  static public boolean getPref(Context ctx, String name, Boolean def) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    return prefs == null ? def : prefs.getBoolean(name, def);
  }

  static public String getPref(Context ctx, String name, String def) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    return prefs == null ? def : prefs.getString(name, def);
  }

  static public int getPref(Context ctx, String name, int def) {
    String prefstr = getPref(ctx, name, String.valueOf(def));
    return Integer.parseInt(prefstr);
  }

  static public boolean getPref(Context ctx, int resid, Boolean def) {
    return getPref(ctx, ctx.getString(resid), def);
  }

  static public String getPref(Context ctx, int resid, String def) {
    return getPref(ctx, ctx.getString(resid), def);
  }

  static public int getPref(Context ctx, int resid, int def) {
    return getPref(ctx, ctx.getString(resid), def);
  }

  static public void setEncryptedPrefs(EncryptedSharedPreferences prefs) { _encprefs = prefs; }

  static public boolean haveEncryptedPref(String key) {
    return _encprefs != null && _encprefs.contains(key);
  }

  static public boolean setEncryptedPref(String key, String val) {
    if (_encprefs == null)
      return false;
    SharedPreferences.Editor prefsed = _encprefs.edit();
    prefsed.putString(key, val);
    prefsed.apply();
    return true;
  }

  @SuppressWarnings("unused")
  static public boolean delEncryptedPref(String key) {
    if (_encprefs == null)
      return false;
    SharedPreferences.Editor prefsed = _encprefs.edit();
    prefsed.remove(key);
    prefsed.apply();
    return true;
  }

  @SuppressWarnings("unused")
  static public String getEncryptedPref(String key, String def) {
    return _encprefs != null ? _encprefs.getString(Constants.PrivKeyPass, def) : def;
  }

  public enum ErrCode {
    Ok,
    NotNeeded,
    Delayed,
    Ongoing,
    Unknown,
    Internal,   // bug
    NoNet,
    Conn,
    NotFound,
    BadParam,
    Perm,
    Unsupported;
    static public boolean error(ErrCode code) { return code != Ok && code != NotNeeded; }
    static public boolean warning(ErrCode code) {
      return code == ErrCode.NotNeeded;
    }
  }

  public interface ITaskResult {
    boolean error();
    boolean warning();
    void set(ErrCode err, String msg);
    ErrCode code();
    String msg();
  }

  static public class TaskResult implements ITaskResult {
    public final String taskname;
    public ErrCode errcode;
    public String msg;

    protected TaskResult() {
      taskname = "";
      errcode = ErrCode.Ok;
      msg = null;
    }

    protected TaskResult(String name, ErrCode code) {
      taskname = name;
      this.errcode = code;
    }

    TaskResult(String name, ErrCode code, String msg) {
      this.taskname = name;
      this.errcode = code;
      this.msg = msg;
    }

    public String tag() { return this.taskname; }

    public boolean error() {
      return ErrCode.error(errcode);
    }

    public boolean warning() {
      return ErrCode.warning(errcode);
    }

    public void set(ErrCode code, String msg) {
      this.errcode = code;
      this.msg = msg;
    }

    public ErrCode code() { return errcode; }

    public String msg() { return this.msg; }
  }

  static public class BackgroundTask extends FutureTask<Utils.ITaskResult> {
    final int _jobid;
    BackgroundTask(Context ctx, Callable<Utils.ITaskResult> callable, int jobid) {
      super(callable);
      _jobid = jobid;
    }
  }

}
