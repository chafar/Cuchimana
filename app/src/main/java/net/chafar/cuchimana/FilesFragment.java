/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static net.chafar.cuchimana.Constants.GeoMime;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.os.HandlerCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;

public class FilesFragment extends ItemsFragment implements FilenameFragment.FilenameListener, AcceptFragment.AcceptListener, LocHelper.LocHelperListener {

  static final int INTENT_REQ_CAMERA    = 1;
  static final int INTENT_REQ_PICK      = 2;
  static final int INTENT_REQ_EDIT      = 3;

  private File              _voiceFile;
  private VoiceRecording    _recording = null;

  private final ActivityResultLauncher<Intent> _activityLauncher =
      registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), res -> {
        Job job = Job.peek();
        if (job != null)
          processActivityResult(res.getResultCode(), res.getData(), job);
      });

  @Override
  public void onCreate(Bundle savedState) {
    super.onCreate(savedState);
    if (savedState != null) {
      String path = savedState.getString("intentfile");
      if (path != null)
        _intentFile = new File(path);
      path = savedState.getString("voicefile");
      if (path != null)
        _voiceFile = new File(path);
    }
    Job.load();
    // Si savedState != null es re-creación ¿hay algo que debamos hacer distinto?
    _actionModeCallback.setFragment(this);
    _actionModeCallback.setMenuId(R.menu.file_context_menu);
    // restore action mode folder_menu if some selection in place
    if (_dataModel.usingFiles() && _dataModel.selected() > 0)
      startActionMode();
  }

  @Override
  public void onPause() {
    super.onPause();
    if (LocHelper._helper != null)
      LocHelper._helper = null;
  }

  @SuppressLint("UnsupportedChromeOsCameraSystemFeature")
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    ViewGroup view = (ViewGroup) inflater.inflate(R.layout.folder_fragment, container, false);
    view.setId(R.id.filefragview);
    RecyclerView rv = view.findViewById(R.id.fragment_view);
    rv.setId(R.id.filefraglistview);
    final FilesViewAdapter adapter = new FilesViewAdapter(this);
    rv.setAdapter(adapter);
    // use a grid layout manager
    GridLayoutManager mgr = new GridLayoutManager(getContext(), ImageManager.instance().dpWidth() / Constants.SampleSize, RecyclerView.VERTICAL, false);
    rv.setLayoutManager(mgr);

    FloatingActionButton fab = view.findViewById(R.id.fab);
    if (!requireContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
      fab.hide(); // btm
    else {
      fab.setImageResource(R.drawable.ic_camera_plus);
      fab.setOnClickListener(view1 -> doCommand(R.id.menu_new_pic));
    }
    // cuidado, esto solo es llamado cuando se asigna el valor del MutableLiveData
// con setValue() o postValue(), lo que solo ocurre en FileManager.load() y en
// la creación, PERO TAMBIEN, y me cago en quien lo ha decido así, cuando
// el lyfecycle de la activity ejecuta el start de los fragments
    _dataModel.mgr().files().observe(getViewLifecycleOwner(), (Observer<List<File>>) files -> adapter.resetView());

    return view;
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    if (_intentFile != null)
      outState.putCharSequence("intentfile", _intentFile.getPath());
    if (_voiceFile != null)
      outState.putCharSequence("voicefile", _voiceFile.getPath());
  }

  private void shareFiles() {
    Log.d(getClass().getSimpleName(), ".shareFiles " + _dataModel.selected());
    if (_dataModel.selected() == 1) {
      File path = _dataModel.selectedItem(_dataModel.firstSelection());
      if ( Utils.fileExtension(path).equals(Constants.GeoExtension) ) {
        Uri uri = LocHelper.load(path);
        if (null != uri) {
          Intent shareIntent = new Intent(Intent.ACTION_SEND);
          shareIntent.addCategory(Intent.CATEGORY_DEFAULT);
          // sin type no encuentra activity y con type sí, pero no con type y uri;
          // whatsapp paise que solo funciona con text/plain y sin uri
          shareIntent.setType(GeoMime);
//          shareIntent.setData(uri);
          shareIntent.putExtra(Intent.EXTRA_TEXT, uri.toString());
          shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.location));
          if ( shareIntent.resolveActivity(requireContext().getPackageManager()) != null )
            startActivity(shareIntent);
          else
            show(getString(R.string.app_notfound), Utils.ErrCode.NotFound);
          return;
        }
      }
    }
    dispatchShareFilesIntent(_dataModel.selections());
  }

  @Override
  public void gotActive() {
    if (_dataModel != null)
      _dataModel.useFiles();
  }

  @Override
  protected boolean doCommand(int itemId) {
    if (super.doCommand(itemId))
      return true;
    Job job = Job.make(itemId);
    if ( itemId == R.id.menu_new_pic || itemId == R.id.menu_new_vid ) {
      dispatchCameraIntent(job);
      return true;
    } else if ( itemId == R.id.menu_new_voice ) {
      _voiceFile = null;
      try {
        _voiceFile = _dataModel.mgr().mkTempVoiceFile();
      } catch ( Exception ignored ) {
      }
      if ( _voiceFile != null ) {
        _recording = VoiceRecording.newInstance(_voiceFile.getPath(), job);
        _recording.show(requireActivity().getSupportFragmentManager(), "VoiceRecording");
      }
      return true;
    } else if ( itemId == R.id.menu_new_text ) {
      try {
        _intentFile = _dataModel.mgr().mkTempTextFile();
      } catch ( Exception ex ) {
        return false;
      }
      job.put(Job.StoreCode.file, _intentFile.getPath());
      launchFilenameFragment(itemId, _intentFile, null, job);
      return true;
    } else if ( itemId == R.id.menu_new_location ) {
      return takeLocation(job);
    } else if ( itemId == R.id.menu_pick ) {
      dispatchPickFileIntent(job);
      return true;
    } else if ( itemId == R.id.action_share ) {
      Job.pop();    // no result espected, so no need to remember anything
      shareFiles();
      return true;
    } else if ( itemId == R.id.action_recycle ) {
      if ( _dataModel.selected() == 1 ) {
        if (!SyncJob.isOngoing(job.get(Job.StoreCode.file, "")))
          doRecycle(job);
        else
          show(getString(R.string.nowsynching), Utils.ErrCode.Unknown);
      } else
        show(getString(R.string.toomanysels), Utils.ErrCode.Unknown);
      return true;
    } else if ( itemId == R.id.action_rotate ) {
      if ( _dataModel.selected() == 1 ) {
        int selpos = _dataModel.firstSelection();
        String mime = Utils.mimeType(_dataModel.name(selpos));
        mime = Utils.mimeMain(mime);
        if ( mime.equals("image") ) {
          job.put(Job.StoreCode.file, FileManager.instance().relFilePath(selpos));
          Utils.exe().execute(new RotateTask(this, _dataModel.selectedItem(selpos), 90, job.id));
        }
      } else
        show(getString(R.string.toomanysels), Utils.ErrCode.Unknown);
      return true;
    } else if ( itemId == R.id.action_edit ) {
      String path = FileManager.relPathOf(_dataModel.file(_dataModel.firstSelection()).getPath());
      if (SyncJob.isOngoing(path))
        show(getString(R.string.nowsynching), Utils.ErrCode.Unknown);
      else
        dispatchEditTextFileIntent( path );
      return true;
    } else if ( itemId == R.id.action_sync ) {
      Vector<Boolean> sels = _dataModel.selections();
      for ( int ii = 0; ii < sels.size(); ++ii ) {
        if ( sels.get(ii) )
          syncFile(job.id, _dataModel.mgr().relFilePath(ii));
      }
      return true;
    }
    Job.pop();   // unused
    return false;
  }

  @Override
  protected void itemClicked(int pos) {
    dispatchViewFileIntent(pos);
  }

  private Boolean takeLocation(Job job) {
    if ( ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
      //FIXME// this is possible on re-installations: we should ask for permission, but currently the activity holds a private code for that request
      return false;
    LocHelper locator = new LocHelper(this, job);
    if (!LocHelper.enabled()) {
      show(getString(R.string.location_disabled), Utils.ErrCode.Unknown);
      return true;
    }
    locator.cancel();    // in case there is some update request running
    File file;
    if (job.type == R.id.action_recycle) {
      int selpos = _dataModel.firstSelection();
      if ( selpos < _dataModel.selections().size() )
        file = _dataModel.selectedItem(selpos);
      else
        return false;
    } else {
      try {
        file = _dataModel.mgr().mkTempLocFile();
      } catch ( Exception ex ) {
        return false;
      }
    }
    Location loc = LocHelper.last();
    // accuracy: radius of circle inside wich there's a 68% of confidence to be in that circle
    if (loc != null && loc.getAccuracy() > 0.0 && loc.getAccuracy() < LocHelper.minAccuracy) {
      if (LocHelper.save(loc, file)) {
        onLocFileSaved(file, job);
        return true;
      }
      return false;
    } else {
      locator.search(file);
    }
    return true;
  }

  public Context ctx() { return requireActivity(); }

  public void onLocFileSaved(File file, Job job) {
    job.put(Job.StoreCode.file, file.getPath());
    if (job.type == R.id.action_recycle) {
      _dataModel.removeSamples(file);
      _dataModel.load();
      autoSyncItem(job, file.getPath(), SyncJob.CodOp.content);
    } else
      launchFilenameFragment(R.string.action_newlocation, file, null, job);
  }

  @Override
  protected void adjustActionMenu() {
    if (_actionMode != null) {
      Menu menu = _actionMode.getMenu();
      Vector<File> sels = _dataModel.selectedItems();
      int firstpos = _dataModel.firstSelection();
      MenuItem item = menu.findItem(R.id.action_recycle);
      if ( item != null) {
        String ext = "";
        if (firstpos < _dataModel.mgr().filesCount())
          ext = Utils.fileExtension(_dataModel.file(firstpos));
        if (!(firstpos < _dataModel.mgr().filesCount() && sels.size() == 1
            && (ext.equals(Utils.getExtensionFromMimeType(Constants.CamPicMime))
                || ext.equals(Utils.getExtensionFromMimeType(Constants.CamVidMime))
                || ext.equals(Utils.getExtensionFromMimeType(Constants.VoiceRecordMime))
                || ext.equals(Constants.GeoExtension))))
          menu.removeItem(R.id.action_recycle);
      }
      item = menu.findItem(R.id.action_rename);
      if ( item != null && sels.size() > 1)
        menu.removeItem(R.id.action_rename);
      item = menu.findItem(R.id.action_rotate);
      String main = Utils.mimeMain(Utils.mimeType(sels.get(0)));
      if ( item != null && ( sels.size() > 1 || sels.size() == 1 && !main.equals("image") ) )
        menu.removeItem(R.id.action_rotate);
      item = menu.findItem(R.id.action_mark);
      if ( item != null) {
        int ii;
        for ( ii = 0; ii < _dataModel.mgr().filesCount(); ++ii )
          if ( _dataModel.selected(ii) && !_dataModel.marked(ii) )
            break;
        if ( ii == _dataModel.mgr().filesCount())
          menu.removeItem(R.id.action_mark);
      }
      item = menu.findItem(R.id.menu_unmark);
      if ( item != null) {
        int ii;
        for ( ii = 0; ii < _dataModel.mgr().filesCount(); ++ii )
          if ( _dataModel.selected(ii) && _dataModel.marked(ii) )
            break;
        if ( ii == _dataModel.mgr().filesCount())
          menu.removeItem(R.id.menu_unmark);
      }
      item = _actionMode.getMenu().findItem(R.id.action_edit);
      if ( item != null ) {
        String ext = "";
        if (firstpos < _dataModel.mgr().filesCount())
          ext = Utils.fileExtension(_dataModel.file(firstpos));
        if (sels.size() != 1 || !ext.equals(Utils.getExtensionFromMimeType(Constants.TextMime)))
          menu.removeItem(R.id.action_edit);
      }
    }
  }

  private void doRecycle(Job job) {
    int selpos = _dataModel.firstSelection();
    String mime = Utils.mimeType(_dataModel.name(selpos));
    switch ( mime ) {
      case Constants.CamPicMime:
      case Constants.PngMime:
      case Constants.CamVidMime:
        dispatchCameraIntent(job);
        break;
      case Constants.VoiceRecordMime:
        if (selpos >= 0) {
          _voiceFile = _dataModel.selectedItem(selpos);
          _recording = VoiceRecording.newInstance(_voiceFile.getPath(), job);
          _recording.show(requireActivity().getSupportFragmentManager(), "VoiceRecording");
        }
        break;
      case GeoMime:
        takeLocation(job);
        break;
      default:
    }
  }

  private void dispatchCameraIntent(Job job) {
    Intent cameraIntent = null;
    _intentFile = null;
    if ( job.type == R.id.menu_new_pic )
        cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    else if ( job.type == R.id.menu_new_vid )
        cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
    else if ( job.type == R.id.action_recycle ) {
      String mime = Utils.mimeType(_dataModel.name(_dataModel.firstSelection()));
        switch(mime) {
          case "image/jpeg":
          case "image/png":
            cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            break;
          case "video/mp4":
            cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            break;
        }
    }
    if ( cameraIntent != null && cameraIntent.getAction() != null) {
      if (job.type == R.id.action_recycle) {
        int selpos = _dataModel.firstSelection();
        if ( selpos < _dataModel.selections().size() )
          _intentFile = _dataModel.selectedItem(selpos);
        else
          return;
      } else {
        try {
          switch ( cameraIntent.getAction() ) {
            case MediaStore.ACTION_IMAGE_CAPTURE:
              _intentFile = _dataModel.mgr().mkTempImageFile();
              break;
            case MediaStore.ACTION_VIDEO_CAPTURE:
              _intentFile = _dataModel.mgr().mkTempVideoFile();
              break;
          }
        } catch ( IOException ex ) {
          show(getString(R.string.file_error) + ": " + ex.getLocalizedMessage(), Utils.ErrCode.Unknown);
        }
      }
      if ( _intentFile != null) {
        Uri fileUri = FileProvider.getUriForFile(requireContext(), requireContext().getString(R.string.fileprovauth), _intentFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // Ensure that there's a camera activity to handle the intent
        PackageManager pm = requireContext().getPackageManager();
        if (pm != null && cameraIntent.resolveActivity(pm) != null) {
          job.code = INTENT_REQ_CAMERA;
          job.put(Job.StoreCode.file, _intentFile.getPath());
          Log.d(getClass().getSimpleName(), "passing uri to camera: " + fileUri.toString());
          _activityLauncher.launch(cameraIntent);
        } else {
          Job.pop();   // unused
          show(getString(R.string.app_notfound), Utils.ErrCode.Unknown);
        }
      }
    }
  }
  /// this doesn't uses job, because called activity returns nothing
  private void dispatchViewFileIntent(int pos) {
    File path = _dataModel.file(pos);
    if (path.exists()) {
      String mime = Utils.mimeType(path.getName());
      Intent showFileIntent;
      if (mime.equals(Constants.TextMime)) {
        showFileIntent = new Intent(requireActivity(), TextEditor.class);
        showFileIntent.setAction(Intent.ACTION_VIEW);
      } else
        showFileIntent = new Intent(Intent.ACTION_VIEW);
      if (Utils.fileExtension(path).equals(Constants.GeoExtension)) {
        // geo: uri, that's understood by both waze and google maps
        Uri uri = LocHelper.load(path);
        showFileIntent.setData(uri);
      } else {
        Uri fileUri = FileProvider.getUriForFile(requireContext(), requireContext().getString(R.string.fileprovauth), path);
        showFileIntent.setDataAndType(fileUri, mime);
        showFileIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Log.d("FILES", "passing uri to visor: " + fileUri.toString());
      }
      // Ensure that there's some activity to handle the intent
      if (showFileIntent.resolveActivity(requireContext().getPackageManager()) != null)
        startActivity(showFileIntent);
      else
        show(getString(R.string.app_notfound), Utils.ErrCode.Unknown);
    } else
      Log.e(getClass().getSimpleName(), "couldn't find file to show: " + path.toString());
  }

  private void dispatchShareFilesIntent(Vector<Boolean> sels) {
    File path;
    ArrayList<Uri> fileUris = new ArrayList<>();
    Intent shareFilesIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
    shareFilesIntent.setType("*/*");
    for (int ii=0; ii<_dataModel.mgr().filesCount(); ++ii) {
      if ( ii < sels.size() && sels.get(ii) ) {
        path = _dataModel.file(ii);
        if ( path.exists() ) {
          fileUris.add(FileProvider.getUriForFile(requireContext(), requireContext().getString(R.string.fileprovauth), path));
        }
      }
    }
    if (fileUris.size() > 0) {
      // Utilizado con chooser: shareFilesIntent.putExtra(Intent.EXTRA_TITLE, "TITLE: Shared by Cuchimana");
      // Util para subject de correos: shareFilesIntent.putExtra(Intent.EXTRA_SUBJECT, "Shared from " + getString(R.string.app_name));
      shareFilesIntent.putExtra(Intent.EXTRA_TEXT, "Shared from " + getString(R.string.app_name));
      shareFilesIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, fileUris);
      // Ensure that there's a camera activity to handle the intent
      if (shareFilesIntent.resolveActivity(requireContext().getPackageManager()) != null)
        startActivity(shareFilesIntent);
      else
        show(getString(R.string.app_notfound), Utils.ErrCode.Unknown);
    }
  }

  private void dispatchEditTextFileIntent(@NonNull String path) {
    Intent intent;
    _intentFile = new File(FileManager.absPath(path));
    if (Utils.getPref(requireActivity(), R.string.extedpref, false))
      intent = new Intent(Intent.ACTION_EDIT);
    else {
      // editor interno
      intent = new Intent(requireActivity(), TextEditor.class);
      intent.setAction(Intent.ACTION_EDIT);
    }
    Uri fileUri = FileProvider.getUriForFile(requireContext(), requireContext().getString(R.string.fileprovauth), _intentFile);
    intent.setDataAndType(fileUri, Constants.TextMime);
    Job job = Job.peek();
    intent.putExtra("jobid", job.id);
    if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
      job.put(Job.StoreCode.file, _intentFile.getPath());
      job.code = INTENT_REQ_EDIT;
      _activityLauncher.launch(intent);
    } else {
      Job.pop();   // unused
      show(getString(R.string.app_notfound), Utils.ErrCode.Unknown);
    }
  }

  private void dispatchPickFileIntent(Job job) {
    Intent intent = testPickFileIntent();
    if (intent != null) {
      intent.putExtra("jobid", job.id);
      job.code = INTENT_REQ_PICK;
      _activityLauncher.launch(intent);
    } else
      show(getString(R.string.app_notfound), Utils.ErrCode.Unknown);
  }

  protected Intent testPickFileIntent() {
    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.putExtra(Intent.CATEGORY_OPENABLE, true);
    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
    if (intent.resolveActivity(requireContext().getPackageManager()) != null)
      return intent;
    else {
      // ACTION_PICK no proporciona buenos resultados: sin setData() solo salen aplis que no
      // tienen na que ver con ficheros, y con setData() no encuentra apli
      intent = new Intent(Intent.ACTION_PICK);
      intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.cuchi_sel));
      if (intent.resolveActivity(requireContext().getPackageManager()) != null)
        return intent;
    }
    return null;
  }

  private void processActivityResult(int result, Intent intent, @NonNull Job job) {
    switch (job.code) {
      case INTENT_REQ_CAMERA:
        if ( result != RESULT_OK ) {
          if (job.type != R.id.action_recycle)
            _intentFile.delete();
          if (result != RESULT_CANCELED)
            show(getString(R.string.taking_aborted), Utils.ErrCode.Unknown);
          return;
        }
        if (job.type == R.id.action_recycle) {
          _dataModel.removeSamples(_intentFile);
          _dataModel.load();
          autoSyncItem(job, FileManager.relPathOf(_intentFile.getPath()), SyncJob.CodOp.content);
          commandDone();
        } else {
          job.put(Job.StoreCode.file, _intentFile.getPath());
          launchFilenameFragment(job.type == R.id.menu_new_pic ? R.string.action_newpic : R.string.action_newvideo, _intentFile, null, job);
        }
        break;
      case INTENT_REQ_PICK:
        if ( result == RESULT_OK)
          receiveFile(intent);
        else
          commandDone();
        break;
      case INTENT_REQ_EDIT:
        _dataModel.removeSamples(_intentFile);
        _dataModel.load();
        autoSyncItem(job, FileManager.relPathOf(_intentFile.getPath()), SyncJob.CodOp.content);
        commandDone();
        break;
    }
  }

  public void recordingDone(Job job, String name) {
    if (_voiceFile.exists() && _voiceFile.length() > 0 && !name.equals("")) {
      File newfile = new File(_voiceFile.getParent(), name + '.' + Utils.fileExtension(_voiceFile));
      if (!newfile.exists() || job.type == R.id.action_recycle) {
        if (_dataModel.renameFile(_voiceFile, name))
          autoSyncItem(job, new File(_dataModel.mgr().relPath(), name).getPath(), SyncJob.CodOp.content);
        else
          show( getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
      }
    } else if (_voiceFile.exists())
      _voiceFile.delete();
    _voiceFile = null;
    commandDone();
  }

  @Override
  public Boolean onFilenameOkClick(DialogFragment frm, String name) {
    if (super.onFilenameOkClick(frm, name))
      return true;
    FilenameFragment dlg = (FilenameFragment) frm;
    Job job = Job.peek();
    if ( job == null ) {
      Log.e(getClass().getSimpleName(), "no job after filename ok click");
      return false;
    }
    if ( job.type == R.id.menu_new_pic || job.type == R.id.menu_new_vid || job.type == R.id.menu_new_text || job.type == R.id.menu_new_location ) {
      File newfile = new File(_intentFile.getParent(), name + '.' + Utils.fileExtension(_intentFile));
      if ( !newfile.exists() ) {
        if ( job.type == R.id.menu_new_location && LocHelper.rename(name)
            || job.type != R.id.menu_new_location && _dataModel.renameFile(_intentFile, name) ) {
          autoSyncItem(job, FileManager.relPathOf(newfile.getPath()), SyncJob.CodOp.content);
          if ( job.type == R.id.menu_new_text ) {
            Job newjob = Job.make(R.id.action_edit);
            String path = FileManager.instance().relPath(name) + '.' + Utils.getExtensionFromMimeType(Constants.TextMime);
            newjob.put(Job.StoreCode.file, path);
            dispatchEditTextFileIntent(path);
          }
        }
      } else
        show(getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
    } else if ( job.type == R.id.action_rename ) {
      String oldpath = job.get(Job.StoreCode.file,"");
      String oldrelpath = FileManager.relPathOf(oldpath);
      String newrelpath = _dataModel.mgr().relPath(dlg.name()) + '.' + Utils.fileExtension(oldpath);
      if (!newrelpath.equals(oldrelpath)) {
        if (_dataModel.renameFile(new File(oldpath), dlg.name()))
          autoRenameSyncItem(job.id, newrelpath, oldrelpath);
        else
          show(getString(R.string.couldnt_complete_action), Utils.ErrCode.Unknown);
      }
    } else {
      return false;
    }
    commandDone();
    return true;
  }

  @Override
  public void onFilenameCancelled() {
    if (_intentFile != null && _intentFile.exists())
      _intentFile.delete();
    commandDone();
  }

  private void rotated() {
    Job job = Job.peek();
    FileManager.instance().removeSamples(new File(job.get(Job.StoreCode.file,"")));
    autoSyncItem(job, job.get(Job.StoreCode.file, "path"), SyncJob.CodOp.content);
    commandDone();
    _dataModel.load();
  }

  @SuppressWarnings("SameParameterValue")
  private static class RotateTask extends Utils.BackgroundTask {
    final File _file;
    final FilesFragment _frag;
    final Handler _postRotate;
    // file: path absoluto
    protected RotateTask(FilesFragment frag, File file, int deg, int jobid) {
      super(frag.requireActivity(), new RotateCallable(file, deg), jobid);
      _frag = frag;
      _file = file;
      // https://developer.android.com/guide/background/threading#java
      _postRotate = HandlerCompat.createAsync(Looper.getMainLooper());
    }
    @Override
    protected void done() {
      if (!isCancelled())
        _postRotate.post(_frag::rotated);
    }
  }

  private static class RotateCallable implements Callable<Utils.ITaskResult> {
    private final File _file;
    private final int _deg;

    protected RotateCallable(File file, int deg) {
      _file = file;
      _deg = deg;
    }

    @Override
    public Utils.TaskResult call() {
      Utils.TaskResult res = new Utils.TaskResult("rotate", Utils.ErrCode.Ok);
      if ( 0>ImageManager.rotate(_file, _deg) )
        res.errcode = Utils.ErrCode.Internal;
      return res;
    }
  }
}
