/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;
import androidx.preference.EditTextPreferenceDialogFragmentCompat;

public class CuSettings extends ActivityBase implements FragmentOnAttachListener {
  @SuppressWarnings("FieldCanBeLocal")
  private SettingsFragment  _settingsfrag = null;

//  public Fragment current() { return null; }

  public void onAttachFragment(@NonNull FragmentManager mgr, @NonNull Fragment fragment) {
    super.onAttachFragment(mgr, fragment);
    Log.d(getClass().getSimpleName(), fragment.getClass().getSimpleName());
    if ( fragment instanceof EditTextPreferenceDialogFragmentCompat ) {
      EditTextPreferenceDialogFragmentCompat df = (EditTextPreferenceDialogFragmentCompat) fragment;
      /////////////// aki getDialog() devuelve null
      Dialog dlg = df.getDialog();
      if (dlg != null)
        Log.d(getClass().getSimpleName(), dlg.getClass().getSimpleName());
    }
    if (fragment instanceof SettingsFragment) {
      _settingsfrag = (SettingsFragment) fragment;
      _settingsfrag.setDataModel(_dataModel);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_settings);
    }

}
