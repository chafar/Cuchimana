/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

final class Constants {
  static public final String RootDir = "Cuchimana";
  /// preferences file for miscellaneous cache cache data
  static public final String StateFile = "state";
  /// StateFile preferences key for current dir
  static public final String CurrDirKey = "currdir";

  /// preferences file for sensible data, entrypted via EncryptedSharedPreferences
  static public final String EncryptedPrefs = "encripted";
  /// Encrypted preferences key for SSH priv key passphrase
  static public final String PrivKeyPass = "privpass";
  /// basename for sftp key pair files
  static public final String SyncKeysFileName = "synckey";

  static public final int SampleSize = 150;
  // it seems to be some consensus about those to be the best assumptions w/respect to camera output formats
  static public final String CamPicMime = "image/jpeg";
  static public final String CamVidMime = "video/mp4";
  static public final String PngMime = "image/png";
  static public final String VoiceRecordMime = "audio/amr";
  static public final String AudioOggMime = "audio/ogg";
  static public final String AudioAacMime = "audio/aac";
  static public final String AudioMpeg = "audio/mpeg";
  static public final String PdfMime = "application/pdf";
  static public final String TextMime = "text/plain";
  static public final String GeoMime = "application/geo+json";
  static public final String GeoExtension = "geojson";
  static public final String OpusExtension = "opus";
  static public final String AacExtension = "aac";
//  static public final String FileProviderAuthority = "net.chafar.android.fileprovider";

}
