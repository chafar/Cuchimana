/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.util.Base64;
import android.util.Log;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.HostKey;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.KeyPair;
import com.jcraft.jsch.Logger;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Vector;

/**
 * JSch:
 *
 * Configuraciones soportadas x jsch: http://epaul.github.io/jsch-documentation/javadoc/
 * en Class OpenSSHConfig
 *
 * About JSch identities:
 *
 * You can add named identities to JSch object, but theese names won't be used for anything
 * by the library except to remove one of those identities identified by the name you
 * passed when added.
 *
 * When connecting, all added identities will be tried in turn until one of them is accepted
 * by the server. 'Accepted' means that the server recognizes the username + pubkey pair
 * presented by as candidate to be authenticated; then, the real authentication involving
 * private key is tried and if it fails, whole authentication process failes.
 *
 * So, to be clear: the name you _may_ give to each one of the identities registered against
 * JSch object is not used in any way in the authentication process.
 *
 * UserInfo is intended mainly for interactive authentication (though it may be used in other
 * ways depending on your implementation). But if you are not using passphrase protected
 * private key or you can provide it without asking the user for it, you don't need UserInfo.
 *
 */
class SSH {

  private static JSch _jsch = null;

  static public JSch engine() {
    if (_jsch == null) {
      _jsch = new JSch();
      JSch.setLogger(new Logger() {
        @Override
        public boolean isEnabled(int level) {
          return true;
        }
        @Override
        public void log(int level, String message) {
          switch (level) {
            case Logger.INFO: Log.i("JSch", message); break;
            case Logger.DEBUG: Log.d("JSch", message); break;
            case Logger.WARN: Log.w("JSch", message); break;
            case Logger.ERROR: Log.e("JSch", message); break;
          }
        }
      });
    }
    return _jsch;
  }

  static public Session newSession(String username, String hostname, int port, String pw) {
    Session sess = newSession(username, hostname, port);
    if (sess != null)
      sess.setPassword(pw);
    return sess;
  }

  static public Session newSession(String username, String hostname, int port) {
    Session sess = null;
    try {
      sess = engine().getSession(username, hostname, port);
    } catch ( JSchException ex ) {
      ex.printStackTrace();
    }
    return sess;
  }

  static public boolean connect(Session sess) {
    if (sess.isConnected())
      return true;
    // una vez conectada, la pw se pierde en la sesión: no sirve para volver a conectar
    try { sess.connect(); }
    catch (Exception ex) {
      Utils.logd(ex, "SSH");
      return false;
    }
    return true;
  }

  static public String doCommand(Session sess, String cmd) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    boolean disconn = false;
    try {
      if (!sess.isConnected())
        if (connect(sess))
          disconn = true;
        else
          return "";

      // SSH Channel (exec, shell, sftp, subsystem
      ChannelExec channelssh = (ChannelExec) sess.openChannel("exec");
      channelssh.setOutputStream(baos);

      // Execute command
      channelssh.setCommand(cmd);
      channelssh.connect();
      // sin esto no funciona: quizá en connect() solo se hace la petición al servidor y hay que dar tiempo a que se ejecute,
      // pero no veo nada que pueda indicar si se ha ejecutado o no: lo suyo sería hacer un ciclo con un sleep pequeñito
      // e ir mirando a ver si se había completado, pero ¿cómo se sabe si se ha completado?
      Thread.sleep(100);
      channelssh.disconnect();
    } catch (Exception ex) {
      ex.printStackTrace();
      return ex.getMessage();
    }
    if (disconn)
      sess.disconnect();
    return baos.toString();
  }

  /** stores new public key pair into the given dir+basename files, with private key protected by pass
   */
  static public boolean newKeyPair(String comment, String dir, String basename, byte[] pass) {
    KeyPair keys = null;
    // la clave ECDSA es mas pequeña (con menos bits (2048 RSA ~ 224 ECDSA
    // ECDSA es mas rápida firmando y mas lenta verificando, ... pero no me ha funcionado, RSA sí
    // try { keys = KeyPair.genKeyPair(engine(), KeyPair.ECDSA, 256); }
    try { keys = KeyPair.genKeyPair(engine(), KeyPair.RSA, 2048); }
    catch (JSchException ignored ) { }
    File file = new File(dir, basename + ".pub");
    if (keys != null)
      try {
        keys.writePublicKey(file.getPath(), comment);
        file = new File(dir, basename + ".priv");
        keys.writePrivateKey(file.getPath(), pass);
      } catch (Exception ignored) { return false; }
    return true;
  }

  /** Returns type string found in public key standar {type} {base64-key} {comment} format
   *  or null if no space found in keydesc
   */
  static public String getKeyTypeString(String keydesc) {
    String[] parts = keydesc.split(" ");
    if (parts.length > 1)
      return parts[0];
    return null;
  }

  static public byte[] getKey(String keydesc) {
    String[] parts = keydesc.split(" ");
    if (parts.length > 1)
      return Base64.decode(parts[1], Base64.DEFAULT);
    return null;
  }

  /** returns HostKey int constant (SSHDSS, SSHRSA, ECDSA256, ECDSA384, ECDSA521)
   *  from key typè name as returned by getKeyTypeString() or -1 if not recognized
   */
  static public int getKeyType(String typestr) {
    switch ( typestr ) {
      case "ssh-rsa":
      case "ssh-dss":
        return HostKey.SSHDSS;
      case "ecdsa-sha2-nistp256":
        return HostKey.ECDSA256;
      case "ecdsa-sha2-nistp384":
        return HostKey.ECDSA384;
      case "ecdsa-sha2-nistp521":
        return HostKey.ECDSA521;
    }
    return -1;
  }

  static public class SSHResult extends Utils.TaskResult {
    static public final String name = "SSH";
    public SSHResult(Utils.ErrCode code, String msg) {
      super(name, code, msg);
    }
    public SSHResult(Utils.ErrCode code) {
      super(name, code);
    }
    public SSHResult(Utils.TaskResult other) {
      super(name, other.errcode, other.msg);
    }
    protected SSHResult(String name, Utils.ErrCode code, String msg) {
      super(name, code, msg);
    }
    protected SSHResult(String name, Utils.ErrCode code) {
      super(name, code);
    }
  }

}

class SFTP {

  static public final int ErrNone = 0;         // Ok
  static public final int ErrEof = 1;          // EOF
  static public final int ErrNotFound = 2;     // No such file
  static public final int ErrPemission = 3;    // Permission denied
  static public final int ErrFailure = 4;      // Unspecified
  static public final int ErrProto = 5;        // Protocol error reported by the server
  static public final int ErrNoConn = 6;       // this is client error
  static public final int ErrConnLost = 7;     // Connection lost
  static public final int ErrNotSupported = 8; // Unspecified
  static public final int ErrExists = 11;      // Already exists
  static public final int ErrProtected = 12;   // Write protected
  static public final int ErrNotEmpty = 18;    // Dir not empty
  static public final int ErrNotDir = 19;      // Not a directory
  static public final int ErrInvalidName = 20; // Invalid file name
  static public final int ErrCantDelete = 22;  // Cannot delete
  static public final int ErrIsDir = 24;       // Path is dir when file expected
  static public final int ErrOther = -1;       // One of sftp codes not defined here

  @SuppressWarnings({"SameParameterValue", "umused"})
  static public class SFTPResult extends Utils.TaskResult {
    static public final String name = "SFTP";
    // from SftpException.id, that (at least, some times) reflects error code sent by sftp server
    @SuppressWarnings("unused")
    public int sftpcode;
    SFTPResult(Utils.ErrCode code, String msg) {
      super(name, code, msg);
      sftpcode = ErrNone;
    }
    SFTPResult(Utils.ErrCode code){
      super(name, code);
      sftpcode = ErrNone;
    }
    void from(SftpException ex) {
      errcode = codeFromSftp(ex.id);
      sftpcode = ex.id;
      msg = ex.getMessage();
    }
    static Utils.ErrCode codeFromSftp(int sftpcode) {
      //noinspection SwitchStatementWithTooManyBranches
      switch (sftpcode) {
        case ErrNone: return Utils.ErrCode.Ok;
        case ErrNotFound: return Utils.ErrCode.NotFound;
        case ErrConnLost:
        case ErrNoConn: return Utils.ErrCode.Conn;       // this is client error
        case ErrPemission:
        case ErrProtected:
        case ErrCantDelete: return Utils.ErrCode.Perm;
        case ErrExists:
        case ErrNotEmpty:
        case ErrNotDir:
        case ErrInvalidName:
        case ErrIsDir: return Utils.ErrCode.BadParam;
        case ErrProto:
        case ErrNotSupported: return Utils.ErrCode.Internal;
        case ErrEof:
        case ErrFailure:
        case ErrOther: return Utils.ErrCode.Unknown;
      }
      return Utils.ErrCode.Unknown;
    }

  }

  static public class SFTPStatResult extends SFTPResult {
    public SftpATTRS attrs;

    @SuppressWarnings("SameParameterValue")
    SFTPStatResult(Utils.ErrCode code) {
      super(code);
    }

    @SuppressWarnings("unused")
    SFTPStatResult(SftpATTRS attrs, Utils.ErrCode code) {
      super(code);
      this.attrs = attrs;
    }

    boolean exists() {
      return attrs != null && errcode != Utils.ErrCode.NotFound;
    }
  }

  @SuppressWarnings("unchecked")
  static public Vector<ChannelSftp.LsEntry> ls(ChannelSftp chan, String path) {

    Vector<ChannelSftp.LsEntry> ls = null;
    try {
      if (!chan.isConnected())
        chan.connect();
      // si el relpath no existe, devuelve null
      ls = (Vector<ChannelSftp.LsEntry>) chan.ls(path);
    } catch ( Exception ex ) {
      ex.printStackTrace();
    }

    return ls;
  }

  static public SFTPResult cd(ChannelSftp chan, String path) {
    return _cd(chan, path, false);
  }

  static public SFTPResult lcd(ChannelSftp chan, String path) {
    return _cd(chan, path, true);
  }

  /// returns Constants.ErrCode.NotFound if path doesn't exist
  private static SFTPResult _cd(ChannelSftp chan, String path, boolean local) {
    SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      if (local)
        chan.lcd(path);
      else
        chan.cd(path);
    } catch ( JSchException ex ) {
      res.set(Utils.ErrCode.Unknown, ex.getMessage());
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  static public SFTPResult mkdir(ChannelSftp chan, String path) {

    SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      chan.mkdir(path);
    } catch ( JSchException ex ) {
      res.set(Utils.ErrCode.Unknown, ex.getMessage());
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  static public SFTPResult rename(ChannelSftp chan, String oldpath, String newpath) {
    SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      chan.rename(oldpath, newpath);
    } catch ( JSchException ex ) {
      res.set(Utils.ErrCode.Unknown, ex.getMessage());
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  static public String pwd(ChannelSftp chan) {
    return _pwd(chan, false);
  }

  static public String lpwd(ChannelSftp chan) {
    return _pwd(chan, true);
  }

  private static String _pwd(ChannelSftp chan, boolean local) {
    String dir;
    try {
      if (!chan.isConnected())
        chan.connect();
      if (local)
        dir = chan.lpwd();
      else
        dir = chan.pwd();
    } catch ( JSchException | SftpException ex ) { return null; }
    return dir;
  }

  /** src and dst may be absolute or relative paths
   *
   * @param mode must be ChannelSftp.OVERWRITE/RESUME/APPEND
   */
  static public SFTPResult put(ChannelSftp chan, String src, String dst, int mode) {
    SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      chan.put(src, dst, mode);
    } catch ( JSchException ex ) {
      res.set(Utils.ErrCode.Unknown, ex.getMessage());
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  /** if server reports file doesn't exist, SftpATTRS returned will get its UID and GID set to -1
   */
  static public SFTPStatResult stat(ChannelSftp chan, String path) {
    SFTPStatResult res = new SFTPStatResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      res.attrs = chan.stat(path);
    } catch ( JSchException ex ) {
      res.errcode = Utils.ErrCode.NotFound;
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  private static SFTPResult setStat(ChannelSftp chan, String path, SftpATTRS attrs) {
    SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
    try {
      if (!chan.isConnected())
        chan.connect();
      chan.setStat(path, attrs);
    } catch ( JSchException ex ) {
      res.errcode = Utils.ErrCode.Unknown;
    } catch ( SftpException ex ) {
      res.from(ex);
    }
    return res;
  }

  @SuppressWarnings("unused")
  static class Session {

    private boolean _ownedsess = false;
    // jcraft session
    private final com.jcraft.jsch.Session _sess;
    // jcraft channel
    private ChannelSftp _sftp = null;
    // jcraft hostkey
    private HostKey _hostkey = null;

    Session(String username, String hostname, int port) {
      _sess = SSH.newSession(username, hostname, port);
      if (_sess != null)
        _ownedsess = true;
    }

    Session(String username, String hostname, int port, String pw) {
      _sess = SSH.newSession(username, hostname, port, pw);
      if (_sess != null)
        _ownedsess = true;
    }

    Session(com.jcraft.jsch.Session sess) {
      _sess = sess;
    }

    public String host() {
      return _sess != null ? _sess.getHost() : "";
    }

    // destructor
    protected void finalize() {
      if (_sftp != null && _sftp.isConnected())
        _sftp.disconnect();
      if (_ownedsess && _sess.isConnected())
        _sess.disconnect();
    }

    public SFTPResult connect() {
      SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
      if (!_sess.isConnected())
        if (!SSH.connect(_sess))
          return new SFTPResult(Utils.ErrCode.Conn, "SSH primary");
      try {
        if (_sftp == null)
          _sftp = (ChannelSftp) _sess.openChannel("sftp");
        if (_sftp != null)
          _sftp.connect();
      } catch ( JSchException ex ) {
        res.set(Utils.ErrCode.Conn, ex.getMessage());
      }
      return res;
    }

    public SFTPResult connect(String keydir, String keynam, String pass) {
      if (_sess == null)
        return new SFTPResult(Utils.ErrCode.Internal);
      if (_hostkey != null) {
        _sess.getHostKeyRepository().add(_hostkey, null);
      } else
        _sess.setConfig("StrictHostKeyChecking", "no");
      _sess.setConfig("PreferredAuthentications", "publickey");
      try {
        byte[] priv = Utils.loadFile(new File(keydir, keynam + ".priv"));
        byte[] pub = Utils.loadFile(new File(keydir, keynam + ".pub"));
        SSH.engine().addIdentity(_sess.getUserName(), priv, pub, pass.getBytes());
      } catch (JSchException ignored) {
        return new SFTPResult(Utils.ErrCode.Internal);
      }
      return connect();
    }

    public void disconnect() {
      if (_sess != null && _sess.isConnected())
        _sess.disconnect();
    }

    public boolean isConnected() {
      return _sess.isConnected() && _sftp.isConnected();
    }

    public String user() { return _sess.getUserName(); }

    public String pwd() {
      return SFTP.pwd(_sftp);
    }

    public SFTPResult cd(String path) {
      return SFTP.cd(_sftp, path);
    }

    public String lpwd() {
      return SFTP.lpwd(_sftp);
    }

    public SFTPResult lcd(String path) {
      return SFTP.lcd(_sftp, path);
    }

    public SFTPResult mkdir(String path) {
      return SFTP.mkdir(_sftp, path);
    }

    public SFTPResult recursiveMakeDir(String path) {
      SFTPResult res = new SFTPResult(Utils.ErrCode.Ok);
      SFTPStatResult sta = this.stat(path);
      if ( sta.exists() ) {
        if ( ! sta.attrs.isDir() )
          res.errcode = Utils.ErrCode.BadParam;
      } else {
        String parent = Utils.directory(path);
        if ( parent. length() < 1 )
          res.errcode = Utils.ErrCode.BadParam;
        else {
          res = recursiveMakeDir(parent);
          if ( !res.error() )
            res = mkdir(path);
        }
      }
      return res;
    }

    public SFTPResult rename(String oldpath, String newpath) { return SFTP.rename(_sftp, oldpath, newpath); }

    /** ChannelSftp.OVERWRITE
     *
     * @param mode must be ChannelSftp.OVERWRITE/RESUME/APPEND
     */
    public SFTPResult put(String src, String dst, int mode) {
      return SFTP.put(_sftp, src, dst, mode);
    }

    public SFTPResult put(String path) {
      return SFTP.put(_sftp, path, path, ChannelSftp.OVERWRITE);
    }

    public Vector<ChannelSftp.LsEntry> ls(String path) {
      return SFTP.ls(_sftp, path);
    }

    public SFTPStatResult stat(String path) {
      return SFTP.stat(_sftp, path);
    }

    public SFTPResult setStat(String path, SftpATTRS attrs) { return SFTP.setStat(_sftp, path, attrs); }

    public void setRSAHostKey(byte[] key) {
      setHostKey(key, HostKey.SSHRSA);
    }

    public void setHostKey(byte[] key) {
      if (_sess != null) {
        try {
          this._hostkey = new HostKey(_sess.getHost(), key);
        } catch ( JSchException ex ) {
          Utils.loge(ex, getClass().getSimpleName());
        }
      }
    }

    public void setHostKey(byte[] key, int type) {
      if (_sess != null) {
        try {
          this._hostkey = new HostKey(_sess.getHost(), type, key);
        } catch ( JSchException ex ) {
          Utils.loge(ex, getClass().getSimpleName());
        }
      }
    }
  }

}