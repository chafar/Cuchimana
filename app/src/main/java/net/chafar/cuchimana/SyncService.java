/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;

/* SyncHandler utiliza el Looper de una hebra no UI para operar. Es para lo que existe.
 * Comunica resultados directamente a la activity, que es hebra UI; para ello
 * se utiliza Messenger, que soporta comunicación entre hebras.
 *
 * Para detener el servicio, en lugar de usar stopSelf() se puede usar stopSelf(int),
 * donde el Int es el startId de un onStartCommand(): solo será efectivo el stop
 * cuando el int sea igual al último startId recibido.
 */
/*
  Synchronization strategy:

  For now, we only support authentication via public key pair. Client key pair get (re-)generated
  when one or both of host or user name preferences change. SyncFragment shows user public key
  so that the user may copy it to the clipboard and manage its inclusion into the authorized_keys
  file in the host. User key pair get stored in FileManager.userDir(), that points to the user app's
  private file area.
*/
public class SyncService extends Service {

  private enum What {
    job,
    disconnect
  }

  private static SyncService.SyncHandler _bghnd;

  @Override
  public void onCreate() {
    super.onCreate();
    HandlerThread th = new HandlerThread("SyncThread", Process.THREAD_PRIORITY_BACKGROUND);
    th.start();
    _bghnd = new SyncHandler(th.getLooper(), getBaseContext().getFilesDir().getPath());
  }

  @Override
  public void onDestroy() {
    _bghnd.disconnect();
    _bghnd = null;
    super.onDestroy();
  }

  @Override
  public void onLowMemory() {
    // free some resources
    Message msg = _bghnd.obtainMessage(What.disconnect.ordinal());
    _bghnd.sendMessage(msg);
    super.onLowMemory();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    // not used, by now
    return null;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);

    // when re-started after killed in START_STICKY mode, intent is null
    if (intent != null) {
      int op = intent.getIntExtra("code", -1);
      if ( 0 > op || op >= SyncJob.CodOp.values().length ) {
        Log.d(getClass().getSimpleName(), String.format("Intent received with wrong ope code: %d", op));
        return START_NOT_STICKY;
      }
      Log.d(getClass().getSimpleName(), String.format("sync intent, code: %s, jobid: %d, path: %s",
          SyncJob.CodOp.values()[op].toString(), intent.getIntExtra("jobid", 0), intent.getStringExtra("path")));
      if ( null == FileManager.instance() ) {
        Log.d(getClass().getSimpleName(), "FileManager instance not available");
        return START_NOT_STICKY;
      }
      SyncJob job;
      if ( op == SyncJob.CodOp.queue.ordinal() ) {
        // take job from queue
        Log.d(getClass().getSimpleName(), String.format("%d jobs in queue ... processing", SyncJob.size()));
        if (SyncJob.size() == 0)
          return START_NOT_STICKY;
        job = SyncJob.peek();
        if (job.res.code() != Utils.ErrCode.Delayed)
          return START_NOT_STICKY;
      } else
        // new job
        job = makeJob(intent);
      Messenger replayto = intent.getParcelableExtra("receiver");
      if ( ! Utils.syncAllowed(getBaseContext()) ) {
        job.res = new SyncResult(Utils.ErrCode.Delayed, "Onlywifi active and no wifi connection");
        if ( null != replayto )
          sendResponse(replayto, job);
        return START_NOT_STICKY;
      }
      Message msg = _bghnd.obtainMessage(What.job.ordinal());
      msg.obj = job;
      msg.replyTo = intent.getParcelableExtra("receiver");
      _bghnd.sendMessage(msg);
    }
    // don't need to be sticky, since activity saves delayed jobs and resubmits them when resumed
    return START_NOT_STICKY;
  }

  private SyncJob makeJob(Intent intent) {
    // this is the main/UI thread, so no sync needed to manage jobs queue
    SyncJob job = SyncJob.make(SyncJob.CodOp.values()[intent.getIntExtra("code", -1)], intent.getStringExtra("path"));
    job.path = intent.getStringExtra("path");
    job.privkeypass = intent.getStringExtra("privkeypass");
    job.params = getSyncPrefs(getBaseContext());
    if ( job.code == SyncJob.CodOp.renamed )
      job.oldpath = intent.getStringExtra("oldpath");
    return job;
  }

  static public SSHParams getSyncPrefs(Context ctx) {
    SSHParams params = new SSHParams();
    params.user = Utils.getPref(ctx, R.string.syncuserpref, "");
    params.host = Utils.getPref(ctx,R.string.synchostpref, "");
    String hostkey = Utils.getPref(ctx, R.string.synchostkeypref, "");
    params.pubkey = hostkey.equals("") ? null : SSH.getKey(hostkey);
    return params;
  }

  static protected void sendResponse(@NonNull Messenger dest, SyncJob job) {
    Message resp = Message.obtain();
    resp.what = ActivityBase.SyncReceiver.What.syncjob.ordinal();
    resp.obj = job;
    try { dest.send(resp); }
    catch ( RemoteException e ) {
      Log.e(SyncService.class.getSimpleName(), String.format("send message exception: %s", e.toString()));
    }
  }

  private final class SyncHandler extends Handler {
    protected SFTP.Session _sess;
    final String _keyDir;
    String _remBaseDir;

    protected SyncHandler(@NonNull Looper looper, String keyDir) {
      super(looper);
      _sess = null;
      _keyDir = keyDir;
    }

    /*
     * About local/remote working dirs, the  policy is to cd'ing to parents
     * of root Cuchimana dir, in order to process without further cd'ing the
     * FileManager rel paths that come in msg.job.path / oldpath
     */
    @Override
    public void handleMessage(@NonNull Message msg) {
      super.handleMessage(msg);
      Log.d(getClass().getSimpleName(), String.format("msg received: %s", What.values()[msg.what].toString()));
      if (msg.what == What.disconnect.ordinal())
        return;
      SyncJob job = (SyncJob) msg.obj;
      job.res.set(Utils.ErrCode.Ongoing, "handling syncing");
      Log.d(getClass().getSimpleName(), String.format("processing job: %s", job.toString()));
      SyncResult res = new SyncResult( Utils.ErrCode.Ok);
      if ( ! job.params.test() )
        res = new SyncResult( Utils.ErrCode.BadParam );
      else {
        if (_sess == null || !_sess.host().equals(job.params.host) || !_sess.user().equals(job.params.user)) {
          if (_sess != null)
            _sess.disconnect();
          _sess = new SFTP.Session(job.params.user, job.params.host, job.params.port);
          _sess.setHostKey(job.params.pubkey);
        }
        if (!_sess.isConnected()) {
          res = connect(job);
          if ( _sess.isConnected() ) {
            _remBaseDir = _sess.pwd();
            if ( _remBaseDir == null )
              res = new SyncResult( Utils.ErrCode.Unknown );
          } else if (res.code() == Utils.ErrCode.NoNet)
            res.set(Utils.ErrCode.Delayed, "Net not available");
        }
        if ( _sess.isConnected() && !res.error() ) {
          switch ( job.code ) {
            case content:
              res = new SyncResult(_sess.lcd(Environment.getExternalStorageDirectory().getPath()));
              if ( !res.error() )
                res = new SyncResult(_sess.cd(_remBaseDir));
              if ( !res.error()) {
                File file = new File(FileManager.absPath(job.path));
                if ( file.isDirectory() )
                  res = syncDir(job.path, job.path);
                else
                  res = syncFile(job.path);
              }
              break;
            case renamed:
              res = syncRename(job);
              break;
            case removed:
              // pendiente de implementar el rm en SFTP/Session u aquí syncRemove()
//              res = syncRemove(job);
              break;
            case test:
              // already connected here
              Log.i(this.getClass().getSimpleName(), String.format("testing %s ok",  job.params.user + '@' + job.params.host));
              break;
          }
        }
      }
      job.res = res;
      if ( job.res.msg == null )
        job.res.msg = String.format("Sync %s result %s", job.code.toString(), res.errcode.toString());
      Log.d(getClass().getSimpleName(), String.format("sending sync completion: %d, code: %s, result: %s", msg.what, job.code.toString(), job.res.errcode.toString()));
      // send msg to activity
      if (msg.replyTo != null)
        sendResponse(msg.replyTo, job);
      else
        Log.e(getClass().getSimpleName(), "... couldn't send message cause no listener declared");
    }

    private SyncResult syncRename(SyncJob job) {
      Log.i(this.getClass().getSimpleName(), "renaming " + job.oldpath + " to " + job.path);
      return new SyncResult(_sess.rename(job.oldpath, job.path));
    }

    private SyncResult syncFile(String path) {
      SyncResult res;
      int lmtime = FileManager.lmtime(new File(FileManager.absPath(path)));
      long rmtime = 0;
      SFTP.SFTPStatResult remres = _sess.stat(path);
      if ( remres.error() && remres.errcode != Utils.ErrCode.NotFound )
        return new SyncResult(remres);
      if (remres.attrs != null)
        rmtime = remres.attrs.getMTime();
      if ( rmtime < lmtime ) {
        Log.i(this.getClass().getSimpleName(), "sending " + path);
        res = new SyncResult(_sess.put(path));
        if ( !res.error() ) {
          remres = _sess.stat(path);
          if ( remres.error() )
            res = new SyncResult(remres);
          else {
            remres.attrs.setACMODTIME(remres.attrs.getATime(), lmtime);
            res = new SyncResult(_sess.setStat(path, remres.attrs));
          }
        }
      } else
        res = new SyncResult(Utils.ErrCode.NotNeeded);
      return res;
    }

    private SyncResult syncDir(String local, String remote) {
      Log.i(this.getClass().getSimpleName(), "testing " + local + " against " + remote);
      String dirpath = FileManager.absPath(local);
      File dir = new File(dirpath);
      if ( ! dir.isDirectory() )
        return new SyncResult(Utils.ErrCode.BadParam);
      SyncResult res = new SyncResult(Utils.ErrCode.Ok);
      SFTP.SFTPStatResult rstat = _sess.stat(remote);
      if ( rstat.errcode == Utils.ErrCode.NotFound ) {
        res = new SyncResult(_sess.recursiveMakeDir(remote));
        if ( res.error() )
          return res;
      }
      File file;
      String path;
      String[] list = dir.list();
      if (list != null)
        for (String item : list)
          if ( item.charAt(0) != '.' ) {
            path = FileManager.pathAdd(local, item);
            file = new File(FileManager.absPath(path));
            if ( !file.isHidden() ) {
              if ( file.isDirectory() )
                res = syncDir(path, path);
              else
                res = syncFile(path);
              if (res.error())
                break;
            }
          }
      return res;
    }

    protected SyncResult connect(SyncJob job) {
      // test internet access
      if (!Utils.isReachable(_sess.host()))
        return new SyncResult(Utils.ErrCode.NoNet, getBaseContext().getString(R.string.nointernet));
      return new SyncResult(_sess.connect(_keyDir, Constants.SyncKeysFileName, job.privkeypass));
    }

    protected void disconnect() {
      if (_sess != null && _sess.isConnected())
        _sess.disconnect();
    }

  }     // SyncHandler

  static public class SyncResult extends Utils.TaskResult {
    static public final String name = "Sync";
    SyncResult(Utils.ErrCode code) {
      super(name, code);
    }
    SyncResult(Utils.ErrCode code, String msg) {
      this(code);
      this.msg = msg;
    }
    SyncResult(Utils.TaskResult other) {
      super(name, other.errcode, other.msg);
    }
  }

  static class SSHParams {
    protected String user;
    protected String host;
    protected byte[] pubkey;
    @SuppressWarnings("CanBeFinal")
    protected int port = 22;

    protected boolean test() {
      return user != null && user.length() > 0
          && host != null && host.length() > 3 && host.contains(".")
          && pubkey != null && pubkey.length > 30
          && port > 0;
    }
  }
}