/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;

public class RecordSample {

  final static int DP_PER_BAR = 3;
  final static float MAX_SAMPLE_HEIGHT = 255.0f;

  private final DisplayMetrics _metrics;
  private final Paint _painting = new Paint();

  private final int _width;
  private final int _height;
  private final Bitmap _bm;

  public RecordSample(DisplayMetrics metrics, int w, int h) {
    super();
    _metrics = metrics;
    _width = w;
    _height = h;
    _bm = Bitmap.createBitmap(_width, _height, Bitmap.Config.ARGB_8888);

    _painting.setStrokeWidth(1f);
    _painting.setAntiAlias(true);
    _painting.setColor(Color.GREEN);
  }

  public Bitmap bitmap() { return _bm; }

  public void draw(byte[] bytes) {

    if ( bytes == null || _width < pixels(DP_PER_BAR) || _height < 8 || bytes.length < 3 )
      return;
    final int drawh = _height / 2;
    final int tbars = _width / pixels(DP_PER_BAR);

    byte sample;
    int topy = (_height - drawh) / 2;
    int barx;
    Canvas canvas = new Canvas(_bm);
    _painting.setARGB(0xff, bytes[bytes.length-1], bytes[bytes.length-2], bytes[bytes.length-3]);

    for ( int ibar = 0; ibar < tbars; ibar++ ) {

      // get sample value
      int byteNum = ibar * bytes.length / tbars;
      sample = bytes[byteNum];
      if (0 > sample)
        sample *= -1;

      barx = ibar * pixels(DP_PER_BAR);
      float left = barx;
      float top = topy + drawh - Math.max(1, drawh * sample / MAX_SAMPLE_HEIGHT);
      float right = barx + pixels(DP_PER_BAR - 1);
      float bottom = topy + drawh;
      canvas.drawRect(left, top, right, bottom, _painting);
    }
  }

  private int pixels(float dp) {
    if ( dp == 0 )
      return 0;
    return (int) Math.ceil(_metrics.density * dp);
  }

}
