/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.List;

class DirsViewAdapter extends ItemsViewAdapter {

  private final DirsFragment _fragment;

  DirsViewAdapter(DirsFragment frm) {
    super(frm);
    _fragment = frm;
  }

  public void onAttachedToRecyclerView(@NonNull RecyclerView rv) {
    _rv = rv;
  }

  @NonNull
  @Override
  public DirViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ipos) {
    View view = LayoutInflater.from(_fragment.requireActivity()).inflate(R.layout.dir_item, parent, false);
    return new DirViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ItemViewHolder viewHolder, int pos) {
    File dir = dirs().get(pos);
    final DirViewHolder holder = (DirViewHolder) viewHolder;
    char[] name = dir.getName().toCharArray();
    holder._textView.setText(name,0, name.length);
    if (dir.getName().equals(".."))
      holder._imgView.setImageResource(R.drawable.ic_folder_upload);
    else
      holder._imgView.setImageResource(R.drawable.folder_placeholder);
    holder.mark(_fragment._dataModel.marked(dir));
    holder._card.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(getClass().getSimpleName(), "_card.onClick");
        if ( 0 < _fragment.dataModel().selected() ) {
          if (!holder._textView.getText().toString().equals(".."))    // don't select ".."
            toggleSelect(holder);
        } else
          _fragment.chdir(holder._textView.getText().toString());
      }
    });
    holder._card.setOnLongClickListener(view -> {
      if (!holder._textView.getText().toString().equals(".."))    // don't select ".."
        toggleSelect(holder);
      return true;
    });
    if (!dir.getName().equals("..")) {   // don't select ".."
      Utils.exe().execute(ItemsViewAdapter.ImageLoadTask.push(this, dirs().get(pos), pos));
    }
  }
  @Override
  public int getItemCount() {
    if (dirs() != null)
      return dirs().size();
    return 0;
  }

  private List<File> dirs() {
    return _fragment.dataModel().mgr().dirs().getValue();
  }

  @Override
  void imageLoaded(int pos, Bitmap bm) {
    View view;
    int iv;
    DirsViewAdapter.DirViewHolder holder;
    for (iv=0; iv<_rv.getChildCount(); ++iv) {
      view = _rv.getChildAt(iv);
      if (_rv.getChildAdapterPosition(view) == pos) {
        holder = (DirsViewAdapter.DirViewHolder) _rv.getChildViewHolder(view);
        holder._imgView.setImageBitmap(bm);
        break;
      }
    }
    if (iv == _rv.getChildCount()) {
      Log.d(getClass().getSimpleName() + ".imageLoaded: ", "Couldn't found View for pos " + pos);
      _pendingbmps.add(new PendingBmp(pos, bm));
    }
  }

  private static class DirViewHolder extends ItemViewHolder {
    private final TextView _textView;

    private DirViewHolder(View view) {
      super(view);
      _textView = view.findViewById(R.id.dir_text);
      _imgView = view.findViewById(R.id.dir_img);
    }
  }
}
