/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;


/**
 */

public class AcceptFragment extends DialogFragment {

  // Use this instance of the interface to deliver action events
  private AcceptListener  _listener = null;
  private Dialog          _dlg = null;
  private @StringRes
        int               _titid = R.string.hello_accept_fragment;
  String                  _textid;
  private boolean         _accepted = false;
  private int             _actioncode = 0;
  private int             _jobid = 0;

  // Esto permite ajustarse la iface como interese
  public interface AcceptListener {
    void onAcceptOkClick(DialogFragment dialog);
    void onAcceptCancelClick();
  }

  public void setListener(AcceptListener listener) { _listener = listener; }

  public void setTitle(@StringRes int titId) {
    _titid = titId;
    if (_dlg != null)
      _dlg.setTitle(_titid);
  }

  public void setText(String textId) {
    _textid = textId;
    if (_dlg != null) {
      TextView tv = _dlg.findViewById(R.id.oktextview);
      if (tv != null)
        tv.setText(_textid);
    }
  }

  public void setActionCode(int code) { _actioncode = code; }

  public void setJobId(int id) { _jobid = id; }

  public int actionCode() { return _actioncode; }

  public int jobId() { return _jobid; }

  private void onOkClick(DialogInterface dialogInterface, int i) {
    _accepted = true;
  }

  @SuppressLint("InflateParams")
  @NonNull
  @Override
  public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

    AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
    LayoutInflater inflater = requireActivity().getLayoutInflater();

    View view = inflater.inflate(R.layout.accept, null);
    builder.setView(view);
    builder.setTitle(_titid);
    builder.setPositiveButton(R.string.ok, this::onOkClick);
    builder.setNegativeButton(R.string.cancel, null);

    _dlg = builder.create();
    // don't use _dlg.findView(), it won't work
    TextView tv = view.findViewById(R.id.oktextview);
    if (tv != null && _textid != null)
      tv.setText(_textid);

    return _dlg;
  }

  @Override
  public void show(@NonNull FragmentManager manager, @Nullable String tag) {
    _accepted = false;
    super.show(manager, tag);
  }

  @Override
  public void onStop() {
    super.onStop();
    if (_listener != null)
      if (_accepted)
        _listener.onAcceptOkClick(this);
      else
        _listener.onAcceptCancelClick();
  }

}
