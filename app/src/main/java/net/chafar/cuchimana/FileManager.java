/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

@SuppressWarnings({"WeakerAccess", "unused"})
public class FileManager {

  private final File                _root;
  private File                      _currPath;
  private String                    _relPath;
  private final String              _cachedir;
  private final String              _userdir;
  private final FilesLiveData       _files, _dirs;
  private final MutableLiveData<String>   _currDirName;
  static private FileManager        _instance = null;

  private FileManager(Context ctx, @Nullable String currpath) {
    _cachedir = ctx.getCacheDir().getPath();
    _userdir = ctx.getFilesDir().getPath();
    _files = new FilesLiveData(new ArrayList<>());
    _dirs = new FilesLiveData(new ArrayList<>());
    // esto NO es igual que ctx.getExternalFilesDir(null)
    _root = new File(Environment.getExternalStorageDirectory(), Constants.RootDir);
    if (!_root.exists())
      _root.mkdir();
    _setCurrPath(currpath);
    _currDirName = new MutableLiveData<>();
    _currDirName.setValue(new File(_relPath).getName());
    Log.d(this.getClass().getSimpleName(), "root path: " + ctx.getExternalFilesDir(null));
    Log.d(this.getClass().getSimpleName(), "root path (old): " + _root.getPath());
    load();
  }

  static public void create(Context ctx, @Nullable String currpath) {
    if (_instance == null)
      _instance = new FileManager(ctx, currpath);
  }

  static public FileManager instance() {
    return _instance;
  }

  public String root() { return _root.getPath(); }

  public void home() {
    _setCurrPath(Constants.RootDir);
    Log.d("home", "newpath: " + _relPath);
    _currDirName.setValue(new File(_relPath).getName());
  }

  public boolean chdir(String relpath) {
    File newpath;
    Log.d("CHDIR: ", _relPath + ", " + relpath + ", " + Constants.RootDir);
    // parece de locos, pero aki relpath == ".." no furrula !!!!!!!!!
    if ( relpath.contentEquals( "..")) {
      if ( _relPath.contentEquals(Constants.RootDir))
        return false;
      newpath = new File(_relPath).getParentFile();
    } else
      newpath = new File(_relPath, relpath);
    if ( null != newpath ) {
      _setCurrPath(newpath.getPath());
      Log.d("CHDIR: ", "newpath: " + _relPath);
      _currDirName.setValue(new File(_relPath).getName());
    }
    return true;
  }

  public boolean mkdir(String name) {
    File newpath = new File(_currPath, name);
    Log.d("MKDIR: ", _relPath + "/" + name + ", " + newpath.toString());
    return newpath.mkdir();
  }
  /// returns absolute path of file at pos
  public File file(int pos) {
    if (_files.getValue() != null) {
      if ( pos <= _files.getValue().size() )
        return _files.getValue().get(pos);
    }
    return null;
  }

  public File dir(int pos) {
    if (_dirs.getValue() != null && pos < _dirs.getValue().size())
      return _dirs.getValue().get(pos);
    return null;
  }

  public void removeSamples(File file) {
    File sample = new File(ImageManager.instance().getSamplePath(file.getName(), Constants.SampleSize, Constants.SampleSize));
    if (sample.exists())
      sample.delete();
  }

  public boolean renameDir(File dir, String name) {
    if (!rename(dir, name)) {
      Log.e(getClass().getSimpleName() + ".renameDir", "couldn't rename " + dir.getPath() + " to " + name);
      return false;
    }
    dir = new File(samplesCachePath(), dir.getName());
    if (rename(dir, name)) {
      dir = new File(cachePath(), dir.getName());
      return rename(dir, name);
    }
    Log.e(getClass().getSimpleName() + ".renameDir", "couldn't rename " + dir.getPath() + " to " + name);
    return false;
  }

  /** If new file sample exists, it is deleted to force rebuilding
   */
  public boolean renameFile(File file, String name) {
    String ext = file.toString().substring(file.toString().lastIndexOf('.'));
    if (!SyncJob.isOngoing(relPathOf(file.getPath())) && rename(file, name + ext)) {
      removeSamples(file);
      return true;
    }
    return false;
  }

  public boolean removeFile(int pos) {
    File file = file(pos);
    if (file != null && !SyncJob.isOngoing(relPathOf(file.getPath()))) {
      if (file.delete()) {
        removeImageSample(file.getName());
        return true;
      }
    }
    return false;
  }

  /** Removes instances of given virtual dir
   * removes every real dir matching virtual dir, if not empty
   * and, if every one has been deleted, it also removes private cache dir.
   * Returns true only if every real public and private dir has been deleted.
   */
  public boolean removeDir(int pos) {
    File dir = dir(pos);
    if (dir.exists())
      return dir.delete();
    return true;
  }

  /// returns current path relative from Cuchimana root, included
  public String relPath() { return _relPath; }

  /// returns current path relative from Cuchimana root, included
  public String relFilePath(int pos) {
    return _relItemPath(_files.getValue(), pos);
  }

  /// returns current path relative from Cuchimana root, included
  public String relDirPath(int pos) {
    return _relItemPath(_dirs.getValue(), pos);
  }

  /// returns current path relative from Cuchimana root, included
  public String relPath(String name) {
    return pathAdd(_relPath, name);
  }

  /// returns current path relative from Cuchimana root, included
  private String _relItemPath(ArrayList<File> list, int pos) {
    if (list != null) {
      if ( pos <= list.size() )
        return pathAdd(_relPath, list.get(pos).getName());
    }
    return null;
  }

  /// returns current absolute path
  public File absCurrPath() { return _currPath; }

  public MutableLiveData<String> currDirName() { return _currDirName; }

  /// returns private files app dir
  public String userDir() {
    return _userdir;
  }

  public File cachePath() {
    /// para acceder al contenido de dirs internos, hay que usar adb shell run-as:
    /// run-as net.chafar.android.piezas ls -Rl /data/user/0/net.chafar.cuchimana/
    return cachePath(_relPath);
  }

  private File cachePath(String vpath) {
    return new File(_cachedir, vpath);
  }

  public String samplesCachePath() {
    return samplesCachePath(relPath());
  }

  public String samplesCachePath(String vpath) {
    File sampath = new File (_cachedir, "samples");
    return new File(sampath, vpath).toString();
  }

  private boolean isInList(List<File> list, String name) {
    if (list != null) {
      for ( File dir : list ) {
        if ( name.equals(dir.getName()) )
          return true;
      }
    }
    return false;
  }

  /// This is, btm, the only place (in addition to the constructor) where changes in _files and _dirs get observable
  public void load() {
    if (extStorageAvailable()) {
      // LiveData observer get warned only when observer value is set via setValue()/postValue():
      // if value is some collection, observer will not notice when its content changes
      // first, populate new lists
      ArrayList<File> files = new ArrayList<>();
      ArrayList<File> dirs = new ArrayList<>();
      if (!_relPath.contentEquals(Constants.RootDir))
        dirs.add(new File(".."));
      _load(files, dirs);
      // then, activate new list, triggering observer
      _files.setValue(files);
      _dirs.setValue(dirs);
    }
  }

  private void _setCurrPath(String vpath) {
    if (vpath != null) {
      _relPath = vpath;
      _currPath = new File(_root.getParent(), _relPath);
    } else {
      _relPath = Constants.RootDir;
      _currPath = _root;
    }
  }

  private void _load(List<File> fileList, List<File> dirList) {
    File[] content;
    if (_currPath.exists()) {
      content = _currPath.listFiles();
      if (content != null) {
        _loadFiles(content, fileList);
        _loadDirs(content, dirList);
      }
    }
  }

  private void _loadFiles(File[] content, List<File> fileList) {
    for ( File file : content )
      if ( !file.isHidden() && file.isFile() && fileList != null )
        fileList.add(file);
  }

  private void _loadDirs(File[] content, List<File> dirList) {
    for ( File dir : content )
      if ( !dir.isHidden() && dir.isDirectory() && dirList != null )
        dirList.add(dir);
  }

  public boolean extStorageAvailable() {
    return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
  }

  LiveData<ArrayList<File>> files() { return _files; }

  LiveData<ArrayList<File>> dirs() { return _dirs; }

  @NonNull
  ArrayList<File> dirList() { return _dirs.getValue() != null ? _dirs.getValue() : new ArrayList<>(); }

  @NonNull
  ArrayList<File> fileList() { return _files.getValue() != null ? _files.getValue() : new ArrayList<>(); }

  int dirsCount() { return _dirs.getValue().size(); }

  int filesCount() { return _files.getValue().size(); }

  public File mkTempImageFile() throws IOException {
    String imageFileName = new SimpleDateFormat("yyMMddHHmmss", Locale.ROOT).format(new Date()) + "-";
    if (!_currPath.exists())
      _currPath.mkdirs();
    return File.createTempFile( imageFileName,"." + Utils.getExtensionFromMimeType(Constants.CamPicMime), _currPath );
  }

  public File mkTempVideoFile() throws IOException {
    String imageFileName = new SimpleDateFormat("yyMMddHHmmss", Locale.ROOT).format(new Date()) + "-";
    if (!_currPath.exists())
      _currPath.mkdirs();
    return File.createTempFile( imageFileName,"." + Utils.getExtensionFromMimeType(Constants.CamVidMime), _currPath );
  }

  public File mkTempVoiceFile() throws IOException {
    String voiceFileName = new SimpleDateFormat("yyMMddHHmmss", Locale.ROOT).format(new Date()) + "-";
    if (!_currPath.exists())
      _currPath.mkdirs();
    return File.createTempFile( voiceFileName,"." + Utils.getExtensionFromMimeType(Constants.VoiceRecordMime), _currPath );
  }

  public File mkTempTextFile() throws IOException {
    String textFileName = new SimpleDateFormat("yyMMddHHmmss", Locale.ROOT).format(new Date()) + "-";
    if (!_currPath.exists())
      _currPath.mkdirs();
    return File.createTempFile( textFileName,"." + Utils.getExtensionFromMimeType(Constants.TextMime), _currPath );
  }

  public File mkTempLocFile() throws IOException {
    String locFileName = new SimpleDateFormat("yyMMddHHmmss", Locale.ROOT).format(new Date());
    if (!_currPath.exists())
      _currPath.mkdirs();
    return File.createTempFile( locFileName,"." + Constants.GeoExtension, _currPath );
  }

  //
  // saving samples
  //

  private void removeImageSample(String name) {
    File sample = new File(samplesCachePath(_relPath), name);
    if (sample.exists())
      sample.delete();
  }

  public void saveImageSample(Bitmap bm, String name) {
    saveImageSample(bm, _relPath, name, false);
  }

  public void saveImageSampleAsync(Bitmap bm, String name) {
    saveImageSample(bm, _relPath, name, true);
  }

  protected void saveImageSample(Bitmap bm, String vpath, String name, boolean async) {
    if (extStorageAvailable()) {
      File dirpath = new File(samplesCachePath(vpath));
      Log.d("SAVE_SAMPLE: ", dirpath + ", " + name);
      if (!dirpath.exists())
        dirpath.mkdirs();
      if (dirpath.canWrite()) {
        File filepath = new File(dirpath, name);
        try {
          FileOutputStream os = new FileOutputStream(filepath);
          // no-async if you are already working in background and you need the file
          if (async)
            Utils.exe().execute( new SaveBitmapTask(bm, os) );
          else
            bm.compress(Bitmap.CompressFormat.PNG, 5, os);
        } catch (Exception ex) {
          Utils.logd(ex,getClass().getSimpleName() + ".saveImageSample");
        }
      }
    }
  }

  //
  // static stuff
  //

  static public File absPath(File file) {
    if ( !file.isAbsolute() ) {
      String root = Environment.getExternalStorageDirectory().getPath();
      return new File(root, file.getPath());
    }
    return file;
  }

  static public String absPath(String path) {
    return absPath(new File(path)).getPath();
  }

  static public String pathAdd(final String path, final String name) {
    return path + '/' + name;
  }

  /// returns Cuchimana rel path from absolute path
  static public String relPathOf(String abspath) {
    return abspath.substring(Environment.getExternalStorageDirectory().getPath().length()+1);
  }

  static public int lmtime(File file) {
    long lmtime = 0L;
    try { lmtime = file.lastModified() / 1000; }
    catch(Exception ex) {
      Utils.loge(ex, "lmtime");
    }
    return (int)lmtime;
  }

  /** changes only de _name_ part of the path
   * returns false only if renaming fails, not if original doesn't exists
   */
  static public boolean rename(@NonNull File old, String newname) {
    if (old.exists())
      return old.renameTo(new File(old.getParent(), newname));
    return false;
  }

  /// btm, it won't write onto an existing file
  static public boolean copyFile(String src, String dest) {
    File org = new File(src);
    if (!org.exists() || !org.canRead())
      return false;
    File dst = absPath(new File(dest));
    if (dst.exists() || dst.getParent() == null)
      return false;
    File path = new File(dst.getParent());
    if (!path.canWrite())
      return false;
    try ( InputStream in = new FileInputStream(src)) {
      OutputStream out = new FileOutputStream(dst.getPath());
      // Transfer bytes from in to out
      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
    } catch(Exception ex) { return false; }
    return true;
  }

  static public boolean copyFile(ParcelFileDescriptor infd, File outf) {
    if ( infd != null && !outf.exists()) {
      FileInputStream fis = new FileInputStream(infd.getFileDescriptor());
      byte[] bytes = new byte[1024];
      int len;
      try {
        FileOutputStream outs = new FileOutputStream(outf);
        do {
          len = fis.read(bytes);
          if (len > 0)
            outs.write(bytes);
        } while (len > 0);
        outs.close();
        fis.close();
        return true;
      } catch ( FileNotFoundException e) {
        Utils.loge( e, "File not found" );
        return false;
      } catch ( IOException e) {
        Utils.loge( e, "IO error" );
        return false;
      }
    }
    return false;
  }

  static public class SaveBitmapTask extends FutureTask<Void> {

    SaveBitmapTask(Bitmap bm, FileOutputStream os) {
      this(bm, os, Bitmap.CompressFormat.PNG);
    }

    SaveBitmapTask(Bitmap bm, FileOutputStream os, Bitmap.CompressFormat fmt) { this(bm, os, fmt, 5); }

    SaveBitmapTask(Bitmap bm, FileOutputStream os, Bitmap.CompressFormat fmt, int quality) {
      super(new SaveBitmapCallable(bm, os, fmt, quality));
    }
  }

  static private class SaveBitmapCallable implements Callable<Void> {
    private final FileOutputStream _os;
    private final Bitmap.CompressFormat _fmt;
    private final int _qty;
    private final Bitmap _bm;

    SaveBitmapCallable(Bitmap bm, FileOutputStream os, Bitmap.CompressFormat fmt, int quality) {
      _os = os;
      _fmt = fmt;
      _qty = quality;
      _bm = bm;
    }

    @Override
    public Void call() {
      try {
        _bm.compress(_fmt, _qty, _os);
        _os.flush();
        _os.close();
      } catch (Exception ex) {
        Utils.loge(ex, getClass().getSimpleName() + ".saveBitmakAsync: ");
      }
      return null;
    }
  }

  private static class FilesLiveData extends MutableLiveData<ArrayList<File>> {

    public FilesLiveData(ArrayList<File> files) {
      super();
      setValue(files);
    }

  }
}
