/*
  Cuchimana. Android document manager
  Copyright (C) 2021  José Esteban Linde

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.chafar.cuchimana;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

/* IMPORTANTE:
 * Aquí, al ser llamada desde otra apli, se crea un view model nuevo:
 * lo que se hace akí no se refleja en Folder, x muy viva que esté ésta.
 */

public class Receive extends ActivityBase {

  private Intent    _intent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_receive);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    _intent = getIntent();
    if (_intent != null) {
      final String action = _intent.getAction();
      if (!Intent.ACTION_SEND.equals(action) && !Intent.ACTION_SEND_MULTIPLE.equals(action)) {
        Log.d(getClass().getSimpleName(), "Not accepted intent action: " + action);
        setResult(Activity.RESULT_CANCELED);
        finish();
      }
    }

    _dataModel.mgr().currDirName().observe(this, name -> {
      if (getSupportActionBar() != null)
        getSupportActionBar().setTitle(name);
    });
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    FloatingActionButton fab = findViewById(R.id.fab);
    if (fab != null) {
      fab.setImageResource(R.drawable.ic_pick);
      fab.setOnClickListener(this::onFabClick);
    }
  }

  private DirsFragment fragment() {
    if (getSupportFragmentManager().getFragments().size() > 0)
      return (DirsFragment) getSupportFragmentManager().getFragments().get(0);
    return null;
  }

  void workDone() {
    setResult(Activity.RESULT_OK);
    finish();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Esto se llama ya avanzada la creación de la vista de la activity y si llamas a Activity.invalidateOptionsMenu()
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.receive_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if ( item.getItemId() == R.id.menu_new_dir ) {
      DirsFragment frm = fragment();
      if ( frm != null )
        return frm.doCommand(item.getItemId());
    }
    return false;
  }

  private void onFabClick(View view) {
    if ( _intent != null ) {
      // localizar el fragment
      DirsFragment frag = fragment();
      if ( frag != null ) {
        Job.make(R.id.menu_pick);
        frag.receiveFile(_intent);
      }
    }
  }
}
